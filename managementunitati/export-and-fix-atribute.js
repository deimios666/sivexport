//This script deletes the "până la" value if it's in the future
//IIFE for async
(async () => {
  const limit = 1000; //nr PJ-uri
  const schoolYearId = AMS.util.AppConfig.selectedYear.data.id; //get selected year ID from SIIIR 22=2019-2020, 23=2020-2021
  const schoolListURL = "https://www.siiir.edu.ro/siiir/list/school.json";
  const schoolListParam = `generatorKey=PASMQG&filter=%5B%7B%22property%22%3A%22statut.description%22%2C%22criteria%22%3A%22LIKE%22%2C%22parameters%22%3A%5B%22Cu%20pers%22%5D%7D%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A${schoolYearId}%2C%22userLevel%22%3A%22COUNTY%22%7D&page=1&start=0&limit=${limit}`;

  const attribListURL =
    "https://www.siiir.edu.ro/siiir/list/entityAttributeValueSchool.json";
  const attribListParamPrefix = `generatorKey=entityAttributeValueSchool_GRID_GENERATOR_KEY&filter=%5B%7B%22property%22%3A%22internalId%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B`;
  const attribListParamSuffix = `%5D%7D%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%7D&page=1&start=0&&limit=${limit}`;

  const editPutURL =
    "https://www.siiir.edu.ro/siiir/management/entityAttributeValueSchool/";

  const csvData = [];

  const csvHeaders = ["Unitate", "ID", "Attrib", "Dela", "Pânăla", "Corectat"];

  //restore console
  let iFrame = document.createElement("iframe");
  iFrame.style.display = "none";
  document.body.appendChild(iFrame);
  window.console = iFrame.contentWindow.console;

  const schoolListResult = await fetch(
    schoolListURL + "?_dc=" + new Date().getTime(),
    {
      method: "post",
      credentials: "same-origin",
      headers: {
        Accept: "application/json",
        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
      },
      body: schoolListParam,
    }
  );

  const schoolList = await schoolListResult.json();

  for (const school of schoolList.page.content) {
    const attribListResult = await fetch(
      attribListURL + "?_dc=" + new Date().getTime(),
      {
        method: "post",
        credentials: "same-origin",
        headers: {
          Accept: "application/json",
          "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
        },
        body: attribListParamPrefix + school.internalId + attribListParamSuffix,
      }
    );

    const attribList = await attribListResult.json();

    for (const attrib of attribList.page.content) {
      const oneCSVLine = [];

      oneCSVLine.push(school.longName);
      oneCSVLine.push(attrib.id);
      oneCSVLine.push(attrib.entityAttribute.name);
      oneCSVLine.push(attrib.dateFrom);
      oneCSVLine.push(attrib.dateTo);

      //delete panaLa if in future
      if (attrib.dateTo) {
        const dateToObj = new Date(
          Date.parse(attrib.dateTo.split("-").join("/"))
        );
        const currentDateObj = new Date();
        if (dateToObj > currentDateObj) {
          //if it's in the future, we delete it
          //edit attribute
          const payload = attrib;
          payload.dateTo = null;
          payload.school = school;

          const editPutResult = await fetch(
            editPutURL + attrib.id + "?_dc=" + new Date().getTime(),
            {
              method: "put",
              credentials: "same-origin",
              headers: {
                Accept: "application/json",
                "Content-type": "application/json",
              },
              body: JSON.stringify(payload),
            }
          );
          oneCSVLine.push("CORECTAT");
        } else {
          oneCSVLine.push("");
        }
      } else {
        oneCSVLine.push("");
      }

      csvData.push('"' + oneCSVLine.join('","') + '"'); //convert to CSV
    }
  }
  const fileName = "export_attrib.csv";
  const buffer = "\uFEFF" + csvHeaders.join(",") + "\n" + csvData.join("\n");
  const blob = new Blob([buffer], {
    type: "text/csv;charset=utf8;",
  });
  const url = window.URL.createObjectURL(blob);
  const a = document.createElement("a");
  a.href = url;
  a.download = fileName;
  a.click();
})();