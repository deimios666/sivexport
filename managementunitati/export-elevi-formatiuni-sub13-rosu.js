//IIFE for async
(async () => {
    const limit = 100;
    const schoolYearId = AMS.util.AppConfig.selectedYear.data.id; //get selected year ID from SIIIR 22=2019-2020, 23=2020-2021
    const formationListURL =
        "https://www.siiir.edu.ro/siiir/list/studyFormation.json";
    const formationListParam = `generatorKey=SFQG&filter=%5B%5D&sort=%5B%7B%22property%22%3A%22studyFormationType.orderBy%22%2C%22direction%22%3A%22ASC%22%7D%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A${schoolYearId}%7D&page=1&start=0&limit=${limit}`;

    const eleviListURL = "https://www.siiir.edu.ro/siiir/list/asocStudent.json";
    const eleviListParamPrefix = `generatorKey=asocStudent_GRID_GENERATOR_KEY&filter=%5B%7B%22property%22%3A%22studyFormation.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B`;
    const eleviListParamSuffix = `%5D%7D%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%7D&page=1&start=0&limit=${limit}`;

    const csvData = [];

    const csvHeaders = [
        "CNP",
        "Nume",
        "Ini",
        "Prenume1",
        "Prenume2",
        "Prenume3",
        "Localitate",
        "Unitatea PJ",
        "Unitatea AR",
        "Clasa",
        "Stare",
        "De la",
        "Până la",
    ];

    const csvKeys = [
        "student.nin",
        "student.lastName",
        "student.fatherInitial",
        "student.firstName",
        "student.firstName1",
        "student.firstName2",
        "studyFormation.school.locality.description",
        "studyFormation.school.parentSchool.longName",
        "studyFormation.school.longName",
        "studyFormation.code",
        "studyStatus.description",
        "from",
        "to",
    ];

    const formationListResult = await fetch(
        formationListURL + "?_dc=" + new Date().getTime(),
        {
            method: "post",
            credentials: "same-origin",
            headers: {
                Accept: "application/json",
                "Content-type":
                    "application/x-www-form-urlencoded; charset=UTF-8",
            },
            body: formationListParam,
        }
    );

    const formationList = await formationListResult.json();

    for (const formation of formationList.page.content) {
        const eleviListResult = await fetch(
            eleviListURL + "?_dc=" + new Date().getTime(),
            {
                method: "post",
                credentials: "same-origin",
                headers: {
                    Accept: "application/json",
                    "Content-type":
                        "application/x-www-form-urlencoded; charset=UTF-8",
                },
                body:
                    eleviListParamPrefix + formation.id + eleviListParamSuffix,
            }
        );

        const eleviList = await eleviListResult.json();

        for (const elev of eleviList.page.content) {
            const oneCSVLine = csvKeys.map((key) => {
                try {
                    const value = key
                        .split(".")
                        .reduce((o, i) => o[i], elev) //resolve value
                        .toString()
                        .split('"')
                        .join('""'); //escape quotes
                    return `"${value}"`; //return the formatted field
                } catch (err) {
                    return `""`;
                }
            });

            csvData.push('"' + oneCSVLine.join('","') + '"'); //convert to CSV
        }
    }
    const fileName = "export_elevi.csv";
    const buffer = "\uFEFF" + csvHeaders.join(",") + "\n" + csvData.join("\n");
    const blob = new Blob([buffer], {
        type: "text/csv;charset=utf8;",
    });
    const url = window.URL.createObjectURL(blob);
    const a = document.createElement("a");
    a.href = url;
    a.download = fileName;
    a.click();
})();