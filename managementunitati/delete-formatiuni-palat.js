//IIFE for async
(async () => {
  const listUrl = "https://www.siiir.edu.ro/siiir/list/cssGroup.json";
  const pageLimit = 500;
  const schoolYearId = AMS.util.AppConfig.selectedYear.data.id;
  const listParamsPrefix = `generatorKey=CSSGQG&filter=%5B%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A${schoolYearId}%7D`;

  const actionUrl = "https://www.siiir.edu.ro/siiir/management/cssGroup/";

  //restore console
  let iFrame = document.createElement('iframe');
  iFrame.style.display = 'none';
  document.body.appendChild(iFrame);
  window.console = iFrame.contentWindow.console;

  const csvData = [];

  //get nr of results
  const primingListResult = await fetch(listUrl + "?_dc=" + new Date().getTime(), {
    method: "post",
    credentials: "same-origin",
    headers: {
      Accept: "application/json",
      "Content-type": "application/x-www-form-urlencoded",
    },
    body: listParamsPrefix + "&limit=1&page=1&start=0",
  });

  const primingListJSON = await primingListResult.json();

  const maxItems = primingListJSON.page.total;
  const maxPages = Math.ceil(maxItems / pageLimit); //calculate nr of pages

  for (let page = 1; page <= maxPages; page++) {

    const start = (page - 1) * pageLimit;

    console.log(`Downloading page ${page} of ${maxPages}`);

    const listResult = await fetch(listUrl + "?_dc=" + new Date().getTime(), {
      method: "post",
      credentials: "same-origin",
      headers: {
        Accept: "application/json",
        "Content-type": "application/x-www-form-urlencoded",
      },
      body: listParamsPrefix + `&limit=${pageLimit}&page=${page}&start=${start}`,
    });

    const listJSON = await listResult.json();

    //do stuff
    for (const formatiune of listJSON.page.content) {
      console.log(`Deleting id: ${formatiune.id}`);
      const deleteResult = await fetch(actionUrl + formatiune.id + "?_dc=" + new Date().getTime(), {
        method: "delete",
        credentials: "same-origin",
        headers: {
          Accept: "application/json",
          "Content-type": "application/json;charset=UTF-8",
        },
        body: JSON.stringify(formatiune)
      });
    }
  }
})();