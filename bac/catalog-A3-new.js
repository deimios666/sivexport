(async () => {

  const reportUrl = 'https://bac.edu.ro/bac/excel/10039.jasper';

  const result = await fetch(reportUrl + "?_dc=" + (new Date).getTime(), {
    method: 'post',
    credentials: 'same-origin',
    headers: {
      "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
      "Content-type": "application/x-www-form-urlencoded"
    },
    body: "type=xls&source=ISJ_ExportA&session=25"
  });

  const resultBlob = await result.blob();

  let url = URL.createObjectURL(resultBlob);
  let a = document.createElement('a');
  a.download = "export.xlsx";
  a.href = url;
  a.textContent = "Download file";
  a.click();

})();