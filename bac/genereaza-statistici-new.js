(async () => {
  const itemUrl = "https://bac.edu.ro/bac/list/school.json";
  const idJudet = AMS.util.AppConfig.filtersConfig.county[0].parameters[0];
  const idSesiune = AMS.util.AppConfig.filtersConfig.currentSessionId;
  const itemListParamString = `filter=%5B%7B%22property%22%3A%22county.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B${idJudet}%5D%7D%5D&sort=%5B%7B%22property%22%3A%22shortName%22%2C%22direction%22%3A%22ASC%22%7D%5D&page=1&start=0&limit=10000`;
  const fileUrl = "https://bac.edu.ro/bac/stats/generateReport";
  const zipFileName = "statistici.zip";

  await import('https://cdnjs.cloudflare.com/ajax/libs/jszip/3.10.0/jszip.min.js');

  const replaceAll = (value, search, replacement) => value.split(search).join(replacement);

  const zip = new JSZip();

  const listResult = await fetch(itemUrl + "?_dc=" + (new Date).getTime(), {
    method: 'post',
    credentials: 'same-origin',
    headers: {
      "Accept": "application/json",
      "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
      "X-Requested-With": "XMLHttpRequest"
    },
    body: itemListParamString
  })
  const listJson = await listResult.json();
  const coll = listJson.page.content;
  const maxValues = coll.length;
  let i = 0;

  const isjResult = await fetch(fileUrl + "?_dc=" + (new Date).getTime(), {
    method: 'post',
    credentials: 'same-origin',
    headers: {
      "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
      "Content-type": "application/x-www-form-urlencoded"
    },
    body: `type=xls&value=NUM&subtotal=TIP&report=DISP&environment=ALL&grades=ALL_GR&schoolYear=ALL&subject=0&schoolId=&session=${idSesiune}&valuesComboBox=NUM&subtotalTypeComboBox=TIP&reportTypeComboBox=DISP&environmentTypeComboBox=ALL&gradesComboBox=ALL_GR&examSubjectEB=Toate&school=Toate&schoolYearComboBox=ALL`
  })
  const isjBlob = await isjResult.blob();
  const isjFileName = `Inspectoratul Școlar Județean.xls`;

  zip.file(isjFileName, isjBlob);

  console.log(`[${i}/${maxValues}] Inspectoratul Școlar Județean`);

  for (const item of coll) {
    i++;
    const { id, shortName, longName } = item;
    const fileResult = await fetch(fileUrl + "?_dc=" + (new Date).getTime(), {
      method: 'post',
      credentials: 'same-origin',
      headers: {
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
        "Content-type": "application/x-www-form-urlencoded"
      },
      body: `type=xls&value=NUM&subtotal=TIP&report=DISP&environment=ALL&grades=ALL_GR&schoolYear=ALL&subject=0&schoolId=${id}&session=${idSesiune}&valuesComboBox=NUM&subtotalTypeComboBox=TIP&reportTypeComboBox=DISP&environmentTypeComboBox=ALL&gradesComboBox=ALL_GR&examSubjectEB=Toate&school=${shortName}&schoolYearComboBox=ALL`
    })
    const fileBlob = await fileResult.blob();
    const fileName = `${replaceAll(longName, '"', "'")}.xls`;

    zip.file(fileName, fileBlob);

    console.log(`[${i}/${maxValues}] ${longName}`);
  }

  const zipFile = await zip.generateAsync({
    type: "blob"
  });

  let url = URL.createObjectURL(zipFile);
  let a = document.createElement('a');
  a.download = zipFileName;
  a.href = url;
  a.textContent = "Download file";
  a.click();

}
)();