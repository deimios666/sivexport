//se rulează cu cont de Centru Examen Bacalaureat
(async () => {
    const currentSessionYearId = AMS.util.AppConfig.filtersConfig.currentSessionYearId;

    const listUrl = `https://bac.edu.ro/bac/list/candidate.json`;
    const listParam = `generatorKey=CCQG&filter=%5B%7B%22property%22%3A%22student.asocSchoolStudyClass.schoolYear.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B${currentSessionYearId}%5D%7D%5D&sort=%5B%7B%22property%22%3A%22student.lastName%22%2C%22direction%22%3A%22ASC%22%7D%2C%7B%22property%22%3A%22student.firstName%22%2C%22direction%22%3A%22ASC%22%7D%2C%7B%22property%22%3A%22student.patronymic%22%2C%22direction%22%3A%22ASC%22%7D%5D&filterName=%5B%5D&requestParams=%7B%7D&viewName=CompetenceC-CE&page=1&start=0&limit=2000`;

    const actionUrl = `https://bac.edu.ro/bac/management/editStatusC.json`;

    const actionParam = ({ id, l1, l2, l3, l4, l5 }) => `id=${id}&status=MODERN_LANG_LEVEL&competenceC1=${l1}&competenceC2=${l2}&competenceC3=${l3}&competenceC4=${l4}&competenceC5=${l5}&field=statusC`;

    const list = await fetch(listUrl, {
        method: 'post',
        credentials: 'same-origin',
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
        },
        body: listParam
    });

    const listJson = await list.json();

    listJson.page.content.forEach(async (elev) => {
        //if statusC==="MODERN_LANG_LEVEL"
        if (elev.statusC === "MODERN_LANG_LEVEL") {
            //check if any of modernLangLevel1-5 is "LINE"
            let found = false;
            const payload = {
                id: elev.id,
            };
            for (let i = 1; i <= 5; i++) {
                if (elev[`modernLangLevel${i}`] === "LINE") {
                    found = true;
                    payload[`l${i}`] = "A1";
                } else {
                    payload[`l${i}`] = elev[`modernLangLevel${i}`];
                }
            }
            if (found) {
                await fetch(actionUrl, {
                    method: 'post',
                    credentials: 'same-origin',
                    headers: {
                        "Accept": "application/json",
                        "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
                    },
                    body: actionParam(payload)
                });
                console.log(`[${elev.id}]Updated`);
            }
        }
    });
})();