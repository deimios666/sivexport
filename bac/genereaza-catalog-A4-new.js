(async () => {
  const countyFromId = AMS.util.AppConfig.filtersConfig.county[0].parameters[0]; //BN-7 BV-10 29-MS
  const countyToId = AMS.util.AppConfig.filtersConfig.county[0].parameters[0]; //CV
  const president = encodeURIComponent("prof. Bobeș Luminița Elena");
  const secretar = encodeURIComponent("prof. Farkas Csaba");
  const vicePresedinte1 = encodeURIComponent("prof. Iuliana Nemes");
  const vicePresedinte2 = encodeURIComponent("");
  const listUrl = "https://bac.edu.ro/bac/list/examCenter.json";
  const listParams = `generatorKey=EVALCCFQG&viewName=default&sort=%5B%5D&requestParams=%7B%22idCountyFrom%22%3A${countyFromId}%7D&filter=%5B%5D&page=1&start=0&limit=1000`;
  const pdfUrl = "https://bac.edu.ro/bac/reports/10108.jasper"

  //internal replaceAll
  const replaceAll = (value, search, replacement) => value.split(search).join(replacement);

  await import('https://cdnjs.cloudflare.com/ajax/libs/jszip/3.10.0/jszip.min.js');
  console.log(JSZip);
  const zip = new JSZip();
  const listResponse = await fetch(`${listUrl}?_dc="` + (new Date).getTime(), {
    method: 'post',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      'X-Requested-With': 'XMLHttpRequest'
    },
    body: listParams
  });

  const listJSON = await listResponse.json();
  const maxValues = listJSON.page.content.length;
  let i = 0;

  for (const item of listJSON.page.content) {
    i++;
    const pdfParam = `type=pdf&source=ISJ_GradeBook&examCenterId=${item.id}&countyFromId=${countyFromId}&countyToId=${countyToId}&examCenterCode=${item.code}&examCenterName=${encodeURIComponent(item.school.longName)}&president=${president}&vicePresedinte2=${vicePresedinte2}&secretar=${secretar}&configCZE=&examCenter=${item.id}&vicePresedinte1=${vicePresedinte1}`;
    const pdfResponse = await fetch(`${pdfUrl}?_dc="` + (new Date).getTime(), {
      method: 'post',
      headers: {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: pdfParam
    });
    const pdfBlob = await pdfResponse.blob();
    zip.file(replaceAll(item.school.longName, '"', "'") + '.pdf', pdfBlob);
    console.log(`[${i}/${maxValues}] ${item.school.longName}`);
  }

  const zipFile = await zip.generateAsync({
    type: "blob"
  });

  let url = URL.createObjectURL(zipFile);
  let a = document.createElement('a');
  a.download = "export.zip";
  a.href = url;
  a.textContent = "Download file";
  a.click();
})();