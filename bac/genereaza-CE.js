const listURL = "https://bac.edu.ro/bac/list/school.json";
const listParam = "generatorKey=ASQG&viewName=default&sort=%5B%7B%22property%22%3A%22county.code%22%7D%2C%7B%22property%22%3A%22locality.description%22%7D%2C%7B%22property%22%3A%22code%22%7D%5D&filter=%5B%5D&page=1&start=0&limit=1000";

const createCEURL = "https://bac.edu.ro/bac/management/examCenter"

const sessionJSON = { //set this manually for now
    "appealType": "FINAL_MARK_APPEAL",
    "code": "2020 - I",
    "currentSession": true,
    "description": "Sesiunea iunie-iulie 2020",
    "from": "2019-09-01T00:00:00",
    "id": 19,
    "properties": {},
    "schoolYear": {
        "code": "2019-2020",
        "description": "Anul școlar 2019-2020",
        "from": "2019-09-01",
        "id": 22,
        "properties": {},
        "to": "2020-08-31"
    },
    "sessionType": "FIRST_SESSION",
    "to": "2020-08-31T00:00:00"
}
const createCEpayload = {
    "code": null,
    "description": null,
    "freezeAppealsDate": null,
    "id": null,
    "otherCounty": false,
    "properties": null,
    "school": null, //get from list
    "session": sessionJSON
}

const assignCEURL = "https://bac.edu.ro/bac/management/asocSchoolExamCenter";

const assignCEpayload = {
    "examCenter": {
        "id": null //get from list
    },
    "examCenterCode": null,
    "id": null,
    "properties": null,
    "school": null, //get from list
    "schoolCode": null
}

//restore console
let iFrame = document.createElement('iframe');
iFrame.style.display = 'none';
document.body.appendChild(iFrame);
window.console = iFrame.contentWindow.console;

//IIFE to use async
(async() => {

    const schoolListResult = await fetch(
        listURL + "?_dc=" + (new Date).getTime(), {
            method: 'post',
            credentials: 'same-origin',
            headers: {
                "Accept": "application/json",
                "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
                "X-Requested-With": "XMLHttpRequest"
            },
            body: listParam
        }
    );

    const schoolListJSON = await schoolListResult.json();


    for (const school of schoolListJSON.page.content) {
        //assign school as CE
        const createCEResponse = await fetch(
            createCEURL + "?_dc=" + (new Date).getTime(), {
                method: 'post',
                credentials: 'same-origin',
                headers: {
                    "Accept": "application/json",
                    "Content-type": "application/json",
                    "X-Requested-With": "XMLHttpRequest"
                },
                body: JSON.stringify({...createCEpayload,
                    school: school
                })
            }
        );
        const createCEJSON = await createCEResponse.json();

        //get CE id
        const CEid = createCEJSON.baseEntity.id;

        //use CE id to assign school to CE
        const assignCEResponse = await fetch(
            assignCEURL + "?_dc=" + (new Date).getTime(), {
                method: 'post',
                credentials: 'same-origin',
                headers: {
                    "Accept": "application/json",
                    "Content-type": "application/json",
                    "X-Requested-With": "XMLHttpRequest"
                },
                body: JSON.stringify({...assignCEpayload,
                    school: school,
                    "examCenter": {
                        "id": CEid
                    },
                })
            }
        );
    }
})();