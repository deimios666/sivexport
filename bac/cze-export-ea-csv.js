let url="https://bac.edu.ro/bac/list/candidate.csv";
let limit=2500
fetch(url + "?_dc=" + (new Date).getTime(), {
    method: 'post',
    credentials: 'same-origin',
    headers: {
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
        "X-Requested-With": "XMLHttpRequest"
    },
    body: "generatorKey=EEAQG&filter=%5B%5D&page=1&sort=%5B%5D&start=0&limit="+limit+"&requestParams=%7B%7D&viewName=ExamA&fileName=Rezultate+proba+E%29a%29_27-06-2018&reportTitle=Rezultate+proba+E%29a%29"
})
.then(res=>res.text())
.then(csvText=>{
  let blob = new Blob(["\uFEFF"+csvText], {
      type: "text/csv;charset=utf-8"
  });
  let url = URL.createObjectURL(blob);
  let a = document.createElement('a');
  a.download = "exportEa.csv";
  a.href = url;
  a.textContent = "Download file";
  a.click();
})
.catch(err=>console.error(err));
