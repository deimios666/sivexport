(async () => {
  const authToken = "";
  const nrDevices = 1316;
  const listUrl = "https://www.mdm-edu.ro/be-mdm/api/Device/GetDevices";
  const listParam = `{"size":${nrDevices},"from":0,"filterList":[],"sortList":[{"columnName":"EnrollDate","order":"DESC"}],"searchAfterList":"00000000-0000-0000-0000-000000000000"}`;

  
  const csvData = [];

  const listResult = await fetch(listUrl, {
    method: "post",
    credentials: "same-origin",
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json-patch+json",
      "X-AUTH-TOKEN": authToken
    },
    body: listParam,
  });

  const listText = await listResult.text();
  const listJSON = JSON.parse(JSON.parse(listText));

  console.log('Found ' + listJSON.length + ' devices');

  for (const device of listJSON) {
    console.log(`${device.HardwareUniqueId}/${device.DeviceId}`);
    csvData.push(`${device.HardwareUniqueId},${device.SIMNumber}`);
  }
  const fileName = "export_imei_tel.csv";
  const buffer = "\uFEFF" + "IMEI,TEL" + "\n" + csvData.join("\n");
  const blob = new Blob([buffer], {
    type: "text/csv;charset=utf8;",
  });

  const url = window.URL.createObjectURL(blob);
  const a = document.createElement("a");
  a.href = url;
  a.download = fileName;
  a.click();
}
)();
