(async () => {
  const authToken = "";
  const nrDevices = 1316;
  const listUrl = "https://www.mdm-edu.ro/be-mdm/api/Device/GetDevices";
  const listParam = `{"size":${nrDevices},"from":0,"filterList":[],"sortList":[{"columnName":"EnrollDate","order":"DESC"}],"searchAfterList":"00000000-0000-0000-0000-000000000000"}`;

  const deviceUrlPrefix = "https://www.mdm-edu.ro/be-mdm/api/Device/GetDevice/";

  const csvData = [];

  const listResult = await fetch(listUrl, {
    method: "post",
    credentials: "same-origin",
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json-patch+json",
      "X-AUTH-TOKEN": authToken
    },
    body: listParam,
  });

  const listText = await listResult.text();
  const listJSON = JSON.parse(JSON.parse(listText));

  console.log('Found ' + listJSON.length + ' devices');

  for (const device of listJSON) {
    console.log(`${device.HardwareUniqueId}/${device.DeviceId}`);

    const deviceResult = await fetch(deviceUrlPrefix + device.DeviceId, {
      method: 'get',
      credentials: "same-origin",
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json; charset=utf-8",
        "X-AUTH-TOKEN": authToken
      }
    });

    const deviceText = await deviceResult.text();
    const deviceJSON = JSON.parse(JSON.parse(deviceText));
    const ourDevice = deviceJSON[0];
    const deviceId = ourDevice.DeviceId;
    const lastCommDate = ourDevice.LastCommDate;
    const imei = ourDevice.HardwareUniqueId;
    csvData.push(`${imei},${deviceId},${lastCommDate}`);
  }
  const fileName = "export_device_connection.csv";
  const buffer = "\uFEFF" + "IMEI,ID,LAST_ONLINE" + "\n" + csvData.join("\n");
  const blob = new Blob([buffer], {
    type: "text/csv;charset=utf8;",
  });

  const url = window.URL.createObjectURL(blob);
  const a = document.createElement("a");
  a.href = url;
  a.download = fileName;
  a.click();
}
)();
