(async () => {
  const pageLimit = 350; //how many records can be downloaded at once. This varies based on server load, so start small. One request must not exceed 30 seconds.

  const schoolYearId = AMS.util.AppConfig.selectedYear.data.id;
  const listUrl = "https://siiir.edu.ro/siiir/list/school.json";
  const listParams = `generatorKey=ASQG&filter=%5B%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A${schoolYearId}%7D`;

  const headers = ["ID", "Tip Unitate", "Mediul", "CIF", "Cod", "Nr HCL", "Data HCL", "Denumire", "Statut", "Cod PJ", "Localitatea", "Localitatea Superioară", "Strada", "Nr", "Telefon", "Fax", "Email"];
  const values = ["id", "schoolType.description", "locality.environment.description", "fiscalCode", "code", "hclNo", "hclDate", "longName", "statut.code", "parentSchool.code", "locality.description", "locality.parentLocality.description", "street", "streetNumber", "phoneNumber", "faxNumber", "email"];

  //restore console
  let iFrame = document.createElement('iframe');
  iFrame.style.display = 'none';
  document.body.appendChild(iFrame);
  window.console = iFrame.contentWindow.console;

  const siiirFetch = async (url, body) => fetch(`${url}?_dc=${new Date().getTime()}`, {
    method: "post",
    credentials: "same-origin",
    headers: {
      Accept: "application/json",
      "Content-type": "application/x-www-form-urlencoded",
    },
    body: body,
  });

  const csvData = [];

  //get nr of results
  const primingListResult = await siiirFetch(listUrl, listParams + "&limit=1&page=1&start=0");

  const primingListJSON = await primingListResult.json();

  const maxItems = primingListJSON.page.total;
  const maxPages = Math.ceil(maxItems / pageLimit); //calculate nr of pages

  for (let page = 1; page <= maxPages; page++) {

    const start = (page - 1) * pageLimit;

    console.log(`Processing page ${page} of ${maxPages}`);

    const listResult = await siiirFetch(listUrl, listParams + `&limit=${pageLimit}&page=${page}&start=${start}`);

    const listJSON = await listResult.json();

    //do the actions
    for (const item of listJSON.page.content) {
      const oneCSVLine = values.map(key => {
        try {
          const val = key.split('.').reduce((o, i) => o[i], item);
          if (typeof val == "string") {
            return `"${val.replaceAll('"', '""')}"`;
          }
          if (val == null) {
            return "";
          } else {
            return `"${val}"`;
          }
        } catch (err) {
          return "";
        }
      }).join(",");
      csvData.push(oneCSVLine);
      console.log("Processed: " + item.longName);
    }
  }
  console.log("DONE!");
  const dateString = (new Date()).toISOString().substring(0, 10);
  const fileName = `export_retea_${dateString}.csv`;
  const buffer = "\uFEFF" + headers.join(",") + "\n" + csvData.join("\n");
  const blob = new Blob([buffer], {
    type: "text/csv;charset=utf8;",
  });
  const url = window.URL.createObjectURL(blob);
  const a = document.createElement("a");
  a.href = url;
  a.download = fileName;
  a.click();
})();