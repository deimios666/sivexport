(async () => {
    //this script presses the "Promovează" button on all schools
    const pageSize = 500;
    const schoolYearId = AMS.util.AppConfig.selectedYear.data.id;
    const listUrl = "/siiir/list/school.json";
    const listParamString = `generatorKey=ASQG&filter=%5B%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A${schoolYearId}%7D`;
    const actionUrl = "/siiir/applyModificationsToFutureSchool.json"

    //restore console
    let iFrame = document.createElement("iframe");
    iFrame.style.display = "none";
    document.body.appendChild(iFrame);
    window.console = iFrame.contentWindow.console;

    const siiirFetch = async (url, body) => fetch(`${url}?_dc=${new Date().getTime()}`, {
        method: "post",
        credentials: "same-origin",
        headers: {
            Accept: "application/json",
            "Content-type": "application/x-www-form-urlencoded",
        },
        body: body,
    });

    const primingListResponse = await siiirFetch(`${listUrl}`, `${listParamString}&limit=1&start=0&page=1`);
    const primingListJSON = await primingListResponse.json();
    const maxResults = primingListJSON.page.total;
    let currentResult = 0;

    const maxPages = Math.ceil(maxResults / pageSize);

    for (let page = 1; page <= maxPages; page++) {
        const start = (page - 1) * pageSize;
        console.log(`Downloading page ${page}/${maxPages}`);
        const schoolListResponse = await siiirFetch(`${listUrl}`, `${listParamString}&limit=${pageSize}&start=${start}&page=${page}`);
        const schoolListJSON = await schoolListResponse.json();
        const schoolList = schoolListJSON.page.content;
        for (const school of schoolList) {
            currentResult++;
            console.log(`[${currentResult}/${maxResults}] processing`);
            await siiirFetch(`${actionUrl}`, `id=${school.id}`);
        }
    }
    console.log("===DONE===");
})();