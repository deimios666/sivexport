(async () => {
    //user config
    const pageSize = 325; //how many records can be downloaded at once. This varies based on server load, so start small. One request must not exceed 30 seconds.

    const schoolYearId = AMS.util.AppConfig.selectedYear.data.id; //MAKE SURE TO SELECT NEXT YEAR
    const listUrl = "/siiir/list/school.json";
    const listParamString = `generatorKey=ASQG&filter=%5B%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A${schoolYearId}%7D`;
    const levelListUrl = "/siiir/list/asocSchoolLevel.json";
    const generateLevelListParam = (id) => `generatorKey=asocSchoolLevel_GRID_GENERATOR_KEY&filter=%5B%7B%22property%22%3A%22school.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B${id}%5D%7D%5D&sort=%5B%7B%22property%22%3A%22level.orderBy%22%2C%22direction%22%3A%22ASC%22%7D%5D&page=1&start=0&limit=100&requestParams=%7B%22schoolYearId%22%3A${schoolYearId}%7D`
    const generatePutActionUrl = (id) => `/siiir/management/school/${id}?schoolYearId=${schoolYearId}`

    const newData = {};

    //newData["siiircode"] = { street: "Principală", streetNumber: "1", postalCode: "520004", phoneNumber: "0267316088", faxNumber: "0267316088", email: "botose@ccdcovasna.ro" };
    newData["1431100167"]={"street":"Victor Babeș","streetNumber":" 18/A","postalCode":"520004","phoneNumber":"0267316088","faxNumber":"0267314274","email":"ccd.covasna@gmail.com"}
    newData["1481100025"]={"street":"Victor Babeș ","streetNumber":" 4/C","postalCode":"520004","phoneNumber":"0367412080","faxNumber":"0367412081","email":"clubulscolarsportiv2008@gmail.com"}
    newData["1461100101"]={"street":"Kós Károly","streetNumber":"22-24","postalCode":"520055","phoneNumber":"0267314571","faxNumber":"0267312984","email":"c.n.mihaiviteazul@gmail.com"}
    newData["1461100431"]={"street":"Kanta","streetNumber":"23","postalCode":"525400","phoneNumber":"0267361957","faxNumber":"0267361957","email":"nmg1680@yahoo.com"}
    newData["1482103566"]={"street":"Kanta","streetNumber":"23","postalCode":"525400","phoneNumber":"0267361957","faxNumber":"0267361957","email":"nmg1680@yahoo.com"}
    newData["1461100603"]={"street":" Oltului","streetNumber":"36-38","postalCode":"520027","phoneNumber":"0267313251","faxNumber":"0267313251","email":"benedekovi@yahoo.com"}
    newData["1461100639"]={"street":"Trandafirilor","streetNumber":"16","postalCode":"525100","phoneNumber":"0267377272","faxNumber":"0267377272","email":"cimbori@gmail.com"}
    newData["1462103606"]={"street":"Trandafirilor","streetNumber":"16","postalCode":"525100","phoneNumber":"0267377272","faxNumber":"0267377272","email":"cimbori@gmail.com"}
    newData["1462102917"]={"street":"Trandafirilor","streetNumber":"16","postalCode":"525100","phoneNumber":"0267377272","faxNumber":"0267377272","email":"cimbori@gmail.com"}
    newData["1461100485"]={"street":"Kriza János","streetNumber":"1","postalCode":"520023","phoneNumber":"0267312271","faxNumber":"0267312271","email":"gradinitacsipike@yahoo.com"}
    newData["1461100119"]={"street":"Dealului","streetNumber":"24 ","postalCode":"520060","phoneNumber":"0267318903","faxNumber":"0267318903","email":"gulliverovoda@gmail.com"}
    newData["1461100517"]={"street":"Tineretului","streetNumber":"2","postalCode":"520064","phoneNumber":"0267312341","faxNumber":"0267312341","email":"hofeherovi@yahoo.com"}
    newData["1462103339"]={"street":"Orbán Balázs","streetNumber":"4","postalCode":"520016","phoneNumber":"0767529967","faxNumber":"0267312341","email":"hofeherovi@yahoo.com"}
    newData["1461100128"]={"street":"Piliske","streetNumber":"4-6","postalCode":"525200","phoneNumber":"0267340437","faxNumber":"0267340437","email":"gppcovasna@gmail.com"}
    newData["1462103583"]={"street":"Piliske","streetNumber":"4-6","postalCode":"525200","phoneNumber":"0267340437","faxNumber":"0267340437","email":"gppcovasna@gmail.com"}
    newData["1462101848"]={"street":"Piliske","streetNumber":"4-6","postalCode":"525200","phoneNumber":"0267340437","faxNumber":"0267340437","email":"gppcovasna@gmail.com"}
    newData["1461100137"]={"street":"Ștefan cel Mare","streetNumber":"40","postalCode":"525200","phoneNumber":"0267340863","faxNumber":"0267340863","email":"korosicss@yahoo.com"}
    newData["1462101812"]={"street":"Kőrösi Csoma Sándor","streetNumber":"16","postalCode":"525201","phoneNumber":"0267340863","faxNumber":"0267340863","email":"korosicss@yahoo.com"}
    newData["1462101821"]={"street":"Kőrösi Csoma Sándor","streetNumber":"16","postalCode":"525201","phoneNumber":"0267340863","faxNumber":"0267340863","email":"korosicss@yahoo.com"}
    newData["1462101803"]={"street":"Gábor Áron","streetNumber":"1","postalCode":"525200","phoneNumber":"0267340962","faxNumber":"0267340863","email":"korosicss@yahoo.com"}
    newData["1461100779"]={"street":"Crângului ","streetNumber":"30","postalCode":"520032","phoneNumber":"0267318429","faxNumber":"0267318429","email":"office@lec.educv.ro"}
    newData["1461100381"]={"street":"Ady Endre","streetNumber":"20","postalCode":"525400","phoneNumber":"0267360670","faxNumber":"0267360670","email":"bodpeter@hotmail.com"}
    newData["1461100422"]={"street":"Școlii","streetNumber":"11","postalCode":"525400","phoneNumber":"0267361332","faxNumber":"0267361332","email":"peter.apor@gmail.com"}
    newData["1461100797"]={"street":"Libertății","streetNumber":"11","postalCode":"520055","phoneNumber":"0267313147","faxNumber":"0267313147","email":"brancusi4000@yahoo.com"}
    newData["1461100544"]={"street":"Gheorghe Doja","streetNumber":"5","postalCode":"525300","phoneNumber":"0267370728","faxNumber":"0267370243","email":"ltbalcescu@yahoo.com"}
    newData["1461100286"]={"street":"Ady Endre","streetNumber":"9","postalCode":"525400","phoneNumber":"0267365045","faxNumber":"0267365045","email":"refkol@yahoo.com"}
    newData["1462101296"]={"street":"Dózsa György","streetNumber":"8","postalCode":"525400","phoneNumber":"0267365045","faxNumber":"0267365045","email":"refkol@yahoo.com"}
    newData["1462100883"]={"street":"Şcolii","streetNumber":"6","postalCode":"525400","phoneNumber":"0267365045","faxNumber":"0267365045","email":"refkol@yahoo.com"}
    newData["1471100594"]={"street":"Kőrösi Csoma Sándor","streetNumber":"19","postalCode":"520009","phoneNumber":"0267315678","faxNumber":"0267315678","email":"pcsfantugheorghe@gmail.com"}
    newData["1452103067"]={"street":"Kossuth Lajos","streetNumber":"52","postalCode":"525100","phoneNumber":"0267377502","faxNumber":"0267377502","email":"ccbaraolt@gmail.com"}
    newData["1452103058"]={"street":"Gheorghe Doja ","streetNumber":"7","postalCode":"525300","phoneNumber":"0267370364","faxNumber":"0267370364","email":"clubintbuz@yahoo.com"}
    newData["1461100553"]={"street":"Principală","streetNumber":"81","postalCode":"527010","phoneNumber":"0267372325","faxNumber":"0267372325","email":"scoalabarcani@yahoo.com"}
    newData["1462103167"]={"street":"Principală","streetNumber":"79","postalCode":"527010","phoneNumber":"0267372325","faxNumber":"0267372325","email":"scoalabarcani@yahoo.com"}
    newData["1462103176"]={"street":"Principală","streetNumber":"74","postalCode":"527011","phoneNumber":"0267372325","faxNumber":"0267372325","email":"scoalabarcani@yahoo.com"}
    newData["1462103149"]={"street":"Principală","streetNumber":"79","postalCode":"527012","phoneNumber":"0267372325","faxNumber":"0267372325","email":"scoalabarcani@yahoo.com"}
    newData["1461100232"]={"street":"Școlii","streetNumber":"433","postalCode":"527145","phoneNumber":"0267373814","faxNumber":"0267373814","email":"antosjanosiskola@freemail.hu"}
    newData["1462101554"]={"street":"Principală","streetNumber":"26","postalCode":"527146","phoneNumber":"0267374026","faxNumber":"0267373814","email":" antosjanosiskola@freemail.hu"}
    newData["1462101545"]={"street":"Principală","streetNumber":"26","postalCode":"527146","phoneNumber":"0267374026","faxNumber":"0267373814","email":"antosjanosiskola@freemail.hu"}
    newData["1462101527"]={"street":"Principală","streetNumber":"304","postalCode":"527145","phoneNumber":"0267373808","faxNumber":"0267373814","email":"antosjanosiskola@freemail.hu"}
    newData["1461100336"]={"street":"Mihai Eminescu","streetNumber":"15","postalCode":"525200","phoneNumber":"0267340162","faxNumber":"0267340162","email":"scavramiancuvoinesti@yahoo.com"}
    newData["1462103592"]={"street":"Ștefan cel Mare","streetNumber":"102","postalCode":"525200","phoneNumber":"0267340191","faxNumber":"0267340162","email":"scavramiancuvoinesti@yahoo.com"}
    newData["1462101052"]={"street":"Ștefan cel Mare","streetNumber":"102","postalCode":"525200","phoneNumber":"0267340191","faxNumber":"0267340162","email":"scavramiancuvoinesti@yahoo.com"}
    newData["1461100277"]={"street":"Principală","streetNumber":"192","postalCode":"527065","phoneNumber":"0267346209","faxNumber":"0267346209","email":"scoalabalintgabor@yahoo.com"}
    newData["1462101346"]={"street":"Principală","streetNumber":"192","postalCode":"527065","phoneNumber":"0267346209","faxNumber":"0267346209","email":"scoalabalintgabor@yahoo.com"}
    newData["1462101301"]={"street":"Principală","streetNumber":"143","postalCode":"527066","phoneNumber":"0267346209","faxNumber":"0267346209","email":"scoalabalintgabor@yahoo.com"}
    newData["1462101337"]={"street":"Principală","streetNumber":"143","postalCode":"527066","phoneNumber":"0267346209","faxNumber":"0267346209","email":"scoalabalintgabor@yahoo.com"}
    newData["1462101364"]={"street":"Principală","streetNumber":"58","postalCode":"527067","phoneNumber":"0267346209","faxNumber":"0267346209","email":"scoalabalintgabor@yahoo.com"}
    newData["1462101328"]={"street":"Principală","streetNumber":"58","postalCode":"527067","phoneNumber":"0267345629","faxNumber":"0267346209","email":"scoalabalintgabor@yahoo.com"}
    newData["1462101355"]={"street":"Principală","streetNumber":"218","postalCode":"527068","phoneNumber":"0267346209","faxNumber":"0267346209","email":"scoalabalintgabor@yahoo.com"}
    newData["1462101373"]={"street":"Principală","streetNumber":"218","postalCode":"527068","phoneNumber":"0267346209","faxNumber":"0267346209","email":"scoalabalintgabor@yahoo.com"}
    newData["1462101319"]={"street":"Principală","streetNumber":"114","postalCode":"527069","phoneNumber":"0267346209","faxNumber":"0267346209","email":"scoalabalintgabor@yahoo.com"}
    newData["1462101382"]={"street":"Principală","streetNumber":"114","postalCode":"527069","phoneNumber":"0267365716","faxNumber":"0267346209","email":"scoalabalintgabor@yahoo.com"}
    newData["1461100535"]={"street":"Aleea Parcului","streetNumber":"394","postalCode":"527040","phoneNumber":"0267374160","faxNumber":"0267374160","email":"barthakarolygimnazium@yahoo.com"}
    newData["1462103235"]={"street":"Principală ","streetNumber":"393","postalCode":"527040","phoneNumber":"0267374160","faxNumber":"0267374160","email":"barthakarolygimnazium@yahoo.com"}
    newData["1462103194"]={"street":"Principală","streetNumber":"122","postalCode":"527041","phoneNumber":"0267374160","faxNumber":"0267374160","email":"barthakarolygimnazium@yahoo.com"}
    newData["1462103208"]={"street":"Principală","streetNumber":"122","postalCode":"527041","phoneNumber":"0267374160","faxNumber":"0267374160","email":"barthakarolygimnazium@yahoo.com"}
    newData["1462103226"]={"street":"Principală","streetNumber":"74","postalCode":"527042","phoneNumber":"0267374160","faxNumber":"0267374160","email":"barthakarolygimnazium@yahoo.com"}
    newData["1462103217"]={"street":"Principală","streetNumber":"74","postalCode":"527042","phoneNumber":"0267374160","faxNumber":"0267374160","email":"barthakarolygimnazium@yahoo.com"}
    newData["1462103253"]={"street":"Principală","streetNumber":"83","postalCode":"527043","phoneNumber":"0267374160","faxNumber":"0267374160","email":"barthakarolygimnazium@yahoo.com"}
    newData["1462103271"]={"street":"Principală","streetNumber":"1","postalCode":"527043","phoneNumber":"0267374160","faxNumber":"0267374160","email":"barthakarolygimnazium@yahoo.com"}
    newData["1462103244"]={"street":"Principala","streetNumber":"83","postalCode":"527043","phoneNumber":"0267374160","faxNumber":"0267374160","email":"barthakarolygimnazium@yahoo.com"}
    newData["1462103262"]={"street":"Principală","streetNumber":"1","postalCode":"527043","phoneNumber":"0267374160","faxNumber":"0267374160","email":"barthakarolygimnazium@yahoo.com"}
    newData["1461100069"]={"street":"Principală","streetNumber":"158","postalCode":"527110","phoneNumber":"0267369213","faxNumber":"0267369213","email":"bem_suli@yahoo.com"}
    newData["1462102026"]={"street":"Principală","streetNumber":"158","postalCode":"527110","phoneNumber":"0267369213","faxNumber":"0267369213","email":"bem_suli@yhoo.com"}
    newData["1462102035"]={"street":"Principală","streetNumber":"497","postalCode":"527110","phoneNumber":"0267369181","faxNumber":"0267369213","email":"bem_suli@yhoo.com"}
    newData["1462102044"]={"street":"Principală","streetNumber":"497","postalCode":"527110","phoneNumber":"0267369181","faxNumber":"0267369213","email":"bem_suli@yahoo.com"}
    newData["1461100191"]={"street":"Școlii","streetNumber":"172","postalCode":"527055","phoneNumber":"0267376419","faxNumber":"0267376419","email":"scoalabradut@yahoo.com"}
    newData["1462101649"]={"street":"Școlii","streetNumber":"171","postalCode":"527055","phoneNumber":"0267376419","faxNumber":"0267376419","email":"scoalabradut@yahoo.com"}
    newData["1462101631"]={"street":"Principală","streetNumber":"26","postalCode":"520756","phoneNumber":"0267376458","faxNumber":"0267376458","email":"scoalabradut@yahoo.com"}
    newData["1462101667"]={"street":"Principală","streetNumber":"26","postalCode":"527056","phoneNumber":"0267376458","faxNumber":"0267376458","email":"scoalabradut@yahoo.com"}
    newData["1462101676"]={"street":"Principală","streetNumber":"388","postalCode":"527057","phoneNumber":"0267376489","faxNumber":"0267376489","email":"scoalabradut@yahoo.com"}
    newData["1462101622"]={"street":"Principală","streetNumber":"330","postalCode":"527057","phoneNumber":"0267376489","faxNumber":"0267376489","email":"scoalabradut@yahoo.com"}
    newData["1462101613"]={"street":"Principală","streetNumber":"198","postalCode":"527058","phoneNumber":"0267376419","faxNumber":"0267376419","email":"scoalabradut@yahoo.com"}
    newData["1462101658"]={"street":"Principală","streetNumber":"65/A","postalCode":"527058","phoneNumber":"0267376203","faxNumber":"0267376419","email":"scoalabradut@yahoo.com"}
    newData["1462100942"]={"street":"Principală","streetNumber":"390","postalCode":"527030","phoneNumber":"0267355828","faxNumber":"0267355828","email":"scoala.belin@yahoo.com"}
    newData["1462100951"]={"street":"Principală","streetNumber":"110","postalCode":"527031","phoneNumber":"0267355763","faxNumber":"0000000000","email":"scoala.belin@yahoo.com"}
    newData["1462100969"]={"street":"Principală","streetNumber":"113","postalCode":"527031","phoneNumber":"0267355763","faxNumber":"0000000000","email":"scoala.belin@yahoo.com"}
    newData["1461100621"]={"street":"Școlii","streetNumber":"521","postalCode":"527060","phoneNumber":"0267368028","faxNumber":"0267368028","email":"sccomenius@yahoo.com"}
    newData["1462102971"]={"street":"Principală","streetNumber":"136","postalCode":"527060","phoneNumber":"0267368028","faxNumber":"0267368028","email":"sccomenius@yahoo.com"}
    newData["1462102926"]={"street":"Școlii ","streetNumber":"521","postalCode":"527060","phoneNumber":"0267368028","faxNumber":"0267368028","email":"sccomenius@yahoo.com"}
    newData["1462102935"]={"street":"Principală","streetNumber":"154","postalCode":"527061","phoneNumber":"0267368028","faxNumber":"0267368028","email":"sccomenius@yahoo.com"}
    newData["1462102953"]={"street":"Principală","streetNumber":"154","postalCode":"527061","phoneNumber":"0267368028","faxNumber":"0267368028","email":"sccomenius@yahoo.com"}
    newData["1462102944"]={"street":"Principală","streetNumber":"11","postalCode":"527062","phoneNumber":"0267368028","faxNumber":"0267368028","email":"sccomenius@yahoo.com"}
    newData["1462102962"]={"street":"Principală","streetNumber":"11","postalCode":"527062","phoneNumber":"0267368028","faxNumber":"0267368028","email":"sccomenius@yahoo.com"}
    newData["1461100707"]={"street":"Principală","streetNumber":"106","postalCode":"527095","phoneNumber":"0267353726","faxNumber":"0267353726","email":"czetzjanos@yahoo.com"}
    newData["1462102605"]={"street":"Principală","streetNumber":"106","postalCode":"527095","phoneNumber":"0267353726","faxNumber":"0267353726","email":"czetzjanos@yahoo.com"}
    newData["1461100205"]={"street":"Principală","streetNumber":"229","postalCode":"527121","phoneNumber":"0267345841","faxNumber":"0267345841","email":"darkoj.iskola@gmail.com"}
    newData["1462101604"]={"street":"Principală","streetNumber":"229","postalCode":"527121","phoneNumber":"0267345841","faxNumber":"0267345841","email":"darkoj.iskola@gmail.com"}
    newData["1461100399"]={"street":"Br. Szentkereszthy Béla","streetNumber":"34","postalCode":"527166","phoneNumber":"0267373596","faxNumber":"0267373596","email":"geleijozsef@yahoo.com"}
    newData["1462100978"]={"street":"Br. Szentkereszty Béla","streetNumber":"50","postalCode":"527166","phoneNumber":"0267373596","faxNumber":"0267373596","email":"geleijozsef@yahoo.com"}
    newData["1461100363"]={"street":"Principală","streetNumber":"299","postalCode":"527115","phoneNumber":"0267344447","faxNumber":"0267344447","email":"fejerakos@yahoo.com"}
    newData["1462101016"]={"street":"Csere","streetNumber":"255","postalCode":"527115","phoneNumber":"0267344299","faxNumber":"0267344447","email":"fejerakos@yahoo.com"}
    newData["1461100327"]={"street":"Kossuth Lajos","streetNumber":"172","postalCode":"525100","phoneNumber":"0267377734","faxNumber":"0267377761","email":"scoalagaalmozes@gmail.com"}
    newData["1462101079"]={"street":"Principală","streetNumber":"202","postalCode":"525101","phoneNumber":"0267377734","faxNumber":"0267377761","email":"scoalagaalmozes@gmail.com"}
    newData["1462101129"]={"street":"Principală","streetNumber":"202","postalCode":"525101","phoneNumber":"0267377734","faxNumber":"0267377761","email":"scoalagaalmozes@gmail.com"}
    newData["1462101102"]={"street":"Principală","streetNumber":"115","postalCode":"525102","phoneNumber":"0267377734","faxNumber":"0267377761","email":"scoalagaalmozes@gmail.com"}
    newData["1462101111"]={"street":"Principală","streetNumber":"115","postalCode":"525102","phoneNumber":"0267377734","faxNumber":"0267377761","email":"scoalagaalmozes@gmail.com"}
    newData["1462101097"]={"street":"Principală","streetNumber":"96","postalCode":"525104","phoneNumber":"0267377734","faxNumber":"0267377761","email":"scoalagaalmozes@gmail.com"}
    newData["1462101147"]={"street":"Principală","streetNumber":"96","postalCode":"525104","phoneNumber":"0267377734","faxNumber":"0267377761","email":"scoalagaalmozes@gmail.com"}
    newData["1462101156"]={"street":"Principală ","streetNumber":"228","postalCode":"525105","phoneNumber":"0267377734","faxNumber":"0267377761","email":"scoalagaalmozes@gmail.com"}
    newData["1462101088"]={"street":"Principală","streetNumber":"228","postalCode":"525105","phoneNumber":"0267377734","faxNumber":"0267377761","email":"scoalagaalmozes@gmail.com"}
    newData["1461100458"]={"street":"Europa","streetNumber":"99","postalCode":"527075","phoneNumber":"0267347019","faxNumber":"0267347019","email":"sc_chichis@yahoo.com"}
    newData["1462100915"]={"street":"Principală","streetNumber":"120","postalCode":"527076","phoneNumber":"0267347019","faxNumber":"0267347019","email":"sc_chichis@yahoo.com"}
    newData["1462100933"]={"street":"Principală","streetNumber":"141","postalCode":"527076","phoneNumber":"0267347019","faxNumber":"0267347019","email":"sc_chichis@yahoo.com"}
    newData["1462100924"]={"street":"Europa","streetNumber":"72","postalCode":"527075","phoneNumber":"0267347019","faxNumber":"0267347019","email":"sc_chichis@yahoo.com"}
    newData["1461100223"]={"street":"Aleea Elevilor","streetNumber":"1","postalCode":"520038","phoneNumber":"0367407421","faxNumber":"0367407421","email":"godriferenc@yahoo.com"}
    newData["1462101599"]={"street":"Romulus Cioflec","streetNumber":"14","postalCode":"520020","phoneNumber":"0367408246","faxNumber":"0367408246","email":"godriferenc@yahoo.com"}
    newData["1461100146"]={"street":"Principală","streetNumber":"41","postalCode":"527080","phoneNumber":"0267353106","faxNumber":"0267353106","email":"komisk68@yahoo.com"}
    newData["1461100526"]={"street":"Principală","streetNumber":"739","postalCode":"527090","phoneNumber":"0267345007","faxNumber":"0267345007","email":"scoala_ghelinta@yahoo.com"}
    newData["1462103303"]={"street":"Principală","streetNumber":"1124","postalCode":"527090","phoneNumber":"0267345156","faxNumber":"0267345007","email":"scoala_ghelinta@yahoo.com"}
    newData["1462103321"]={"street":"Principală","streetNumber":"725","postalCode":"527090","phoneNumber":"0267345063","faxNumber":"0267345007","email":"scoala_ghelinta@yahoo.com"}
    newData["1462103312"]={"street":"Principală","streetNumber":"1124","postalCode":"527090","phoneNumber":"0267345156","faxNumber":"0267345007","email":"scoala_ghelinta@yahoo.com"}
    newData["1462103298"]={"street":"Principală","streetNumber":"7","postalCode":"527091","phoneNumber":"0267345007","faxNumber":"0267345007","email":"scoala_ghelinta@yahoo.com"}
    newData["1462103289"]={"street":"Principală","streetNumber":"7","postalCode":"527091","phoneNumber":"0267345007","faxNumber":"0267345007","email":"scoala_ghelinta@yahoo.com"}
    newData["1461100856"]={"street":"Școlii","streetNumber":"154","postalCode":"527165","phoneNumber":"0267374510","faxNumber":"0267374510","email":"kalnokyludmilla@yahoo.com"}
    newData["1462102166"]={"street":"Bedo Albert","streetNumber":"158","postalCode":"527167","phoneNumber":"0267374510","faxNumber":"0267374510","email":"kalnokyludmilla@yahoo.com"}
    newData["1462102157"]={"street":"Bedo Albert","streetNumber":"158","postalCode":"527167","phoneNumber":"0267374510","faxNumber":"0267374510","email":"kalnokyludmilla@yahoo.com"}
    newData["1462102175"]={"street":"Școlii","streetNumber":"154","postalCode":"527165","phoneNumber":"0267374510","faxNumber":"0267374510","email":"kalnokyludmilla@yahoo.com"}
    newData["1461100318"]={"street":"Baksa","streetNumber":"139","postalCode":"527112","phoneNumber":"0267369610","faxNumber":"0267369610","email":"almasuli@yahoo.com"}
    newData["1462101183"]={"street":"Şcolii","streetNumber":"188","postalCode":"527111","phoneNumber":"0267369613","faxNumber":"0267369610","email":"almasuli@yahoo.com"}
    newData["1462101174"]={"street":"Baksa","streetNumber":"139","postalCode":"527112","phoneNumber":"0267369610","faxNumber":"0267369610","email":"almasuli@yahoo.com"}
    newData["1462101165"]={"street":"Şcolii","streetNumber":"188","postalCode":"527111","phoneNumber":"0267369613","faxNumber":"0267369610","email":"almasuli@yahoo.com"}
    newData["1461100734"]={"street":"Principală","streetNumber":"833","postalCode":"527160","phoneNumber":"0267367007","faxNumber":"0267367007","email":"kicsiantal@gmail.com"}
    newData["1462102564"]={"street":"Principală","streetNumber":"833","postalCode":"527160","phoneNumber":"0267367007","faxNumber":"0267367007","email":"kicsiantal@gmail.com"}
    newData["1461100562"]={"street":"Principală","streetNumber":"487","postalCode":"527020","phoneNumber":"0267355425","faxNumber":"0267355425","email":"ervinkv@yahoo.com"}
    newData["1461100838"]={"street":"Principală","streetNumber":"96","postalCode":"527105","phoneNumber":"0267347643","faxNumber":"0267347643","email":"laszlolukacsiskola@gmail.com"}
    newData["1462102184"]={"street":"Principală","streetNumber":"89","postalCode":"527105","phoneNumber":"0267347643","faxNumber":"0267347643","email":"laszlolukacsiskola@gmail.com"}
    newData["1461100675"]={"street":"Mihai Viteazul","streetNumber":"188","postalCode":"525300","phoneNumber":"0267370496","faxNumber":"0267371491","email":"scoalaib@yahoo.com"}
    newData["1462102754"]={"street":"Principală","streetNumber":"66","postalCode":"525301","phoneNumber":"0267370406","faxNumber":"0267371491","email":"scoalabradet@yahoo.com"}
    newData["1462102695"]={"street":"Principală","streetNumber":"66","postalCode":"525301","phoneNumber":"0267370406","faxNumber":"0267371491","email":"scoalabradet@yahoo.com"}
    newData["1462103615"]={"street":"Gheorghe Doja","streetNumber":"11","postalCode":"525300","phoneNumber":"0267370779","faxNumber":"0267370779","email":"gpp_intorsura@yahoo.com"}
    newData["1462103407"]={"street":"Aviatorului","streetNumber":"11","postalCode":"525300","phoneNumber":"0267370910","faxNumber":"0267371491","email":"scoalaib@yahoo.com"}
    newData["1462102686"]={"street":"Gheorghe Doja","streetNumber":"11","postalCode":"525300","phoneNumber":"0267370779","faxNumber":"0267370779","email":"gpp_intorsura@yahoo.com"}
    newData["1462102736"]={"street":"Gheorghe Doja","streetNumber":"11","postalCode":"525300","phoneNumber":"0267370779","faxNumber":"0267370779","email":"gpp_intorsura@yahoo.com"}
    newData["1462103393"]={"street":"Aviatorului","streetNumber":"11","postalCode":"525300","phoneNumber":"0267370910","faxNumber":"0267371491","email":"scoalaib@yahoo.com"}
    newData["1461100657"]={"street":"Ciucului","streetNumber":"538","postalCode":"527116","phoneNumber":"0267365253","faxNumber":"0267365253","email":"scoalabixad@yahoo.com"}
    newData["1461100087"]={"street":"Principală","streetNumber":"467","postalCode":"527185","phoneNumber":"0267343054","faxNumber":"0267343054","email":"scoala.mikes@gmail.com"}
    newData["1462101025"]={"street":"Principală","streetNumber":"43","postalCode":"527186","phoneNumber":"0267343719","faxNumber":"0267343719","email":"scoala.mikes@gmail.com"}
    newData["1462101034"]={"street":"Principală","streetNumber":"408","postalCode":"527186","phoneNumber":"0267343719","faxNumber":"0267343719","email":"scoala.mikes@gmail.com"}
    newData["1462100354"]={"street":"Principală","streetNumber":"43","postalCode":"527186","phoneNumber":"0267343720","faxNumber":"0267343719","email":"scoala.mikes@gmail.com"}
    newData["1462101943"]={"street":"Principală","streetNumber":"927","postalCode":"527185","phoneNumber":"0267343054","faxNumber":"0267343054","email":"scoala.mikes@gmail.com"}
    newData["1462101952"]={"street":"Principală","streetNumber":"103","postalCode":"527185","phoneNumber":"0267343014","faxNumber":"0000000000","email":"scoala.mikes@gmail.com"}
    newData["1462101961"]={"street":"Principală","streetNumber":"927","postalCode":"527185","phoneNumber":"0267343127","faxNumber":"0267343054","email":"scoala.mikes@gmail.com"}
    newData["1461100802"]={"street":"Nemere","streetNumber":"2","postalCode":"525400","phoneNumber":"0267362691","faxNumber":"0267362691","email":"scoala@molnarjozsias.ro"}
    newData["1462103547"]={"street":"Táncsics Mihály","streetNumber":"2","postalCode":"525400","phoneNumber":"0267362691","faxNumber":"0267362691","email":"scoala@molnarjozsias.ro"}
    newData["1461100508"]={"street":"Principală","streetNumber":"165","postalCode":"527143","phoneNumber":"0267366656","faxNumber":"0267366656","email":"esztelnekiskola@yahoo.com "}
    newData["1462103357"]={"street":"Principală","streetNumber":"165","postalCode":"527143","phoneNumber":"0267366656","faxNumber":"0267366656","email":" esztelnekiskola@yahoo.com"}
    newData["1461100259"]={"street":"Lalelei","streetNumber":"7","postalCode":"520086","phoneNumber":"0267351715","faxNumber":"0267351715","email":"office@colan.ro"}
    newData["1462101468"]={"street":"Lalelei","streetNumber":"7","postalCode":"520086","phoneNumber":"0267321794","faxNumber":"0267321794","email":"pinocchionapkozi@yahoo.com"}
    newData["1461100173"]={"street":"Principală","streetNumber":"228","postalCode":"527155","phoneNumber":"0267373052","faxNumber":"0267373052","email":"scoalanrussu@yahoo.com"}
    newData["1462101726"]={"street":"Principală","streetNumber":"1227","postalCode":"527155","phoneNumber":"0267373052","faxNumber":"0267373052","email":"scoalanrussu@yahoo.com"}
    newData["1462101744"]={"street":"Principală","streetNumber":"123","postalCode":"527155","phoneNumber":"0267373052","faxNumber":"0267373052","email":"scoalanrussu@yahoo.com"}
    newData["1462101789"]={"street":"Principală","streetNumber":"509","postalCode":"527155","phoneNumber":"0267373052","faxNumber":"0267373052","email":"scoalanrussu@yahoo.com"}
    newData["1462101717"]={"street":"Principală","streetNumber":"843","postalCode":"527155","phoneNumber":"0267373052","faxNumber":"0267373052","email":"scoalanrrusu@yahoo.com"}
    newData["1462101753"]={"street":"Principală","streetNumber":"509","postalCode":"527155","phoneNumber":"0267373052","faxNumber":"0267373052","email":"scoalanrussu@yahoo.com"}
    newData["1462101762"]={"street":"Principală","streetNumber":"843","postalCode":"527155","phoneNumber":"0267373052","faxNumber":"0267373052","email":"scoalanrussu@yahoo.com"}
    newData["1462101771"]={"street":"Principală","streetNumber":"986","postalCode":"527155","phoneNumber":"0267373052","faxNumber":"0267373052","email":"scoalanrussu@yahoo.com"}
    newData["1462101735"]={"street":"Principală","streetNumber":"986","postalCode":"527155","phoneNumber":"0267373052","faxNumber":"0267373052","email":"scoalanrussu@yahoo.com"}
    newData["1461100309"]={"street":"Principală","streetNumber":"352","postalCode":"527120","phoneNumber":"0267345732","faxNumber":"0267345732","email":"orbanbalazsgimnazium@gmail.com"}
    newData["1462101206"]={"street":"Principală","streetNumber":"356","postalCode":"527120","phoneNumber":"0267345732","faxNumber":"0267345732","email":"orbanbalazsgimnazium@gmail.com"}
    newData["1462101215"]={"street":"Principală","streetNumber":"368","postalCode":"527122","phoneNumber":"0267345732","faxNumber":"0267345732","email":"orbanbalazsgimnazium@gmail.com"}
    newData["1461100345"]={"street":"Stadionului","streetNumber":"FN","postalCode":"525400","phoneNumber":"0267361250","faxNumber":"0267361250","email":"scoalapetofis@yahoo.com"}
    newData["1461100096"]={"street":"Gábor Áron","streetNumber":"79","postalCode":"527130","phoneNumber":"0267331022","faxNumber":"0267331022","email":"tatrangi.scoala@gmail.com"}
    newData["1462101934"]={"street":"Principală","streetNumber":"33","postalCode":"527131","phoneNumber":"0267331022","faxNumber":"0267331022","email":"tatrangi.scoala@gmail.com"}
    newData["1462101907"]={"street":"Principală","streetNumber":"33","postalCode":"527131","phoneNumber":"0267331022","faxNumber":"0267331022","email":"tatrangi.scoala@gmail.com"}
    newData["1462101916"]={"street":"Principală","streetNumber":"62","postalCode":"527132","phoneNumber":"0267331022","faxNumber":"0267331022","email":"tatrangi.scoala@gmail.com"}
    newData["1462101875"]={"street":"Principală","streetNumber":"62","postalCode":"527132","phoneNumber":"0267331022","faxNumber":"0267331022","email":"tatrangi.scoala@gmail.com"}
    newData["1462101866"]={"street":"Kossuth Lajos","streetNumber":"364","postalCode":"527130","phoneNumber":"0267331022","faxNumber":"0267331022","email":"tatrangi.scoala@gmail.com"}
    newData["1462101893"]={"street":"Principală","streetNumber":"247","postalCode":"527137","phoneNumber":"0267331022","faxNumber":"0267331022","email":"tatrangi.scoala@gmail.com"}
    newData["1462101925"]={"street":"Principală","streetNumber":"247","postalCode":"527137","phoneNumber":"0267331022","faxNumber":"0267331022","email":"tatrangi.scoala@gmail.com"}
    newData["1461100078"]={"street":"Principală","streetNumber":"37","postalCode":"527117","phoneNumber":"0267353873","faxNumber":"0267383873","email":"tjiskola@gmail.com"}
    newData["1462102017"]={"street":"Principală","streetNumber":"37","postalCode":"527117","phoneNumber":"0267353873","faxNumber":"0267353873","email":"tjiskola@gmail.com"}
    newData["1462101979"]={"street":"Principală","streetNumber":"27","postalCode":"527118","phoneNumber":"0267344356","faxNumber":"0267383873","email":"tjiskola@gmail.com"}
    newData["1462101997"]={"street":"Principală","streetNumber":"27","postalCode":"527118","phoneNumber":"0267344356","faxNumber":"0267383873","email":"tjiskola@gmail.com"}
    newData["1462101988"]={"street":"Principală","streetNumber":"1","postalCode":"527119","phoneNumber":"0267355415","faxNumber":"0267383873","email":"tjiskola@gmail.com"}
    newData["1462102008"]={"street":"Principală","streetNumber":"1","postalCode":"527119","phoneNumber":"0267355415","faxNumber":"0267383873","email":"tjiskola@gmail.com"}
    newData["1461100051"]={"street":"Principală","streetNumber":"149","postalCode":"527140","phoneNumber":"0267368617","faxNumber":"0267368617","email":"scoalapoian16@yahoo.com"}
    newData["1462102071"]={"street":"Principală","streetNumber":"10","postalCode":"527141","phoneNumber":"0267368617","faxNumber":"0267368617","email":"scoalapoian16@yahoo.com"}
    newData["1462102062"]={"street":"Principală","streetNumber":"10","postalCode":"527141","phoneNumber":"0267368617","faxNumber":"0267368617","email":"scoalapoian16@yahoo.com"}
    newData["1462102053"]={"street":"Principală","streetNumber":"141","postalCode":"527140","phoneNumber":"0267368617","faxNumber":"0267368617","email":"scoalapoian16@yahoo.com"}
    newData["1461100214"]={"street":"Benedek Elek","streetNumber":"20","postalCode":"520067","phoneNumber":"0267313110","faxNumber":"0267314049","email":"varadijozsefiskola@gmail.com"}
    newData["1461100295"]={"street":"Principală","streetNumber":"198","postalCode":"527085","phoneNumber":"0267375749","faxNumber":"0267375749","email":"scoaladobarlau@yahoo.com"}
    newData["1462101224"]={"street":" Principală","streetNumber":"239 A","postalCode":"527085","phoneNumber":"0267375749","faxNumber":"0267375749","email":"scoaladobarlau@yahoo.com"}
    newData["1462101269"]={"street":"Principală","streetNumber":"121","postalCode":"527086","phoneNumber":"0758781847","faxNumber":"0267375749","email":"scoaladobarlau@yahoo.com"}
    newData["1462101278"]={"street":"Principală","streetNumber":"121","postalCode":"527086","phoneNumber":"0766897397","faxNumber":"0267375749","email":"scoaladobarlau@yahoo.com"}
    newData["1462101251"]={"street":"Principală","streetNumber":"72","postalCode":"527087","phoneNumber":"0742340142","faxNumber":"0267375749","email":"scoaladobarlau@yahoo.com"}
    newData["1462101242"]={"street":"Principală","streetNumber":"72","postalCode":"527087","phoneNumber":"0749126858","faxNumber":"0267375749","email":"scoaladobarlau@yahoo.com"}
    newData["1461100752"]={"street":"Principală","streetNumber":"798","postalCode":"527190","phoneNumber":"0267375019","faxNumber":"0267375019","email":"zabisk@yahoo.com"}
    newData["1462102415"]={"street":"Principală","streetNumber":"50","postalCode":"527191","phoneNumber":"0267375019","faxNumber":"0267375019","email":"zabisk@yahoo.com"}
    newData["1462102406"]={"street":"Principală","streetNumber":"50","postalCode":"527191","phoneNumber":"0267375019","faxNumber":"0267375019","email":"zabisk@yahoo.com"}
    newData["1462102347"]={"street":"Principală","streetNumber":"27","postalCode":"527192","phoneNumber":"0267375019","faxNumber":"0267375019","email":"zabisk@yahoo.com"}
    newData["1462102374"]={"street":"Principală","streetNumber":"238","postalCode":"527192","phoneNumber":"0267375019","faxNumber":"0267375019","email":"zabisk@yahoo.com"}
    newData["1462102383"]={"street":"Principală","streetNumber":"58","postalCode":"527193","phoneNumber":"0267375019","faxNumber":"0267375019","email":"zabisk@yahoo.com"}
    newData["1462102433"]={"street":"Principală","streetNumber":"479","postalCode":"527190","phoneNumber":"0267375019","faxNumber":"0267375019","email":"zabisk@yahoo.com"}
    newData["1462102392"]={"street":"Principală","streetNumber":"479","postalCode":"527190","phoneNumber":"0267375019","faxNumber":"0267375019","email":"zabisk@yahoo.com"}
    newData["1462102356"]={"street":"Principală","streetNumber":"910","postalCode":"527190","phoneNumber":"0267375070","faxNumber":"0267375019","email":"zabisk@yahoo.com"}
    newData["1462102424"]={"street":"Principală","streetNumber":"910","postalCode":"527190","phoneNumber":"0267375070","faxNumber":"0267375019","email":"zabisk@yahoo.com"}
    newData["1461100241"]={"street":"Kálvin tér","streetNumber":"3","postalCode":"520014","phoneNumber":"0267351448","faxNumber":"0267351448","email":"scoalaspecialacv@gmail.com"}
    
    let iFrame = document.createElement("iframe");
    iFrame.style.display = "none";
    document.body.appendChild(iFrame);
    window.console = iFrame.contentWindow.console;

    const siiirFetch = async (url, body) =>
        fetch(`${url}?_dc=${new Date().getTime()}`, {
            method: "post",
            credentials: "same-origin",
            headers: {
                Accept: "application/json",
                "Content-type": "application/x-www-form-urlencoded",
            },
            body: body,
        });

    const siiirPut = async (url, body) =>
        fetch(`${url}`, {
            method: "put",
            credentials: "same-origin",
            headers: {
                Accept: "application/json",
                "Content-type": "application/json",
            },
            body: JSON.stringify(body),
        });

    const schoolsPrimingResult = await siiirFetch(listUrl, listParamString + "&page=1&start=0&limit=1");
    const schoolsPrimingJson = await schoolsPrimingResult.json();
    const nrSchools = schoolsPrimingJson.page.total;

    let currentResult = 0;
    const maxPages = Math.ceil(nrSchools / pageSize);


    for (let currentPage = 1; currentPage <= maxPages; currentPage++) {
        console.log(`Page ${currentPage} of ${maxPages}`);
        const schoolsResult = await siiirFetch(listUrl, listParamString + `&page=${currentPage}&start=${(currentPage - 1) * pageSize}&limit=${pageSize}`);
        const schoolsJson = await schoolsResult.json();
        const schools = schoolsJson.page.content;

        for (let i = 0; i < schools.length; i++) {
            const school = schools[i];
            if (newData[school.code]) {
                const payload = { ...school, ...newData[school.code] };
                if (school.parentSchool === null) {
                    //PJ
                    const levelListResult = await siiirFetch(levelListUrl, generateLevelListParam(school.id));
                    const levelListJson = await levelListResult.json();
                    const levels = levelListJson.page.content;
                    payload.schoolLevels = levels;
                }
                await siiirPut(generatePutActionUrl(school.id), payload);
            }
        }
    }

})();