//IIFE for async
(async () => {
  const pageLimit = 500;

  const schoolYearId = AMS.util.AppConfig.selectedYear.data.id;
  const listUrl = "https://www.siiir.edu.ro/siiir/list/school.json";
  const listParams = `generatorKey=ASQG&filter=%5B%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A${schoolYearId}%7D`;

  const extraListUrl = "https://www.siiir.edu.ro/siiir/list/asocSchoolLevel.json";
  const extraListParamPrefix = `generatorKey=asocSchoolLevel_GRID_GENERATOR_KEY&filter=%5B%7B%22property%22%3A%22school.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B`;
  const extraListParamSuffix = `%5D%7D%5D&sort=%5B%7B%22property%22%3A%22level.orderBy%22%2C%22direction%22%3A%22ASC%22%7D%5D&page=1&start=0&limit=25&requestParams=%7B%22schoolYearId%22%3A${schoolYearId}%7D`;

  const actionUrl = "https://www.siiir.edu.ro/siiir/management/school/";

  const hclNrDate = {};

  //hclNrDate["LOCALITATE"] = { nr: 1, date: "2000-01-01"};
  hclNrDate["AITA MARE"]={hclNo: 35, hclDate: "2023-04-25"};
hclNrDate["ARACI"]={hclNo: 10, hclDate: "2023-03-23"};
hclNrDate["ARCUŞ"]={hclNo: 1, hclDate: "2023-01-26"};
hclNrDate["BARAOLT"]={hclNo: 6, hclDate: "2023-02-01"};
hclNrDate["BARCANI"]={hclNo: 15, hclDate: "2023-01-30"};
hclNrDate["BĂŢANII MARI"]={hclNo: 8, hclDate: "2023-02-27"};
hclNrDate["BELIN"]={hclNo: 8, hclDate: "2023-01-25"};
hclNrDate["BIXAD"]={hclNo: 5, hclDate: "2023-01-26"};
hclNrDate["BODOC"]={hclNo: 96, hclDate: "2022-12-28"};
hclNrDate["BOROŞNEU MARE"]={hclNo: 7, hclDate: "2023-01-27"};
hclNrDate["BRĂDUŢ"]={hclNo: 3, hclDate: "2023-01-27"};
hclNrDate["BRATEŞ"]={hclNo: 5, hclDate: "2023-01-30"};
hclNrDate["BREŢCU"]={hclNo: 4, hclDate: "2023-01-25"};
hclNrDate["CATALINA"]={hclNo: 9, hclDate: "2023-02-06"};
hclNrDate["CERNAT"]={hclNo: 15, hclDate: "2023-02-20"};
hclNrDate["CHICHIŞ"]={hclNo: 2, hclDate: "2023-01-31"};
hclNrDate["COMANDĂU"]={hclNo: 64, hclDate: "2022-12-28"};
hclNrDate["COVASNA"]={hclNo: 17, hclDate: "2023-01-31"};
hclNrDate["DALNIC"]={hclNo: 59, hclDate: "2022-12-12"};
hclNrDate["DOBÂRLĂU"]={hclNo: 2, hclDate: "2023-01-31"};
hclNrDate["ESTELNIC"]={hclNo: 3, hclDate: "2023-01-30"};
hclNrDate["GHELINŢA"]={hclNo: 4, hclDate: "2023-01-26"};
hclNrDate["GHIDFALĂU"]={hclNo: 13, hclDate: "2023-03-27"};
hclNrDate["HĂGHIG"]={hclNo: 4, hclDate: "2023-01-31"};
hclNrDate["HERCULIAN"]={hclNo: 8, hclDate: "2023-02-27"};
hclNrDate["ILIENI"]={hclNo: 16, hclDate: "2023-02-27"};
hclNrDate["ÎNTORSURA BUZĂULUI"]={hclNo: 4, hclDate: "2023-01-26"};
hclNrDate["LEMNIA"]={hclNo: 7, hclDate: "2023-01-31"};
hclNrDate["MALNAŞ"]={hclNo: 12, hclDate: "2023-03-08"};
hclNrDate["MERENI"]={hclNo: 6, hclDate: "2023-01-26"};
hclNrDate["MICFALĂU"]={hclNo: 2, hclDate: "2023-01-27"};
hclNrDate["MOACŞA"]={hclNo: 6, hclDate: "2023-01-17"};
hclNrDate["OJDULA"]={hclNo: 10, hclDate: "2023-02-23"};
hclNrDate["OZUN"]={hclNo: 36, hclDate: "2023-04-18"};
hclNrDate["POIAN"]={hclNo: 3, hclDate: "2023-01-31"};
hclNrDate["RECI"]={hclNo: 106, hclDate: "2022-12-23"};
hclNrDate["SÂNZIENI"]={hclNo: 3, hclDate: "2023-01-26"};
hclNrDate["SFÂNTU GHEORGHE"]={hclNo: 7, hclDate: "2023-01-18"};
hclNrDate["SITA BUZĂULUI"]={hclNo: 104, hclDate: "2022-12-28"};
hclNrDate["TÂRGU SECUIESC"]={hclNo: 144, hclDate: "2023-07-20"};
hclNrDate["TURIA"]={hclNo: 7, hclDate: "2023-01-26"};
hclNrDate["VALEA CRIŞULUI"]={hclNo: 15, hclDate: "2023-02-22"};
hclNrDate["VALEA MARE"]={hclNo: 6, hclDate: "2023-02-24"};
hclNrDate["VÂRGHIŞ"]={hclNo: 11, hclDate: "2023-02-28"};
hclNrDate["ZĂBALA"]={hclNo: 2, hclDate: "2023-01-27"};
hclNrDate["ZAGON"]={hclNo: 4, hclDate: "2023-01-20"};

  //restore console
  let iFrame = document.createElement('iframe');
  iFrame.style.display = 'none';
  document.body.appendChild(iFrame);
  window.console = iFrame.contentWindow.console;

  const siiirFetch = async (url, body) => fetch(`${url}?_dc=${new Date().getTime()}`, {
    method: "post",
    credentials: "same-origin",
    headers: {
      Accept: "application/json",
      "Content-type": "application/x-www-form-urlencoded",
    },
    body: body,
  });

  const siiirPutId = async (url, body, id) => fetch(`${url}${id}?_dc=${new Date().getTime()}`, {
    method: "put",
    credentials: "same-origin",
    headers: {
      Accept: "application/json",
      "Content-type": "application/json",
    },
    body: JSON.stringify(body),
  });

  //get nr of results
  const primingListResult = await siiirFetch(listUrl, listParams + "&limit=1&page=1&start=0");

  const primingListJSON = await primingListResult.json();

  const maxItems = primingListJSON.page.total;
  const maxPages = Math.ceil(maxItems / pageLimit); //calculate nr of pages

  for (let page = 1; page <= maxPages; page++) {

    const start = (page - 1) * pageLimit;

    console.log(`Processing page ${page} of ${maxPages}`);

    const listResult = await siiirFetch(listUrl, listParams + `&limit=${pageLimit}&page=${page}&start=${start}`);

    const listJSON = await listResult.json();

    //do the actions
    for (const item of listJSON.page.content) {

      //check if is PJ
      const payload = item;

      if (payload.parentSchool == null) {
        //check if the item is in the list
        if (!hclNrDate[payload.locality.description]) { console.error(`Can't find: ${payload.locality.description}`,); return; }
        //PJ - need to get levels
        payload.hclNo = "" + hclNrDate[payload.locality.description].hclNo;
        payload.hclDate = hclNrDate[payload.locality.description].hclDate;
        const levelsResult = await siiirFetch(extraListUrl, extraListParamPrefix + payload.id + extraListParamSuffix);
        const levelsJSON = await levelsResult.json();
        payload.schoolLevels = levelsJSON.page.content;
      } else {
        //check if the item is in the list
        if (!hclNrDate[payload.parentSchool.locality.description]) { console.error(`Can't find: ${payload.parentSchool.locality.description}`,); return; }
        //AR can modify directly
        payload.hclNo = "" + hclNrDate[payload.parentSchool.locality.description].hclNo;
        payload.hclDate = hclNrDate[payload.parentSchool.locality.description].hclDate;
      }
      await siiirPutId(actionUrl, payload, payload.id);
      console.log("Processed: " + item.longName);
    }
    console.log("DONE!");
  }
})();