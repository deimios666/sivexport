(async () => {
    const schoolId = 11435966;
    const schoolYearId = AMS.util.AppConfig.selectedYear.data.id;
    const actionUrl = "/siiir/management/asocSchoolLevel";
    const nivelAntepreșcolar = {
        "properties": {},
        "from": "1900-01-01",
        "to": null,
        "code": "APRE",
        "description": "Antepreşcolar",
        "orderBy": 1,
        "id": 1
    }
})();