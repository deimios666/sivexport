//IIFE for async
(async() => {
    const listUrl = "https://www.siiir.edu.ro/siiir/list/asocStudentRejoinView.json";
    const pageLimit = 500;
    const schoolYearId = AMS.util.AppConfig.selectedYear.data.id;
    const listParamsPrefix = `generatorKey=ASRQG&filter=%5B%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A${schoolYearId}%7D`;
    const headers = [
        ["ID", "id"],
        ["CNP", "student.nin"],
        ["Cod unitate PJ vechi", "studyFormation.schoolPlanForStudyFormation.school.code"],
        ["Nume unitate PJ vechi", "studyFormation.schoolPlanForStudyFormation.school.longName"],
        ["Cod unitate vechi", "studyFormation.school.code"],
        ["Nume unitate vechi", "studyFormation.school.longName"],
        ["Nivel vechi", "studyFormation.schoolPlanForStudyFormation.studyFormationType.description"],
        ["Cod unitate PJ ReÎnsc", "schoolPlan.school.code"],
        ["Nume unitate PJ ReÎnsc", "schoolPlan.school.longName"],
        ["Cod unitate ReÎnsc", "schoolPlan.schoolForPlan.code"],
        ["Nume unitate ReÎnsc", "schoolPlan.schoolForPlan.longName"],
        ["Limba Plan ReÎnsc", "schoolPlan.teachingLanguage.code"],
        ["Nivel Reînsc", "schoolPlan.studyFormationType.description"],
        ["ID Plan Reînsc", "schoolPlan.id"],
    ];

    //restore console
    let iFrame = document.createElement('iframe');
    iFrame.style.display = 'none';
    document.body.appendChild(iFrame);
    window.console = iFrame.contentWindow.console;

    const csvData = [];

    //get nr of results
    const primingListResult = await fetch(listUrl + "?_dc=" + new Date().getTime(), {
        method: "post",
        credentials: "same-origin",
        headers: {
            Accept: "application/json",
            "Content-type": "application/x-www-form-urlencoded",
        },
        body: listParamsPrefix + "&limit=1&page=1&start=0",
    });

    const primingListJSON = await primingListResult.json();

    const maxItems = primingListJSON.page.total;
    const maxPages = Math.ceil(maxItems / pageLimit); //calculate nr of pages

    for (let page = 1; page <= maxPages; page++) {

        const start = (page - 1) * pageLimit;

        console.log(`Downloading page ${page} of ${maxPages}`);

        const listResult = await fetch(listUrl + "?_dc=" + new Date().getTime(), {
            method: "post",
            credentials: "same-origin",
            headers: {
                Accept: "application/json",
                "Content-type": "application/x-www-form-urlencoded",
            },
            body: listParamsPrefix + `&limit=${pageLimit}&page=${page}&start=${start}`,
        });

        const listJSON = await listResult.json();

        const pageCSVData = await Promise.all(
            listJSON.page.content.map(
                async(item, index) => {
                    return headers.map(key => {
                        const keyPath = key[1];
                        try {
                            const value = keyPath.split('.')
                                .reduce((o, i) => o[i], item) //resolve value
                                .toString()
                                .split('"').join('""'); //escape quotes
                            return `"${value}"`; //return the formatted field
                        } catch (err) {
                            return `""`;
                        }
                    }).join(',');
                }
            )
        );
        csvData.push(pageCSVData.join("\n"));
    }

    const dateString = (new Date()).toISOString().substring(0,10);
    const fileName = `export_preșcolar_reînscrieri_${dateString}.csv`;
    const buffer = "\uFEFF" + headers.map(h => `"${h[0]}"`).join(",") + "\n" + csvData.join("\n");
    const blob = new Blob([buffer], {
        type: "text/csv;charset=utf8;",
    });

    const url = window.URL.createObjectURL(blob);
    const a = document.createElement("a");
    a.href = url;
    a.download = fileName;
    a.click();
})();