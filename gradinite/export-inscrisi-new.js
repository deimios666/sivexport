//IIFE for async
(async() => {
    const listUrl = "/siiir/list/kindergartenRegistration.json";
    const pageLimit = 2000;
    const schoolYearId = AMS.util.AppConfig.selectedYear.data.id;
    const listParamsPrefix = `generatorKey=KRQG&filter=%5B%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A${schoolYearId}%7D`;
    const headers = [
        ["CNP", "kindergartenStudent.nin"],
        ["Nume", "kindergartenStudent.lastName"],
        ["I", "kindergartenStudent.fatherInitial"],
        ["Prenume1", "kindergartenStudent.firstName"],
        ["Prenume2", "kindergartenStudent.firstName1"],
        ["Prenume3", "kindergartenStudent.firstName2"],
        ["Cod unitate PJ", "desiredSchoolPlan1.school.code"],
        ["Nume unitate PJ", "desiredSchoolPlan1.school.longName"],
        ["Cod unitate", "desiredSchoolPlan1.schoolForPlan.code"],
        ["Nume unitate", "desiredSchoolPlan1.schoolForPlan.longName"],
        ["Forma", "desiredSchoolPlan1.educationForm.description"],
        ["Tip predare", "desiredSchoolPlan1.teachingType.description"],
        ["Mod predare", "desiredSchoolPlan1.teachingMode.description"],
        ["Grupa", "desiredSchoolPlan1.studyFormationType.description"],
        ["Limba", "desiredSchoolPlan1.teachingLanguage.description"],
        ["Grupă specială", "desiredSchoolPlan1.specialGroup.description"],
        ["ID plan", "desiredSchoolPlan1.id"],
        ["Locuri", "desiredSchoolPlan1.studentNo"],
        ["Cod unitate PJ2", "desiredSchoolPlan2.school.code"],
        ["Nume unitate PJ2", "desiredSchoolPlan2.school.longName"],
        ["Cod unitate2", "desiredSchoolPlan2.schoolForPlan.code"],
        ["Nume unitate2", "desiredSchoolPlan2.schoolForPlan.longName"],
        ["Forma2", "desiredSchoolPlan2.educationForm.description"],
        ["Tip predare2", "desiredSchoolPlan2.teachingType.description"],
        ["Mod predare2", "desiredSchoolPlan2.teachingMode.description"],
        ["Grupa2", "desiredSchoolPlan2.studyFormationType.description"],
        ["Limba2", "desiredSchoolPlan2.teachingLanguage.description"],
        ["Grupă specială2", "desiredSchoolPlan2.specialGroup.description"],
        ["ID plan2", "desiredSchoolPlan2.id"],
        ["Locuri2", "desiredSchoolPlan2.studentNo"],
        ["Cod unitate PJ3", "desiredSchoolPlan3.school.code"],
        ["Nume unitate PJ3", "desiredSchoolPlan3.school.longName"],
        ["Cod unitate3", "desiredSchoolPlan3.schoolForPlan.code"],
        ["Nume unitate3", "desiredSchoolPlan3.schoolForPlan.longName"],
        ["Forma3", "desiredSchoolPlan3.educationForm.description"],
        ["Tip predare3", "desiredSchoolPlan3.teachingType.description"],
        ["Mod predare3", "desiredSchoolPlan3.teachingMode.description"],
        ["Grupa3", "desiredSchoolPlan3.studyFormationType.description"],
        ["Limba3", "desiredSchoolPlan3.teachingLanguage.description"],
        ["Grupă specială3", "desiredSchoolPlan3.specialGroup.description"],
        ["ID plan3", "desiredSchoolPlan3.id"],
        ["Locuri3", "desiredSchoolPlan3.studentNo"],
        ["Etapa-Faza", "resolvedInStage"],
        ["Stare", "registrationEvaluationStatus"],
        ["Validat", "kindergartenRegistrationValidity.valid"],
        ["Evaluat", "registrationEvaluationStatus"],
    ];



    //restore console
    let iFrame = document.createElement('iframe');
    iFrame.style.display = 'none';
    document.body.appendChild(iFrame);
    window.console = iFrame.contentWindow.console;

    const csvData = [];

    //get nr of results
    const primingListResult = await fetch(listUrl + "?_dc=" + new Date().getTime(), {
        method: "post",
        credentials: "same-origin",
        headers: {
            Accept: "application/json",
            "Content-type": "application/x-www-form-urlencoded",
        },
        body: listParamsPrefix + "&limit=1&page=1&start=0",
    });

    const primingListJSON = await primingListResult.json();

    const maxItems = primingListJSON.page.total;
    const maxPages = Math.ceil(maxItems / pageLimit); //calculate nr of pages

    for (let page = 1; page <= maxPages; page++) {

        const start = (page - 1) * pageLimit;

        console.log(`Downloading page ${page} of ${maxPages}`);

        const listResult = await fetch(listUrl + "?_dc=" + new Date().getTime(), {
            method: "post",
            credentials: "same-origin",
            headers: {
                Accept: "application/json",
                "Content-type": "application/x-www-form-urlencoded",
            },
            body: listParamsPrefix + `&limit=${pageLimit}&page=${page}&start=${start}`,
        });

        const listJSON = await listResult.json();

        const pageCSVData = await Promise.all(
            listJSON.page.content.map(
                async(item, index) => {
                    return headers.map(key => {
                        const keyPath = key[1];
                        try {
                            const value = keyPath.split('.')
                                .reduce((o, i) => o[i], item) //resolve value
                                .toString()
                                .split('"').join('""'); //escape quotes
                            return `"${value}"`; //return the formatted field
                        } catch (err) {
                            return `""`;
                        }
                    }).join(',');
                }
            )
        );
        csvData.push(pageCSVData.join("\n"));
    }

    const dateString = (new Date()).toISOString().substring(0, 10);
    const fileName = `export_preșcolar_înscrieri_${dateString}.csv`;
    const buffer = "\uFEFF" + headers.map(h => `"${h[0]}"`).join(",") + "\n" + csvData.join("\n");
    const blob = new Blob([buffer], {
        type: "text/csv;charset=utf8;",
    });

    const url = window.URL.createObjectURL(blob);
    const a = document.createElement("a");
    a.href = url;
    a.download = fileName;
    a.click();
})();