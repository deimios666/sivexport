const listUrl = "https://www.siiir.edu.ro/siiir/list/kindergartenRegistration.json";
const nextSchoolYearId = AMS.util.AppConfig.selectedYear.data.id; //run with NEXT year seleceted!
const listParam = `generatorKey=KRQG&filter=%5B%7B%22property%22%3A%22kindergartenRegistrationValidity.valid%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5Bfalse%5D%7D%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A${nextSchoolYearId}%7D&page=1&start=0&limit=1000`;

const actionUrl = "https://www.siiir.edu.ro/siiir/management/kindergartenRegistration";

//IIFE to use async
(async ()=>{
  const listResult = await fetch(
    listUrl + "?_dc=" + (new Date).getTime(), {
          method: 'post',
          credentials: 'same-origin',
          headers: {
            "Accept": "application/json",
            "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
          },
          body: listParam
        }
    );
  const listJSON = await listResult.json();

  for (const item of listJSON.page.content){
    await fetch(actionUrl + "/" + item.id + "?_dc=" + (new Date).getTime() + "&schoolYearId=" + nextSchoolYearId, {
                    method: 'DELETE',
                    credentials: 'same-origin',
                    headers: {
                        "Accept": "application/json",
                        "Content-type": "application/json"
                    },
                    body: JSON.stringify(item)
                });
  }
})();