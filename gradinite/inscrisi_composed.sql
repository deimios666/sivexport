SELECT inscrisi.CNP,
 inscrisi.Nume,
 inscrisi.I,
 inscrisi.Prenume1,
 inscrisi.Prenume2,
 inscrisi.Prenume3,
 iif(
 	inscrisi.[Etapa-Faza]="STAGE_1_PHASE_3",
 	inscrisi.[Cod unitate PJ3],
 	iif(
	 	inscrisi.[Etapa-Faza]="STAGE_1_PHASE_2",
	 	inscrisi.[Cod unitate PJ2],
	 	inscrisi.[Cod unitate PJ],
 		)
 	) as [Cod unitate PJ],
 iif(
 	inscrisi.[Etapa-Faza]="STAGE_1_PHASE_3",
 	inscrisi.[Nume unitate PJ3],
 	iif(
	 	inscrisi.[Etapa-Faza]="STAGE_1_PHASE_2",
	 	inscrisi.[Nume unitate PJ2],
	 	inscrisi.[Nume unitate PJ],
 		)
 	) as [Nume unitate PJ],
 iif(
 	inscrisi.[Etapa-Faza]="STAGE_1_PHASE_3",
 	inscrisi.[Cod unitate3],
 	iif(
	 	inscrisi.[Etapa-Faza]="STAGE_1_PHASE_2",
	 	inscrisi.[Cod unitate2],
	 	inscrisi.[Cod unitate],
 		)
 	) as [Cod unitate],
 iif(
 	inscrisi.[Etapa-Faza]="STAGE_1_PHASE_3",
 	inscrisi.[Nume unitate3],
 	iif(
	 	inscrisi.[Etapa-Faza]="STAGE_1_PHASE_2",
	 	inscrisi.[Nume unitate2],
	 	inscrisi.[Nume unitate],
 		)
 	) as [Nume unitate],
 iif(
 	inscrisi.[Etapa-Faza]="STAGE_1_PHASE_3",
 	inscrisi.[Forma3],
 	iif(
	 	inscrisi.[Etapa-Faza]="STAGE_1_PHASE_2",
	 	inscrisi.[Forma2],
	 	inscrisi.[Forma],
 		)
 	) as [Forma],
 iif(
 	inscrisi.[Etapa-Faza]="STAGE_1_PHASE_3",
 	inscrisi.[Tip predare3],
 	iif(
	 	inscrisi.[Etapa-Faza]="STAGE_1_PHASE_2",
	 	inscrisi.[Tip predare2],
	 	inscrisi.[Tip predare],
 		)
 	) as [Tip predare],
 iif(
 	inscrisi.[Etapa-Faza]="STAGE_1_PHASE_3",
 	inscrisi.[Mod predare3],
 	iif(
	 	inscrisi.[Etapa-Faza]="STAGE_1_PHASE_2",
	 	inscrisi.[Mod predare2],
	 	inscrisi.[Mod predare],
 		)
 	) as [Mod predare],
 iif(
 	inscrisi.[Etapa-Faza]="STAGE_1_PHASE_3",
 	inscrisi.[Grupa3],
 	iif(
	 	inscrisi.[Etapa-Faza]="STAGE_1_PHASE_2",
	 	inscrisi.[Grupa2],
	 	inscrisi.[Grupa],
 		)
 	) as [Grupa],
 iif(
 	inscrisi.[Etapa-Faza]="STAGE_1_PHASE_3",
 	inscrisi.[Limba3],
 	iif(
	 	inscrisi.[Etapa-Faza]="STAGE_1_PHASE_2",
	 	inscrisi.[Limba2],
	 	inscrisi.[Limba],
 		)
 	) as [Limba],
 iif(
 	inscrisi.[Etapa-Faza]="STAGE_1_PHASE_3",
 	inscrisi.[Grupă specială3],
 	iif(
	 	inscrisi.[Etapa-Faza]="STAGE_1_PHASE_2",
	 	inscrisi.[Grupă specială2],
	 	inscrisi.[Grupă specială],
 		)
 	) as [Grupă specială],
 iif(
 	inscrisi.[Etapa-Faza]="STAGE_1_PHASE_3",
 	inscrisi.[ID plan3],
 	iif(
	 	inscrisi.[Etapa-Faza]="STAGE_1_PHASE_2",
	 	inscrisi.[ID plan2],
	 	inscrisi.[ID plan],
 		)
 	) as [ID plan],
 iif(
 	inscrisi.[Etapa-Faza]="STAGE_1_PHASE_3",
 	inscrisi.[Locuri3],
 	iif(
	 	inscrisi.[Etapa-Faza]="STAGE_1_PHASE_2",
	 	inscrisi.[Locuri2],
	 	inscrisi.[Locuri],
 		)
 	) as [Locuri],
  inscrisi.[Etapa-Faza],
 inscrisi.Stare,
 inscrisi.Validat,
 inscrisi.Evaluat
FROM inscrisi;



SELECT inscrisi_composed.[ID plan], Sum(IIf([Stare]='APPROVED',1,0)) AS Admis, Sum(IIf([Stare]='APPROVED',0,1)) AS Respins
FROM inscrisi_composed
GROUP BY inscrisi_composed.[ID plan];


SELECT stat.[Nume unitate PJ],
 stat.[Nume unitate],
 stat.Forma,
 stat.[Tip predare],
 stat.[Mod predare],
 stat.Limba,
 stat.Grupa,
 stat.Clase,
 stat.Locuri,
 nz(stat.Reînscriși,0)+nz(stat.Promovați,0) as [Total Reînscriși],
 stat.Reînscriși,
 stat.Promovați,
 nz(stat.[Însc peste 3],0)+nz(stat.[Însc sub 3],0) as [Total Înscriși],
 stat.[Însc peste 3],
 stat.[Însc sub 3],
 stat.Admis,
 stat.Respins
FROM stat LEFT JOIN retea ON stat.[Nume unitate] = retea.[Denumire lunga];
