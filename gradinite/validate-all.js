(async () => {
    const listUrl = `https://www.siiir.edu.ro/siiir/list/kindergartenRegistration.json`;
    const listParams = `generatorKey=KRQG&filter=%5B%7B%22property%22%3A%22kindergartenRegistrationValidity.valid%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5Bfalse%5D%7D%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A26%7D&page=1&start=0&limit=25&sort=%5B%7B%22property%22%3A%22kindergartenStudent.firstName%22%2C%22direction%22%3A%22ASC%22%7D%5D`;

    const getItemUrl = `https://www.siiir.edu.ro/siiir/management/getKindergartenRegistrationStudent.json`;
    const getItemParams = `cnp=`;

    const actionUrlPrefix = `https://www.siiir.edu.ro/siiir/management/saveKindergartenOpis/`;
    const actionParams = `birthCertificate=true&identityCard=true&valid=true`;

    //get list of invalid registrations
    //for each registration
    //use CNP to get existing registration
    //pass the existing registration with valid=true

})();
