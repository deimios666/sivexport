//WARNING: can only be run as school user!!!
//IIFE closure for async and isolation
(async() => {

    const schoolYearId = AMS.util.AppConfig.selectedYear.data.id;
    const limit = 1000;
    const stage = "TOATE"; //other valid values: "PHASE_x_STAGE_y" where X:1,2 and Y:1,2,3) and "COMPLETED"

    const listUrl = "/siiir/list/kindergartenRegistration.json";
    const listParams = `generatorKey=KRQG&filter=%5B%7B%22property%22%3A%22registrationEvaluationStatus%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B%22NOTEVALUATED%22%5D%7D%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A${schoolYearId}%2C%22selectedPhase%22%3A%22${stage}%22%7D&page=1&start=0&limit=${limit}`;

    const actionUrl = "/siiir/acceptKinderStudent";

    const actionParamsPrefix = "regId=";
    const actionParamsSuffix = "&loggedSchoolId=";

    const fallbackActionUrl = "/siiir/rejectKinderStudent";

    const listResult = await fetch(listUrl + "?_dc=" + (new Date).getTime(), {
        method: 'post',
        credentials: 'same-origin',
        headers: {
            "Accept": "application/json",
            "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
        },
        body: listParams
    });

    const listJSON = await listResult.json();

    for (const elev of listJSON.page.content) {

        const pjId = (elev.kindergarten1.parentSchool || elev.kindergarten1).id;

        const result = await fetch(actionUrl + "?_dc=" + (new Date).getTime(), {
            method: 'post',
            credentials: 'same-origin',
            headers: {
                "Accept": "application/json",
                "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
            },
            body: actionParamsPrefix + elev.id + actionParamsSuffix + pjId
        });

        const resultJSON = await result.json();

        //check if failed
        if ( !resultJSON.success ) {
            //check if reason is lack of space and reject if true
            if ( resultJSON.message.includes("noSeats") ) {
                await fetch(fallbackActionUrl + "?_dc=" + (new Date).getTime(), {
                    method: 'post',
                    credentials: 'same-origin',
                    headers: {
                        "Accept": "application/json",
                        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                    },
                    body: actionParamsPrefix + elev.id + actionParamsSuffix + pjId
                });
            }
        }
    }
})();