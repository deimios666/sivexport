const listURL = "https://edusal.edu.ro/auth/isj/utilitareScoli/NormeScoli.aspx";
const luna = 8;
const an = 2024;
const listParams = `?a=1&luna=${luna}&an=${an}&save=False`;


const header = [
  "Cod şcoală",
  "Nume şcoală",
  "Norme aprobate",
  "MEN-Didactic",
  "MEN-Auxiliar",
  "MEN-Nedidactic",
  "MEN-Nedidactic ISJ",
  "CL-Didactic",
  "CL-Auxiliar",
  "CL-Nedidactic",
  "CL-Nedidactic ISJ",
  "CJ-Didactic",
  "CJ-Auxiliar",
  "CJ-Nedidactic",
  "CJ-Nedidactic ISJ",
  "VP-MEN-Didactic",
  "VP-MEN-Auxiliar",
  "VP-MEN-Nedidactic",
  "VP-MEN-Nedidactic ISJ",
  "VP-CLCJ-Didactic",
  "VP-CLCJ-Auxiliar",
  "VP-CLCJ-Nedidactic",
  "VP-CLCJ-Nedidactic ISJ",
  "Buton"
];

String.prototype.replaceAll = function (search, replacement) {
  var target = this;
  if (this.search(search) !== -1) {
    return target.split(search).join(replacement);
  } else {
    return target;
  }
};

let csvHeader = '"' + header.join('","') + '"';

const numberRegex = /^[0-9,.]+$/;

//iife to use async on top level
(async () => {

  //fetch values from server
  const listResult = await fetch(listURL + listParams);
  const listText = await listResult.text();
  const listHTML = document.createElement("html");
  //parse result as DOM
  listHTML.innerHTML = listText;
  const statTableRows = listHTML.querySelectorAll("div#tabel_stat_personal>table tr");

  let maxRows = statTableRows.length;
  let maxCols = header.length;

  const lineReducer = (lineAcc, cell, index) => {
    if (index % 2 === 1) return lineAcc; //skip every other cell
    let firstLine = ",";
    let value = cell.innerText.trim();
    value = value.replaceAll('"', '""');
    if (value.match(numberRegex)) {
      value = value.replaceAll(',', '|').replaceAll('.', ',').replaceAll('|', '.'); //triple replace to invert , and . in numbers
    }
    if (lineAcc === "") { firstLine = "" };
    return lineAcc + firstLine + `"` + value + `"`;
  };

  const csvReducer = (csvAcc, row) => {
    let line = Array.from(row.getElementsByTagName("td")).reduce(lineReducer, "");//starting value is empty string
    //skip empty lines
    if (line === "") {
      return csvAcc;
    } else {
      return csvAcc + "\n" + line;
    }
  };

  //prepend utf8 BOM for excel then reduce cells into row strings then reduce row strings into CSV file string
  let buffer = Array.from(statTableRows).reduce(csvReducer, "\uFEFF" + csvHeader); //starting value is header row with utf8 BOM

  let fileName = `export_norme_${an}_${luna}.csv`;
  let blob = new Blob([buffer], {
    "type": "text/csv;charset=utf8;"
  });

  const url = window.URL.createObjectURL(blob);
  const a = document.createElement("a");
  a.href = url;
  a.download = fileName;
  a.click();

})();
