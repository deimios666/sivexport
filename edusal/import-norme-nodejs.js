const chromium = require("playwright").chromium;

const config = {
  username: "ES_CV_01",
  password: "",
  year: 2024,
  month: 9,
}

const data = {
  461687: {"Cod şcoală": 461687,"Nume şcoală": 0,"Norme aprobate": 88,"MEN-Didactic": 0,"MEN-Auxiliar": 0,"MEN-Nedidactic": 0,"MEN-Nedidactic ISJ": 0,"CL-Didactic": 0,"CL-Auxiliar": 0,"CL-Nedidactic": 0,"CL-Nedidactic ISJ": 0,"CJ-Didactic": 0,"CJ-Auxiliar": 0,"CJ-Nedidactic": 0,"CJ-Nedidactic ISJ": 0,"VP-MEN-Didactic": 0,"VP-MEN-Auxiliar": 0,"VP-MEN-Nedidactic": 0,"VP-MEN-Nedidactic ISJ": 0,"VP-CLCJ-Didactic": 35,"VP-CLCJ-Auxiliar": 8,"VP-CLCJ-Nedidactic": 45,"VP-CLCJ-Nedidactic ISJ": 0}
}
/* Format --
sirues: {
  "Cod şcoală": 0,
  "Nume şcoală": 0,
  "Norme aprobate": 0,
  "MEN-Didactic": 0,
  "MEN-Auxiliar": 0,
  "MEN-Nedidactic": 0,
  "MEN-Nedidactic ISJ": 0,
  "CL-Didactic": 0,
  "CL-Auxiliar": 0,
  "CL-Nedidactic": 0,
  "CL-Nedidactic ISJ": 0,
  "CJ-Didactic": 0,
  "CJ-Auxiliar": 0,
  "CJ-Nedidactic": 0,
  "CJ-Nedidactic ISJ": 0,
  "VP-MEN-Didactic": 0,
  "VP-MEN-Auxiliar": 0,
  "VP-MEN-Nedidactic": 0,
  "VP-MEN-Nedidactic ISJ": 0,
  "VP-CLCJ-Didactic": 0,
  "VP-CLCJ-Auxiliar": 0,
  "VP-CLCJ-Nedidactic": 0,
  "VP-CLCJ-Nedidactic ISJ": 0
}*/

const template = [
  "Cod şcoală", "L", "Nume şcoală", "L", "Norme aprobate", "L", "MEN-Didactic", "L", "MEN-Auxiliar", "L", "MEN-Nedidactic", "L", "MEN-Nedidactic ISJ", "L", "CL-Didactic", "L", "CL-Auxiliar", "L", "CL-Nedidactic", "L", "CL-Nedidactic ISJ", "L", "CJ-Didactic", "L", "CJ-Auxiliar", "L", "CJ-Nedidactic", "L", "CJ-Nedidactic ISJ", "L", "VP-MEN-Didactic", "L", "VP-MEN-Auxiliar", "L", "VP-MEN-Nedidactic", "L", "VP-MEN-Nedidactic ISJ", "L", "VP-CLCJ-Didactic", "L", "VP-CLCJ-Auxiliar", "L", "VP-CLCJ-Nedidactic", "L", "VP-CLCJ-Nedidactic ISJ", "L", "Buton", "ButonId"
];


const editorMapping = {
  //"Cod şcoală": "phMain_txtScoala",
  "Norme aprobate": "phMain_txtNormeAprobate",
  "MEN-Didactic": "phMain_txtNormeDid1",
  "MEN-Auxiliar": "phMain_txtNormeDidAux1",
  "MEN-Nedidactic": "phMain_txtNormeNedid1",
  "MEN-Nedidactic ISJ": "phMain_txtNedidacticISJ1",
  "CL-Didactic": "phMain_txtNormeDid2",
  "CL-Auxiliar": "phMain_txtNormeDidAux2",
  "CL-Nedidactic": "phMain_txtNormeNedid2",
  "CL-Nedidactic ISJ": "phMain_txtNedidacticISJ2",
  "CJ-Didactic": "phMain_txtNormeDid3",
  "CJ-Auxiliar": "phMain_txtNormeDidAux3",
  "CJ-Nedidactic": "phMain_txtNormeNedid3",
  "CJ-Nedidactic ISJ": "phMain_txtNedidacticISJ3",
  "VP-MEN-Didactic": "phMain_txtNormeDid4",
  "VP-MEN-Auxiliar": "phMain_txtNormeDidAux4",
  "VP-MEN-Nedidactic": "phMain_txtNormeNedid4",
  "VP-MEN-Nedidactic ISJ": "phMain_txtNedidacticISJ4",
  "VP-CLCJ-Didactic": "phMain_txtNormeDid5",
  "VP-CLCJ-Auxiliar": "phMain_txtNormeDidAux5",
  "VP-CLCJ-Nedidactic": "phMain_txtNormeNedid5",
  "VP-CLCJ-Nedidactic ISJ": "phMain_txtNedidacticISJ5"
};
String.prototype.replaceAll = function (search, replacement) {
  var target = this;
  if (this.search(search) !== -1) {
    return target.split(search).join(replacement);
  } else {
    return target;
  }
};
const numberRegex = /^[0-9,.]+$/;
const fixNumber = value => {
  if (String(value).match(numberRegex)) {
    return parseFloat(
      String(value)
        .replaceAll(",", "|")
        .replaceAll(".", ",")
        .replaceAll("|", ".")
    ); //triple replace to invert , and . in numbers
  }
  return value;
};
const breakNumber = value => {
  if (String(value).match(numberRegex)) {
    return String(value)
      .replaceAll(",", "|")
      .replaceAll(".", ",")
      .replaceAll("|", "."); //triple replace to invert , and . in numbers
  }
  return value;
};
const log = message => console.log(message);
(async () => {
  const year = "" + config.year;
  const month = "" + config.month;
  const browser = await chromium.launch();
  const page = await browser.newPage({
    viewport: { width: 1920, height: 2000 },
  });
  log("Loading login page");
  await page.goto("https://edusal.edu.ro/");
  await page.locator("#phContent_TextBoxUser").fill(config.username);
  await page.locator("#phContent_TextBoxPass").fill(config.password);
  //await page.screenshot({ path: "edusal_login.png" });
  log("Logging in");
  await page.click("#phContent_ButtonLogIn");
  await page.waitForLoadState();
  await page.goto(
    `https://edusal.edu.ro/auth/isj/UtilitareScoli/NormeScoli.aspx?a=1&luna=${month}&an=${year}`
  );
  await page.waitForLoadState();
  //for each sirues
  for (currentCodScoala of Object.keys(data)) {
    //const currentData = data[currentCodScoala];
    log("Loading page NormeScoli");
    await page.click("#phMain_txtFCodScoala", { clickCount: 3 });
    await page.locator("#phMain_txtFCodScoala").fill(currentCodScoala);
    /*await page.select("#phMain_ddlMonth", month);
    await page.select("#phMain_ddlYear", year);*/
    log(`Loading page NormeScoli for code: ${currentCodScoala} month: ${month} year: ${year}`);
    await page.click("#phMain_btnApplyFilter");
    await page.waitForLoadState();
    //await page.screenshot({ path: `edusal_filter_${currentCodScoala}.png` });
    //get school list and values
    let schoolTable = await page.$$eval(
      "#tabel_stat_personal table tr.impar, #tabel_stat_personal table tr.par",
      rows => rows.map(row => {
        const buttonId = row.querySelector("input").id;
        return row.innerText + "\t" + buttonId;
      })
    );

    //convert tab separated lines into keyed objects
    const valueTable = schoolTable.map(rowText =>
      rowText.split("\t").reduce(
        (acc, cell, index) => {
          const key = template[index];
          acc[key] = fixNumber(cell.trim()); //get key name from template[i]
          return acc;
        },
        {} //empty obj default for reduce
      )
    );
    //console.log(valueTable);
    schoolTable = null;
    let maxRows = valueTable.length;
    log("Table length: " + maxRows);
    //for each school, sequentially
    log("Iterating schools");
    await valueTable.reduce(async (previousPromise, currentSourceData) => {
      await previousPromise;
      //check if current values different from new values
      //get values for current school code from data
      const currentCodScoala = currentSourceData["Cod şcoală"];
      log(currentCodScoala + " Processing");
      const currentTargetData = data[currentCodScoala];
      //compare properties, technically it's not true "equality" check but "contains" check
      const compareResult = Object.keys(editorMapping).every(
        key => {
          const source = currentSourceData[key];
          const target = currentTargetData[key];
          if (source != target) { log(`Comparing: "${source}" and "${target}" result: ${source == target}`); }
          try {
            return source == target;
          } catch {
            return true;
          }
        }
      );
      //if we have any differences we edit
      if (!compareResult) {
        log(currentCodScoala + " Needs correction");
        //get edit button ID
        const buttonId = currentSourceData["ButonId"];
        log(currentCodScoala + " Pressing edit")
        //press edit button
        await page.click("#" + buttonId);
        await page.waitForLoadState();
        log(currentCodScoala + " Writing values");
        //edit values
        await Object.getOwnPropertyNames(editorMapping).reduce(async (previousPromise2, key) => {
          await previousPromise2;
          log(currentCodScoala + " editing " + editorMapping[key] + " to " + breakNumber(currentSourceData[key]));
          const input = await page.locator("#" + editorMapping[key]);
          //await input.click({ clickCount: 3 });
          await input.fill(breakNumber(currentTargetData[key]));
          return Promise.resolve();
        },
          Promise.resolve());
        log(currentCodScoala + " Screenshotting and saving");
        //press save
        //await page.screenshot({ path: "edusal" + buttonId + ".png" });
        //WARNING - enable at your own risk
        await page.click("#phMain_btnSave");
        await page.waitForLoadState();
      }
      //next school
      return Promise.resolve();
    }, Promise.resolve());
  }

  //log("Generating screenshot");
  //await page.screenshot({ path: "edusal.png" });
  await browser.close();
})();
