(async () => {
  //WARNING RUN WITH DISABLED CACHE
  const luna = 10;
  const an = 2022;

  const mainListUrl = `https://edusal.edu.ro/auth/isj/gestiuneapersonalului/statpersonal.aspx?a=1&luna=${luna}&an=${an}&activ=1`;

  const pageClassName = `pager_item`;

  const itemLinkPrefix = `/auth/isj/gestiuneapersonalului/v2_33/StatPersonalDetails.aspx`;

  const schoolDetailsIds = [
    'phMain_ucSchoolDetails_lblSchoolName',
    'phMain_ucSchoolDetails_lblSchoolCode',
    'phMain_lblTipUnitate',
    'phMain_lblNumarElevi',
  ];

  const csvData = [];

  const fetchAsDom = async (url) => {
    const response = await fetch(url, {
      method: "get",
      credentials: "same-origin",
    });
    const text = await response.text();
    const parser = new DOMParser();
    const dom = parser.parseFromString(text, `text/html`);
    return dom;
  }

  //get page list
  const pageListDom = await fetchAsDom(mainListUrl);
  const pageList = pageListDom.getElementsByClassName(pageClassName);
  const pageUrls = Array.from(pageList).map((page) => page.href);
  pageUrls.push(mainListUrl);

  //for each page
  for (const pageUrl of pageUrls) {
    console.log(`Processing page: ${pageUrl}`);
    //get school list
    const schoolListDom = await fetchAsDom(pageUrl);
    const schoolUrls = Array.from(schoolListDom
      .getElementsByTagName(`a`))
      .filter((a) => a.href.includes(itemLinkPrefix))
      .map((a) => a.href);
    //download school
    for (const schoolUrl of schoolUrls) {
      //extract data
      const schoolDom = await fetchAsDom(schoolUrl);
      const schoolDetails = schoolDetailsIds.map(id => {
        const value = schoolDom
          .querySelector(`#${id}`)
          .innerText;
        if (value.includes(`"`)) {
          return `"` + value.split(`"`).join(`""`) + `"`
        }
        return `"${value}"`;
      });
      console.log(schoolDetails[0]);
      csvData.push(schoolDetails.join(`,`));
    }
  }

  console.log(csvData);

  const fileName = "export_edusal_elevi.csv";
  const buffer = "\uFEFF" + "nume,cod,tip,elevi" + "\n" + csvData.join("\n");
  const blob = new Blob([buffer], {
    type: "text/csv;charset=utf8;",
  });
  const url = window.URL.createObjectURL(blob);
  const a = document.createElement("a");
  a.href = url;
  a.download = fileName;
  a.click();
})();
