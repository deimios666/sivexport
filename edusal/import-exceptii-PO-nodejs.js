const chromium = require("playwright").chromium;

const config = {
  username: "ES_CV_01",
  password: "",
}

const defaultData = {
  schoolCode: 0,
  cnp: "0",
  dateFrom: "01.09.2024",
  dateTo: "21.06.2025",
  observatii: "",
  avizIsjNr: "901",
  avizIsjData: "01.10.2024",
  active: true
}

const data = [
  {schoolCode:0,cnp:"0"},  
]

const fieldMapping = {
  schoolCode: "phMain_UCMainControl_txtCodScoala",
  cnp: "phMain_UCMainControl_txtCNP",
  dateFrom: "phMain_UCMainControl_tbxValabilDeLa",
  dateTo: "phMain_UCMainControl_tbxValabilPanaLa",
  observatii: "phMain_UCMainControl_txtObservatii",
  avizIsjNr: "phMain_UCMainControl_txtNumarDocument",
  avizIsjData: "phMain_UCMainControl_txtDataDocument",
  active: "phMain_UCMainControl_chkActive"
}


const log = message => console.log(message);

(async () => {
  const browser = await chromium.launch();
  const page = await browser.newPage({
    viewport: { width: 1920, height: 2000 },
  });
  log("Loading login page");
  await page.goto("https://edusal.edu.ro/");
  await page.locator("#phContent_TextBoxUser").fill(config.username);
  await page.locator("#phContent_TextBoxPass").fill(config.password);
  //await page.screenshot({ path: "edusal_login.png" });
  log("Logging in");
  await page.click("#phContent_ButtonLogIn");
  await page.waitForLoadState();

  await page.goto("https://edusal.edu.ro/auth/isj/UtilitareScoli/ExceptiiScoala.aspx");
  await page.waitForLoadState();
  //await page.screenshot({ path: "edusal_exceptii.png" });
  //#phMain_UCMainControl_comboTipExceptie
  //await page.screenshot({ path: "edusal_exceptii2.png" });
  //for each school
  for (const currentData of data) {
    const payload = { ...defaultData, ...currentData };
    await page.locator("#phMain_UCMainControl_comboTipExceptie").selectOption({ label: "Exceptie trimitere alerta #PO" });
    await page.waitForLoadState();
    for (const key of Object.keys(fieldMapping)) {
      const input = await page.locator("#" + fieldMapping[key]);
      const value = payload[key];
      if (typeof value === "boolean") {
        //if boolean then checkbox
        if (value === true) {
          await input.check();
        } else {
          await input.uncheck();
        }
      } else {
        //not boolean we fill text
        await input.fill(`${value}`);
      }
    }
    await page.click("#phMain_UCMainControl_btnSalveaza");
    await page.waitForLoadState();
    await page.screenshot({ path: `edusal_exceptii_${currentData.schoolCode}-${currentData.cnp}.png` });
  }

  //log("Generating screenshot");
  //await page.screenshot({ path: "edusal.png" });
  await browser.close();
})();
