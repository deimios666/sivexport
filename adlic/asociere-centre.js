var codJud = "CV";

var listUrl = "https://adlic.edu.ro/adlic/list/school.json";
var listCentreUrl = "https://adlic.edu.ro/adlic/list/enrollmentCenter.json";
var listAssocUrl = "https://adlic.edu.ro/adlic/list/asocSchoolEnrollmentCenter.json";
var actionUrl = "https://adlic.edu.ro/adlic/management/asocSchoolEnrollmentCenter";

var listParams = "generatorKey=AASQG&viewName=default&requestParams=%7B%22enrollmentCenter%22%3A13560%7D&filter=%5B%5D&page=1&start=0&limit=500&sort=%5B%7B%22property%22%3A%22code%22%2C%22direction%22%3A%22ASC%22%7D%5D";
var listCentreParams = "generatorKey=ECQG&filter=%5B%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%7D&page=1&start=0&limit=500";
var listAssocParams = "generatorKey=asocSchoolEnrollmentCenter_GRID_GENERATOR_KEY&filter=%5B%7B%22property%22%3A%22enrollmentCenter.county.code%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B%22" + codJud + "%22%5D%7D%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%7D&page=1&start=0&limit=500";
var actionParams = {
    "id": null,
    "enrollmentCenter": {
        "id": 13560
    },
    "school": {
        "id": "11169457"
    },
    "properties": null
};

var schools = {};
var enrollmentCenters = {};


//internal replaceAll
String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};

//get schools

fetch(listUrl + "?_dc=" + (new Date).getTime(), {
    method: 'post',
    credentials: 'same-origin',
    headers: {
        "Accept": "application/json",
        "Content-type": "application/x-www-form-urlencoded"
    },
    body: listParams

})
    .then(
        response => response.json()
    )
    .then(
        jsonResponse => {
            jsonResponse.page.content.forEach(
                schoolItem => {
                    schools[schoolItem.longName] = schoolItem.id;
                }
            );
        }
    );


//get enrollment centers
async function getEnrollmentCenters() {
    const response = await fetch(listCentreUrl + "?_dc=" + (new Date).getTime(), {
        method: 'post',
        credentials: 'same-origin',
        headers: {
            "Accept": "application/json",
            "Content-type": "application/x-www-form-urlencoded"
        },
        body: listCentreParams

    });
    const jsonResponse = await response.json();
    jsonResponse.page.content.forEach(
        centreItem => {
            enrollmentCenters[centreItem.description] = centreItem.id;
        }
    );

}

getEnrollmentCenters();
//get assoc
function doExportAssoc() {
    fetch(listAssocUrl + "?_dc=" + (new Date).getTime(), {
        method: 'post',
        credentials: 'same-origin',
        headers: {
            "Accept": "application/json",
            "Content-type": "application/x-www-form-urlencoded"
        },
        body: listAssocParams

    })
        .then(
            response => response.json()
        )
        .then(
            jsonResponse => {
                var csvData = [];
                jsonResponse.page.content.forEach(
                    assocItem => {
                        csvData.push([
                            assocItem.enrollmentCenter.description,
                            assocItem.school.longName
                        ]);
                    }
                );
                var fileName = "export_adlic_assoc.csv";
                var buffer = "\uFEFFCentrul,Școala\n" + csvData.join("\n");
                var blob = new Blob([buffer], {
                    "type": "text/csv;charset=utf8;"
                });
                var schoolDownloadUrl = window.URL.createObjectURL(blob);
                var a = document.createElement('a');
                a.href = schoolDownloadUrl;
                a.download = fileName;
                a.click();
            }
        );
}
//implement export school list
function doExportSchoolList() {
    var schoolNames = Object.getOwnPropertyNames(schools);
    var fileName = "export_adlic_school.csv";
    var buffer = "\uFEFFȘcoala\n" + schoolNames.join("\n");
    var blob = new Blob([buffer], {
        "type": "text/csv;charset=utf8;"
    });
    var schoolDownloadUrl = window.URL.createObjectURL(blob);
    var a = document.createElement('a');
    a.href = schoolDownloadUrl;
    a.download = fileName;
    a.click();
}

//implement export centre list
async function doExportCentreList() {
    var centreNames = Object.getOwnPropertyNames(enrollmentCenters);
    var fileName = "export_adlic_centre.csv";
    var buffer = "\uFEFFCentrul\n" + centreNames.join("\n");
    var blob = new Blob([buffer], {
        "type": "text/csv;charset=utf8;"
    });
    var centreDownloadUrl = window.URL.createObjectURL(blob);
    var a = document.createElement('a');
    a.href = centreDownloadUrl;
    a.download = fileName;
    a.click();
}

function doImportAssoc() {
    var importArray = importBox.value.split("\n");
    importArray.forEach(
        item => {
            var value = item.split(/\t/);
            var payload = {
                "id": null,
                "enrollmentCenter": {
                    "id": enrollmentCenters[value[0]]
                },
                "school": {
                    "id": schools[value[1]]
                },
                "properties": null
            };

            fetch(actionUrl + "?_dc=" + (new Date).getTime(), {
                method: 'post',
                credentials: 'same-origin',
                headers: {
                    "Accept": "application/json",
                    "Content-type": "application/json"
                },
                body: JSON.stringify(payload)
            });
        }
    );
}

async function doCreateCentre() {
    const actionUrl = `https://adlic.edu.ro/adlic/management/enrollmentCenter?_dc=${new Date().getTime()}`;
    const actionParams = { "to": null, "id": null, "rromaFinalised": null, "county": null, "description": "Mikes", "from": null, "code": "1", "properties": null, "cesFinalised": null };
    const importValues = importBox.value.split("\n");
    const importArray = importValues.slice(1);

    for (const item of importArray) {
        const value = item.split(/\t/);
        actionParams.code = value[0];
        actionParams.description = value[1];
        await fetch(actionUrl, {
            method: 'post',
            credentials: 'same-origin',
            headers: {
                "Accept": "application/json",
                "Content-type": "application/json"
            },
            body: JSON.stringify(actionParams)
        });
    }

    await getEnrollmentCenters();
}

//implement importing assoc

//implement deleting assoc


//create GUI overlay and buttons

//create base div
var baseDiv = document.createElement("div");
baseDiv.setAttribute("class", "module-selector-module-container-horizontal");
baseDiv.setAttribute("style", "height:340px; width:800px;vertical-align:center; position:absolute;top:5px;left:5px; z-index:10;");

//create import box
var importBox = document.createElement("textarea");
importBox.setAttribute("style", "height:200px;width:790px;");
importBox.setAttribute("id", "importBox");
baseDiv.appendChild(importBox);

//create import centre button
var impcButton = document.createElement("button");
impcButton.setAttribute("style", "height:25px; width:150px;vertical-align:center;");
impcButton.onclick = doCreateCentre;
var impcButtonText = document.createTextNode("Import Centre")
impcButton.appendChild(impcButtonText);
baseDiv.appendChild(impcButton);

//create import button
var importButton = document.createElement("button");
importButton.setAttribute("style", "height:25px; width:150px;vertical-align:center;");
importButton.onclick = doImportAssoc;
var importButtonText = document.createTextNode("Importă Asocieri")
importButton.appendChild(importButtonText);
baseDiv.appendChild(importButton);

//create export school button
var exsButton = document.createElement("button");
exsButton.setAttribute("style", "height:25px; width:150px;vertical-align:center;");
exsButton.onclick = doExportSchoolList;
var exsButtonText = document.createTextNode("Export Școli")
exsButton.appendChild(exsButtonText);
baseDiv.appendChild(exsButton);

//create export centru button
var excButton = document.createElement("button");
excButton.setAttribute("style", "height:25px; width:150px;vertical-align:center;");
excButton.onclick = doExportCentreList;
var excButtonText = document.createTextNode("Export Centre")
excButton.appendChild(excButtonText);
baseDiv.appendChild(excButton);


//create export current assoc button
var exaButton = document.createElement("button");
exaButton.setAttribute("style", "height:25px; width:150px;vertical-align:center;");
exaButton.onclick = doExportAssoc;
var exaButtonText = document.createTextNode("Export Asocieri")
exaButton.appendChild(exaButtonText);
baseDiv.appendChild(exaButton);

//create import description
const descPar1 = document.createElement("p");
descPar1.innerHTML = `
<p>Datele se importează în format TAB DELIMITED. Copy-paste din Excel</p>
<p>Atenție: primul rând se ignorează</p>
<p>"Importă Centre" cap de tabel: Cod centru, Descriere</p>
<p>"Importă Asocieri" cap de tabel: Cod centru, Școala</p>
`;
baseDiv.appendChild(descPar1);

var body = document.getElementsByTagName("BODY")[0];
body.appendChild(baseDiv);