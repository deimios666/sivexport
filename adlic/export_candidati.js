(async () => {
  const listUrl = "https://adlic.edu.ro/adlic/list/candidate.json";

  const itemListParamString = `generatorKey=CLQG&filter=%5B%5D&sort=%5B%7B%22property%22%3A%22county.description%22%2C%22direction%22%3A%22ASC%22%7D%2C%7B%22property%22%3A%22lastName%22%2C%22direction%22%3A%22ASC%22%7D%2C%7B%22property%22%3A%22fatherInitial%22%2C%22direction%22%3A%22ASC%22%7D%2C%7B%22property%22%3A%22firstName%22%2C%22direction%22%3A%22ASC%22%7D%2C%7B%22property%22%3A%22nin%22%2C%22direction%22%3A%22ASC%22%7D%5D&filterName=%5B%5D&requestParams=%7B%7D`;
  const fileName = "export_candidati_optiuni.csv";
  const dataFormat = [
    ["CNP", "nin"],
    ["Nume", "firstName"],
    ["Ini", "fatherInitial"],
    ["Prenume", "lastName"],
    ["Şcoală proveniență", "school.longName"],
    ["Centru local", "enrollmentCenter.description"],
    ["Media de admitere", "averageAdm"],
  ];

  const pageSize = 200;

  //internal replaceAll
  String.prototype.replaceAll = function (search, replacement) {
    const target = this;
    if (this.search(`"`) > -1) {
      return target.split(search).join(replacement);
    }
    return this;
  };

  const primingResponse = await fetch(listUrl + "?_dc=" + (new Date).getTime(), {
    method: 'post',
    credentials: 'same-origin',
    headers: {
      "Accept": "application/json",
      "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
      "X-Requested-With": "XMLHttpRequest"
    },
    body: itemListParamString + "&page=1&start=0&limit=1"
  });

  let CSVArray = [];

  const primingResponseJSON = await primingResponse.json();
  const maxItems = primingResponseJSON.page.total;
  const maxPages = Math.ceil(maxItems / pageSize);

  console.log(`Candidates: ${maxItems}, Pages: ${maxPages}`);

  for (let page = 1; page <= maxPages; page++) {
    console.log(`Processing page: ${page}/${maxPages}`);
    const start = (page - 1) * pageSize;
    const listResponse = await fetch(listUrl + "?_dc=" + (new Date).getTime(), {
      method: 'post',
      credentials: 'same-origin',
      headers: {
        "Accept": "application/json",
        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
        "X-Requested-With": "XMLHttpRequest"
      },
      body: itemListParamString + `&page=${page}&start=${start}&limit=${pageSize}`
    });
    const listJSON = await listResponse.json();

    for await (const item of listJSON.page.content) {
      const CSVLine = dataFormat.map(([_, valuePath]) => {
        try {
          const value = valuePath.split('.')
            .reduce((o, i) => o[i], item) //resolve value
            .toString()
            .split('"').join('""'); //escape quotes
          return `"${value}"`; //return the formatted field
        } catch (err) {
          return `""`;
        }
      })
      CSVArray.push(CSVLine.join(","));
    }
  }
  const buffer = "\uFEFF" + dataFormat.map(([key, _]) => key).join(",") + "\n" + CSVArray.join("\n");
  let blob = new Blob([buffer], {
    "type": "text/csv;charset=utf8;"
  });
  const link = document.createElement("a");
  link.href = URL.createObjectURL(blob);
  link.download = fileName;
  link.click();
  link.remove();
})()
