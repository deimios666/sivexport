(async () => {

  const listUrl = "https://adlic.edu.ro/adlic/list/specialization.json";
  const listParam = "generatorKey=SPINCQG&filter=%5B%5D&sort=%5B%7B%22property%22%3A%22county.code%22%2C%22direction%22%3A%22ASC%22%7D%2C%7B%22property%22%3A%22code%22%2C%22direction%22%3A%22ASC%22%7D%5D&filterName=%5B%5D&requestParams=%7B%7D&page=1&start=0&limit=1000";
  const actionUrl = "https://adlic.edu.ro/adlic/management/specialization/";

  const headers = [
    "ID",
    "Cod",
    "Liceu",
    "Domeniu",
    "Limba",
    "Bilingv",
    "Filieră",
    "Profil",
    "Locuri",
    "Locuri rromi",
    "Locuri CES",
  ];

  const keys = [
    "id",
    "code",
    "highSchool.longName",
    "domain.description",
    "teachingLanguage.description",
    "bilingual.description",
    "branch.description",
    "profile.description",
    "numberOfSeats",
    "numberOfRromaSeats",
    "numberOfCesSeats",
  ]

  const doExport = async () => {
    //get list
    const listResult = await fetch(
      listUrl + "?_dc=" + new Date().getTime(),
      {
        method: "post",
        credentials: "same-origin",
        headers: {
          Accept: "application/json",
          "Content-type":
            "application/x-www-form-urlencoded; charset=UTF-8",
        },
        body: listParam,
      }
    );
    const resultJSON = await listResult.json();

    const csvData = [];
    //process elements
    for (const item of resultJSON.page.content) {
      const oneCSVLine = keys.map((key) => {
        try {
          const value = key
            .split(".")
            .reduce((o, i) => o[i], item) //resolve value
            .toString()
            .split('"')
            .join('""'); //escape quotes
          return `"${value}"`; //return the formatted field
        } catch (err) {
          return `""`;
        }
      });
      csvData.push(oneCSVLine.join(',')); //convert to CSV
    }

    //save CSV
    const fileName = "export_specializari.csv";
    const buffer = "\uFEFF" + headers.join(",") + "\n" + csvData.join("\n");
    const blob = new Blob([buffer], {
      type: "text/csv;charset=utf8;",
    });
    const url = window.URL.createObjectURL(blob);
    const a = document.createElement("a");
    a.href = url;
    a.download = fileName;
    a.click();
  }

  const doImport = async () => {
    //for each line in textbox
    const importText = document.getElementById("importvocational").value;
    const importLines = importText.split("\n");

    for (const importLine of importLines) {

      const importValues = importLine.split("\t");

      const id = importValues[0];
      const numberOfRromaSeats = importValues[9];
      const numberOfCesSeats = importValues[10];

      if (isNaN(id)) {
        console.log("Skipping header")
      } else if (id > 0) {
        console.log("id: ", id)
        //execute update
        const getResult = await fetch(
          `${actionUrl}${id}?_dc=${new Date().getTime()}&id=${id}`,
          {
            method: "get",
            credentials: "same-origin",
            headers: {
              Accept: "application/json",
              "Content-type":
                "application/x-www-form-urlencoded; charset=UTF-8",
            }
          }
        );
        const jsonObj = await getResult.json();
        const payload = jsonObj.baseEntity;

        //modify payload
        payload.numberOfRromaSeats = numberOfRromaSeats;
        payload.numberOfCesSeats = numberOfCesSeats;

        await fetch(
          `${actionUrl}${id}?_dc=${new Date().getTime()}`,
          {
            method: "put",
            credentials: "same-origin",
            headers: {
              Accept: "application/json",
              "Content-type": "application/json",
            },
            body: JSON.stringify(payload)
          }
        );
      }
    }

    document.getElementById("importvocational").value = "Done!";
  }


  //create UI
  //main panel
  const mainPanel = document.createElement("div");
  mainPanel.className = "module-selector-module-container-horizontal";
  mainPanel.style = "position:fixed; top:2em; right:2em; margin:2em; width:400px; height:400px;padding:1em;";
  //export button
  const exportButton = document.createElement("button");
  exportButton.className = "x-btn";
  exportButton.style = "width:5em; height:2em;";
  exportButton.textContent = "Export";
  exportButton.onclick = doExport;
  //import button
  const importButton = document.createElement("button");
  importButton.className = "x-btn";
  importButton.style = "width:5em; height:2em;";
  importButton.textContent = "Import";
  importButton.onclick = doImport;
  //import textarea
  const importTextArea = document.createElement("textarea");
  importTextArea.style = "margin-top:4em; width:390px; height:320px;";
  importTextArea.className = "x-panel x-border-item x-box-item x-window-item x-panel-default";
  importTextArea.id = "importvocational";
  //assemble
  mainPanel.appendChild(exportButton);
  mainPanel.appendChild(importButton);
  mainPanel.appendChild(importTextArea);
  document.querySelector("body").appendChild(mainPanel);
})()
