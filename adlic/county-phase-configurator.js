let idCounty = 16
let phaseId = "2";



let url = "https://adlic.edu.ro/adlic/management/countyPhaseConfigurator/"+idCounty+ "?_dc=" + (new Date).getTime()

let payload = {"id":idCounty,"county":{"properties":{},"code":"CV","description":"COVASNA","id":16},"phase":{"id":phaseId}}

fetch(url, {
  method: 'put',
  credentials: 'same-origin',
  headers: {
      "Accept": "application/json",
      "Content-type": "application/json"
  },
  body: JSON.stringify(payload)
})
.then(res=>res.json())
.then(resJSON=>console.log(resJSON))
.catch(err=>console.error(err));
