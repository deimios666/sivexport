let idCentru = 16025;
let phaseId = "3";

let url = "https://adlic.edu.ro/adlic/management/enrollmentCenterPhaseConfigurator/"+idCentru+ "?_dc=" + (new Date).getTime()

let payload = {"id":idCentru,"enrollmentCenter":{"properties":{},"code":"3","description":"\u0218coala Gimnazial\u0103 \"Nicolae Colan\" Sf\u00e2ntu Gheorghe","id":idCentru,"county":{"properties":{},"code":"CV","description":"COVASNA","id":16}},"properties":{},"phase":{"id":phaseId}}

fetch(url, {
  method: 'put',
  credentials: 'same-origin',
  headers: {
      "Accept": "application/json",
      "Content-type": "application/json"
  },
  body: JSON.stringify(payload)
})
.then(res=>res.json())
.then(resJSON=>console.log(resJSON))
.catch(err=>console.error(err));
