const listUrl = "https://adlic.edu.ro/adlic/list/enrollmentCenter.json";
const listParams =
  "generatorKey=ECQG&filter=%5B%5D&filterName=%5B%5D&requestParams=%7B%7D&page=1&start=0&limit=1000&sort=%5B%7B%22property%22%3A%22code%22%2C%22direction%22%3A%22ASC%22%7D%5D";
const actionUrl =
  "https://adlic.edu.ro/adlic/management/userEnrollmentCenterOperator";
const actionParams = {
  to: null,
  passwordConfirmation: null,
  lastName: "", //<--Name
  phone: "",
  properties: null,
  from: null,
  password: "@@@@@",
  id: null,
  username: null,
  enrollmentCenter: {
    id: "", //<---plug in ID
  },
  email: "",
  roles: null,
  plainPassword: null,
  active: true,
  firstName: "", //<--Name
  nin: "",
};

const headers = ["Cod centru", "Unitatea", "Username", "Password"];

const csvData = [];

//IIFE for async
(async () => {
  const listResult = await fetch(listUrl + "?_dc=" + new Date().getTime(), {
    method: "post",
    credentials: "same-origin",
    headers: {
      Accept: "application/json",
      "Content-type": "application/x-www-form-urlencoded",
    },
    body: listParams,
  });
  const listJSON = await listResult.json();

  //for each centru
  for (const centru of listJSON.page.content) {
    const payload = {
      ...actionParams,
      enrollmentCenter: {
        id: "" + centru.id,
      },
      firstName: "Operator",
      lastName: "Centru " + centru.id,
    };
    const actionResult = await fetch(actionUrl + "?_dc=" + new Date().getTime(), {
      method: "post",
      credentials: "same-origin",
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
      },
      body: JSON.stringify(payload),
    });
    const actionJSON = await actionResult.json();

    //generate a CSV line
    const oneLine = [];
    oneLine.push(centru.code);
    oneLine.push(centru.description.split('"').join('""'));
    oneLine.push(actionJSON.baseEntity.username);
    oneLine.push(actionJSON.baseEntity.plainPassword);
    csvData.push('"' + oneLine.join('","') + '"');
  }

  const fileName = "export_conturi_adlic.csv";
  const buffer = "\uFEFF" + headers.join(",") + "\n" + csvData.join("\n");
  const blob = new Blob([buffer], {
    type: "text/csv;charset=utf8;",
  });

  const url = window.URL.createObjectURL(blob);
  const a = document.createElement("a");
  a.href = url;
  a.download = fileName;
  a.click();
})();
