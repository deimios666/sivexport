(async () => {
  const itemUrl = "https://adlic.edu.ro/adlic/list/candidate.json";
  const pageSize = 200;
  const itemListParamString = `generatorKey=CLQG&filter=%5B%7B%22property%22%3A%22numberOfOptions%22%2C%22criteria%22%3A%22GT%22%2C%22parameters%22%3A%5B0%5D%7D%5D&filterName=%5B%5D&requestParams=%7B%7D&sort=%5B%7B%22property%22%3A%22averageAdm%22%2C%22direction%22%3A%22DESC%22%7D%2C%7B%22property%22%3A%22averageEv%22%2C%22direction%22%3A%22DESC%22%7D%2C%7B%22property%22%3A%22averageAbs%22%2C%22direction%22%3A%22DESC%22%7D%2C%7B%22property%22%3A%22noteLanguageRo%22%2C%22direction%22%3A%22DESC%22%7D%2C%7B%22property%22%3A%22noteMat%22%2C%22direction%22%3A%22DESC%22%7D%2C%7B%22property%22%3A%22noteLanguageNative%22%2C%22direction%22%3A%22DESC%22%7D%5D`;

  const actionUrl = "https://adlic.edu.ro/adlic/management/candidate/"; //903651?id=903651&_dc=1642160758860"

  //internal replaceAll
  String.prototype.replaceAll = function (search, replacement) {
    const target = this;
    if (this.search(`"`) > -1) {
      return target.split(search).join(replacement);
    }
    return this;
  };

  const primingResponse = await fetch(itemUrl + "?_dc=" + (new Date).getTime(), {
    method: 'post',
    credentials: 'same-origin',
    headers: {
      "Accept": "application/json",
      "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
      "X-Requested-With": "XMLHttpRequest"
    },
    body: itemListParamString + "&page=1&start=0&limit=1"
  });

  const CSVArray = [];
  const plan = [];

  const lastValues = {
    averageAdm: 0,
    averageEv: 0,
    averageAbs: 0,
    noteLanguageRo: 0,
    noteMat: 0,
    noteLanguageNative: 0,
    specializationCode: 0,
  }

  const primingResponseJSON = await primingResponse.json();
  const maxItems = primingResponseJSON.page.total;
  const maxPages = Math.ceil(maxItems / pageSize);

  console.log(`Candidates: ${maxItems}, Pages: ${maxPages}`);

  for (let page = 1; page <= maxPages; page++) {
    console.log(`Processing page: ${page}/${maxPages}`);
    const start = (page - 1) * pageSize;
    const listResponse = await fetch(itemUrl + "?_dc=" + (new Date).getTime(), {
      method: 'post',
      credentials: 'same-origin',
      headers: {
        "Accept": "application/json",
        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
        "X-Requested-With": "XMLHttpRequest"
      },
      body: itemListParamString + `&page=${page}&start=${start}&limit=${pageSize}`
    });
    const listJSON = await listResponse.json();

    for (const item of listJSON.page.content) {
      const id = item.id;
      const cnp = item.nin;
      const actionResponse = await fetch(actionUrl + id + "?id=" + id + "&_dc=" + (new Date).getTime(), {
        method: 'get',
        credentials: 'same-origin',
        headers: {
          "Accept": "application/json",
          "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
          "X-Requested-With": "XMLHttpRequest"
        }
      });
      const actionResponseJSON = await actionResponse.json();
      const options = actionResponseJSON.baseEntity.options;

      let admis = false;
      for (optItem of options) {
        if (!(optItem.specialization.code in plan)) {
          plan[optItem.specialization.code] = optItem.specialization.numberOfSeats;
        }
        if (plan[optItem.specialization.code] > 0) {
          //we have space, admit
          plan[optItem.specialization.code]--;
          CSVArray.push(`${cnp},${optItem.id},${optItem.numberOfOrder},${optItem.specialization.code},${plan[optItem.specialization.code]},${item.averageAdm}`);
          admis = true;
          break;
        }
      }
      if (!admis) CSVArray.push(`${cnp},,,NEADMIS,,${item.averageAdm}`);
    }
  }
  const fileName = "export_admisi.csv";
  const buffer = "\uFEFF" + "CNP,ID,ORD,COD,LOCURI_RAMASE,MEDIA_ADM\n" + CSVArray.join("\n");
  let blob = new Blob([buffer], {
    "type": "text/csv;charset=utf8;"
  });
  const link = document.createElement("a");
  link.href = URL.createObjectURL(blob);
  link.download = fileName;
  link.click();
  link.remove();
})()
