(async () => {
  const itemUrl = "https://adlic.edu.ro/adlic/list/excludedCandidate.json";
  const pageSize = 200;
  const itemListParamString = `generatorKey=CDLQG&filter=%5B%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%7D`;
  const actionUrl = "https://adlic.edu.ro/adlic/management/candidate/"; //903651?id=903651&_dc=1642160758860"

  //internal replaceAll
  String.prototype.replaceAll = function (search, replacement) {
    const target = this;
    if (this.search(`"`) > -1) {
      return target.split(search).join(replacement);
    }
    return this;
  };

  const primingResponse = await fetch(itemUrl + "?_dc=" + (new Date).getTime(), {
    method: 'post',
    credentials: 'same-origin',
    headers: {
      "Accept": "application/json",
      "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
      "X-Requested-With": "XMLHttpRequest"
    },
    body: itemListParamString + "&page=1&start=0&limit=1"
  });

  let CSVArray = [];

  const primingResponseJSON = await primingResponse.json();
  const maxItems = primingResponseJSON.page.total;
  const maxPages = Math.ceil(maxItems / pageSize);

  console.log(`Candidates: ${maxItems}, Pages: ${maxPages}`);

  for (let page = 1; page <= maxPages; page++) {
    console.log(`Processing page: ${page}/${maxPages}`);
    const start = (page - 1) * pageSize;
    const listResponse = await fetch(itemUrl + "?_dc=" + (new Date).getTime(), {
      method: 'post',
      credentials: 'same-origin',
      headers: {
        "Accept": "application/json",
        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
        "X-Requested-With": "XMLHttpRequest"
      },
      body: itemListParamString + `&page=${page}&start=${start}&limit=${pageSize}`
    });
    const listJSON = await listResponse.json();

    for await (const item of listJSON.page.content) {
      if (item.eraseDesc === "Repartizat in sesiunea 1 2021") {
        const id = item.id;
        const cnp = item.nin;
        const actionResponse = await fetch(actionUrl + id + "?id=" + id + "&_dc=" + (new Date).getTime(), {
          method: 'get',
          credentials: 'same-origin',
          headers: {
            "Accept": "application/json",
            "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
            "X-Requested-With": "XMLHttpRequest"
          }
        });
        const actionResponseJSON = await actionResponse.json();
        const options = actionResponseJSON.baseEntity.options;

        options.forEach(optItem => {
          CSVArray.push(`${cnp},${optItem.id},${optItem.numberOfOrder},${optItem.specialization.code}`);
        })
      }
    }
  }
  const fileName = "export_optiuni.csv";
  const buffer = "\uFEFF" + "CNP,ID,ORD,COD\n" + CSVArray.join("\n");
  let blob = new Blob([buffer], {
    "type": "text/csv;charset=utf8;"
  });
  const link = document.createElement("a");
  link.href = URL.createObjectURL(blob);
  link.download = fileName;
  link.click();
  link.remove();
})()
