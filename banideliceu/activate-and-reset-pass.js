//IIFE for async
(async () => {
  const listUrl = "https://euro.edu.ro/ssp/list/hsmUserSchoolOperator.json";
  const listParams = "generatorKey=HSMUSOQG&filter=%5B%5D&sort=%5B%7B%22property%22%3A%22username%22%2C%22direction%22%3A%22ASC%22%7D%5D&filterName=%5B%5D&requestParams=%7B%7D&page=1&start=0&limit=1000";
  const actionUrl1 = "https://euro.edu.ro/ssp/action/systemUserManagement/resetPassword.json";
  const actionParam1 = "entity=hsmUserSchoolOperator&systemUserId=";

  const actionUrl2 = ["https://euro.edu.ro/ssp/management/hsmUserSchoolOperator/", "?id="];
  const actionParams2 = {
    active: true
  };
  const headers = ["Unitatea", "Username", "Password"];

  const csvData = [];

  const listResult = await fetch(listUrl + "?_dc=" + new Date().getTime(), {
    method: "post",
    credentials: "same-origin",
    headers: {
      Accept: "application/json",
      "Content-type": "application/x-www-form-urlencoded",
    },
    body: listParams,
  });
  const listJSON = await listResult.json();

  //for each centru
  for (const centru of listJSON.page.content) {
    const actionResult1 = await fetch(actionUrl1 + "?_dc=" + new Date().getTime(), {
      method: "post",
      credentials: "same-origin",
      headers: {
        Accept: "application/json",
        "Content-type": "application/x-www-form-urlencoded",
      },
      body: actionParam1 + centru.id
    });
    const actionResultJSON1 = await actionResult1.json();

    const payload = Object.assign({}, centru);

    payload["active"] = true;
    payload["roles"] = null;
    payload["school"] = { id: centru.school.id };
    payload["password"] = "";
    payload["passwordConfirmation"] = "";
    payload["cnp"] = "";

    await fetch(actionUrl2[0] + centru.id + actionUrl2[1] + centru.id + "&_dc=" + new Date().getTime(), {
      method: "put",
      credentials: "same-origin",
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
      },
      body: JSON.stringify(payload),
    });

    //generate a CSV line
    const oneLine = [];
    oneLine.push(centru.school.name.split('"').join('""'));
    oneLine.push(centru.username);
    oneLine.push(actionResultJSON1.msg);
    csvData.push('"' + oneLine.join('","') + '"');
  }

  const fileName = "export_conturi_BDL.csv";
  const buffer = "\uFEFF" + headers.join(",") + "\n" + csvData.join("\n");
  const blob = new Blob([buffer], {
    type: "text/csv;charset=utf8;",
  });

  const url = window.URL.createObjectURL(blob);
  const a = document.createElement("a");
  a.href = url;
  a.download = fileName;
  a.click();
})();
