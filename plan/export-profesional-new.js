//IIFE for async
(async () => {
  const listUrl = "/siiir/list/profesional.json";
  const pageLimit = 1000;
  const schoolYearId = AMS.util.AppConfig.selectedYear.data.id;
  const countyId = 16; //Covasna
  const listParamsPrefix = `generatorKey=profesional_GRID_GENERATOR_KEY&filter=%5B%7B%22property%22%3A%22schoolYear.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B${schoolYearId}%5D%7D%2C%7B%22property%22%3A%22school.county.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B${countyId}%5D%7D%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A${schoolYearId}%7D`;
  const headers = [
    ["id", "id"],
    ["schoollongName", "schoolForPlan.longName"],
    ["educationTypecode", "educationType.code"],
    ["educationTypedescription", "educationType.description"],
    ["studyFormationTypecode", "studyFormationType.code"],
    ["studyFormationTypedescription", "studyFormationType.description"],
    ["teachingModecode", "teachingMode.code"],
    ["teachingModedescription", "teachingMode.description"],
    ["teachingTypecode", "teachingType.code"],
    ["teachingTypedescription", "teachingType.description"],
    ["teachingLanguagecode", "teachingLanguage.code"],
    ["teachingLanguagedescription", "teachingLanguage.description"],
    ["teachingLanguageteachingLanguage", "teachingLanguage.teachingLanguage"],
    ["teachingLanguagemodernLanguage", "teachingLanguage.modernLanguage"],
    ["teachingLanguagenativeLanguage", "teachingLanguage.nativeLanguage"],
    ["nativeLanguage", "nativeLanguage"],
    ["deficiencyNo", "deficiencyNo"],
    ["classNo", "classNo"],
    ["studentNo", "studentNo"],
    ["availableClassNo", "availableClassNo"],
    ["availableStudentNo", "availableStudentNo"],
    ["availableDeficiencyNo", "availableDeficiencyNo"],
    ["computedClassNo", "computedClassNo"],
    ["computedStudentNo", "computedStudentNo"],
    ["computedDeficiencyNo", "computedDeficiencyNo"],
    ["specializationQualificationfrom", "specializationQualification.from"],
    ["specializationQualificationto", "specializationQualification.to"],
    ["specializationQualificationcode", "specializationQualification.code"],
    ["specializationQualificationdescription", "specializationQualification.description"],
    ["specializationQualificationorderBy", "specializationQualification.orderBy"],
    ["specializationQualificationid", "specializationQualification.id"],
    ["specializationQualificationspecializationQualificationType", "specializationQualification.specializationQualificationType"],
    ["specializationQualificationtechnicalSchool", "specializationQualification.technicalSchool"],
    ["specializationQualificationadlicUse", "specializationQualification.adlicUse"],
    ["specializationQualificationadlicSpecialization", "specializationQualification.adlicSpecialization"],
    ["domainfrom", "domain.from"],
    ["domainto", "domain.to"],
    ["domaincode", "domain.code"],
    ["domaindescription", "domain.description"],
    ["domainorderBy", "domain.orderBy"],
    ["domainid", "domain.id"],
    ["domaindomainTypefrom", "domain.domainType.from"],
    ["domaindomainTypeto", "domain.domainType.to"],
    ["domaindomainTypecode", "domain.domainType.code"],
    ["domaindomainTypedescription", "domain.domainType.description"],
    ["domaindomainTypeorderBy", "domain.domainType.orderBy"],
    ["domaindomainTypeid", "domain.domainType.id"],
    ["domainparentDomainfrom", "domain.parentDomain.from"],
    ["domainparentDomainto", "domain.parentDomain.to"],
    ["domainparentDomaincode", "domain.parentDomain.code"],
    ["domainparentDomaindescription", "domain.parentDomain.description"],
    ["domainparentDomainorderBy", "domain.parentDomain.orderBy"],
    ["domainparentDomainid", "domain.parentDomain.id"],
    ["domainparentDomaindomainTypefrom", "domain.parentDomain.domainType.from"],
    ["domainparentDomaindomainTypeto", "domain.parentDomain.domainType.to"],
    ["domainparentDomaindomainTypecode", "domain.parentDomain.domainType.code"],
    ["domainparentDomaindomainTypedescription", "domain.parentDomain.domainType.description"],
    ["domainparentDomaindomainTypeorderBy", "domain.parentDomain.domainType.orderBy"],
    ["domainparentDomaindomainTypeid", "domain.parentDomain.domainType.id"],
    ["domainparentDomainparentDomain", "domain.parentDomain.parentDomain"],
    ["domainparentDomainadlicUse", "domain.parentDomain.adlicUse"],
    ["domainparentDomainadlicSpecializationfrom", "domain.parentDomain.adlicSpecialization.from"],
    ["domainparentDomainadlicSpecializationto", "domain.parentDomain.adlicSpecialization.to"],
    ["domainparentDomainadlicSpecializationcode", "domain.parentDomain.adlicSpecialization.code"],
    ["domainparentDomainadlicSpecializationdescription", "domain.parentDomain.adlicSpecialization.description"],
    ["domainparentDomainadlicSpecializationid", "domain.parentDomain.adlicSpecialization.id"],
    ["domainparentDomainadlicSpecializationprofileCode", "domain.parentDomain.adlicSpecialization.profileCode"],
    ["domainadlicUse", "domain.adlicUse"],
    ["domainadlicSpecialization", "domain.adlicSpecialization"],
    ["isTrainingStage", "isTrainingStage"],
    ["domainparentDomainadlicSpecialization", "domain.parentDomain.adlicSpecialization"],
    ["domainadlicSpecializationfrom", "domain.adlicSpecialization.from"],
    ["domainadlicSpecializationto", "domain.adlicSpecialization.to"],
    ["domainadlicSpecializationcode", "domain.adlicSpecialization.code"],
    ["domainadlicSpecializationdescription", "domain.adlicSpecialization.description"],
    ["domainadlicSpecializationid", "domain.adlicSpecialization.id"],
    ["domainadlicSpecializationprofileCode", "domain.adlicSpecialization.profileCode"],
    ["isDual", "isDual"],
  ];

  //restore console
  let iFrame = document.createElement('iframe');
  iFrame.style.display = 'none';
  document.body.appendChild(iFrame);
  window.console = iFrame.contentWindow.console;

  const csvData = [];

  //get nr of results
  const primingListResult = await fetch(listUrl + "?_dc=" + new Date().getTime(), {
    method: "post",
    credentials: "same-origin",
    headers: {
      Accept: "application/json",
      "Content-type": "application/x-www-form-urlencoded",
    },
    body: listParamsPrefix + "&limit=1&page=1&start=0",
  });

  const primingListJSON = await primingListResult.json();

  const maxItems = primingListJSON.page.total;
  const maxPages = Math.ceil(maxItems / pageLimit); //calculate nr of pages

  for (let page = 1; page <= maxPages; page++) {

    const start = (page - 1) * pageLimit;

    console.log(`Downloading page ${page} of ${maxPages}`);

    const listResult = await fetch(listUrl + "?_dc=" + new Date().getTime(), {
      method: "post",
      credentials: "same-origin",
      headers: {
        Accept: "application/json",
        "Content-type": "application/x-www-form-urlencoded",
      },
      body: listParamsPrefix + `&limit=${pageLimit}&page=${page}&start=${start}`,
    });

    const listJSON = await listResult.json();

    const pageCSVData = await Promise.all(
      listJSON.page.content.map(
        async (item, index) => {
          return headers.map(key => {
            const keyPath = key[1];
            try {
              const value = keyPath.split('.')
                .reduce((o, i) => o[i], item) //resolve value
                .toString()
                .split('"').join('""'); //escape quotes
              return `"${value}"`; //return the formatted field
            } catch (err) {
              return `""`;
            }
          }).join(',');
        }
      )
    );
    csvData.push(pageCSVData.join("\n"));
  }

  const dateString = (new Date()).toISOString().substring(0, 10);
  const fileName = `export_plan_profesional_${dateString}.csv`;
  const buffer = "\uFEFF" + headers.map(h => `"${h[0]}"`).join(",") + "\n" + csvData.join("\n");
  const blob = new Blob([buffer], {
    type: "text/csv;charset=utf8;",
  });
  const url = window.URL.createObjectURL(blob);
  const a = document.createElement("a");
  a.href = url;
  a.download = fileName;
  a.click();
})();