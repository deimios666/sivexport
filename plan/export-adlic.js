//DOES NOT WORK
(async () => {
  const listUrl = "https://www.siiir.edu.ro/siiir/list/asocSchoolPlanReplicSpecializationView.json";
  const pageLimit = 1000;
  const schoolYearId = AMS.util.AppConfig.selectedYear.data.id;
  const listParamsPrefix = `generatorKey=ASPRSVQG&filter=%5B%5D&sort=%5B%7B%22property%22%3A%22schoolName%22%2C%22direction%22%3A%22ASC%22%7D%2C%7B%22property%22%3A%22specialization%22%2C%22direction%22%3A%22ASC%22%7D%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A${schoolYearId}%7D`;
  const headers = [
    ["Nr", "id"],
    ["Cod judet", "school.county.code"],
    ["Cod specializare", "specializationCode"],
    ["Cod liceu", "school.code"],
    ["Denumire liceu", "schoolName"],
    ["Nivel invatamant", ""],
    ["Forma invatamant", "educationFormCode"],
    ["Limba predare","teachingLanguage.code"],
    ["Filiera",""],
    ["Profil",""],
    ["Bilingv","modernLanguage.code"],
    ["Specializare",""],
    ["Denumire specializare","specialization"],
    ["Nr locuri","classNo"],
    ["Nr clase","studentNo"]
  ];

  //restore console
  let iFrame = document.createElement('iframe');
  iFrame.style.display = 'none';
  document.body.appendChild(iFrame);
  window.console = iFrame.contentWindow.console;

  const csvData = [];

  //get nr of results
  const primingListResult = await fetch(listUrl + "?_dc=" + new Date().getTime(), {
    method: "post",
    credentials: "same-origin",
    headers: {
      Accept: "application/json",
      "Content-type": "application/x-www-form-urlencoded",
    },
    body: listParamsPrefix + "&limit=1&page=1&start=0",
  });

  const primingListJSON = await primingListResult.json();

  const maxItems = primingListJSON.page.total;
  const maxPages = Math.ceil(maxItems / pageLimit); //calculate nr of pages

  for (let page = 1; page <= maxPages; page++) {

    const start = (page - 1) * pageLimit;

    console.log(`Downloading page ${page} of ${maxPages}`);

    const listResult = await fetch(listUrl + "?_dc=" + new Date().getTime(), {
      method: "post",
      credentials: "same-origin",
      headers: {
        Accept: "application/json",
        "Content-type": "application/x-www-form-urlencoded",
      },
      body: listParamsPrefix + `&limit=${pageLimit}&page=${page}&start=${start}`,
    });

    const listJSON = await listResult.json();

    const pageCSVData = await Promise.all(
      listJSON.page.content.map(
        async (item, index) => {
          return headers.map(key => {
            const keyPath = key[1];
            try {
              const value = keyPath.split('.')
                .reduce((o, i) => o[i], item) //resolve value
                .toString()
                .split('"').join('""'); //escape quotes
              return `"${value}"`; //return the formatted field
            } catch (err) {
              return `""`;
            }
          }).join(',');
        }
      )
    );
    csvData.push(pageCSVData.join("\n"));
  }

  const dateString = (new Date()).toISOString().substring(0, 10);
  const fileName = `export_plan_adlic_${dateString}.csv`;
  const buffer = "\uFEFF" + headers.map(h => `"${h[0]}"`).join(",") + "\n" + csvData.join("\n");
  const blob = new Blob([buffer], {
    type: "text/csv;charset=utf8;",
  });
  const url = window.URL.createObjectURL(blob);
  const a = document.createElement("a");
  a.href = url;
  a.download = fileName;
  a.click();
})();