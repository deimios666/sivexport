(async () => {
  const plans = [
    "anteprescolar",
    "prescolar",
    "primar",
    "gimnazial",
    "liceal",
    "profesional",
    "postliceal",
    "clubSportivScolar",
  ];

  const data = [{ "id": 1376916, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1376918, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1376907, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1382818, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1377399, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1352433, "deficiencyNo": 1, "nivel": "liceal" },
  { "id": 1352526, "deficiencyNo": 1, "nivel": "liceal" },
  { "id": 1357070, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1351511, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1351728, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1351799, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1358635, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1358512, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1367350, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1367358, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1367544, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1370853, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1380887, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1380890, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1379250, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1379256, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1379215, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1379220, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1379112, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1379072, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1353349, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1352285, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1353480, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1353374, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1358861, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1358883, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1358863, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1358887, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1380633, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1352175, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1359880, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1369863, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1360227, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1368095, "deficiencyNo": 1, "nivel": "profesional" },
  { "id": 1368151, "deficiencyNo": 1, "nivel": "profesional" },
  { "id": 1359012, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1369715, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1358764, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1369819, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1376429, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1366022, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1359379, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1359305, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1355248, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1355293, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1355289, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1399047, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1351861, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1351798, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1354852, "deficiencyNo": 1, "nivel": "profesional" },
  { "id": 1399728, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1399406, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1399179, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1398938, "deficiencyNo": 1, "nivel": "prescolar" },
  { "id": 1398930, "deficiencyNo": 1, "nivel": "prescolar" },
  { "id": 1379463, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1379434, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1378116, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1378118, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1378120, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1380699, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1382654, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1378656, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1378598, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1378616, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1352630, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1355769, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1355770, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1379398, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1380259, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1380545, "deficiencyNo": 1, "nivel": "liceal" },
  { "id": 1380547, "deficiencyNo": 1, "nivel": "liceal" },
  { "id": 1380243, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1381463, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1381518, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1381440, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1381447, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1381437, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1350863, "deficiencyNo": 1, "nivel": "prescolar" },
  { "id": 1351027, "deficiencyNo": 1, "nivel": "prescolar" },
  { "id": 1351190, "deficiencyNo": 1, "nivel": "prescolar" },
  { "id": 1368654, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1368660, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1374910, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1368618, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1368622, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1381635, "deficiencyNo": 1, "nivel": "liceal" },
  { "id": 1359098, "deficiencyNo": 1, "nivel": "liceal" },
  { "id": 1380756, "deficiencyNo": 1, "nivel": "liceal" },
  { "id": 1380777, "deficiencyNo": 1, "nivel": "profesional" },
  { "id": 1380771, "deficiencyNo": 1, "nivel": "profesional" },
  { "id": 1380773, "deficiencyNo": 1, "nivel": "profesional" },
  { "id": 1380818, "deficiencyNo": 1, "nivel": "profesional" },
  { "id": 1380819, "deficiencyNo": 1, "nivel": "profesional" },
  { "id": 1380817, "deficiencyNo": 1, "nivel": "profesional" },
  { "id": 1380794, "deficiencyNo": 1, "nivel": "profesional" },
  { "id": 1380820, "deficiencyNo": 1, "nivel": "profesional" },
  { "id": 1380821, "deficiencyNo": 1, "nivel": "profesional" },
  { "id": 1381889, "deficiencyNo": 1, "nivel": "profesional" },
  { "id": 1356981, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1357084, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1357176, "deficiencyNo": 1, "nivel": "liceal" },
  { "id": 1356967, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1357383, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1379765, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1379524, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1382350, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1382340, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1382343, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1382344, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1382345, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1382334, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1382336, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1354536, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1367462, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1367485, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1367404, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1367399, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1367395, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1367382, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1367386, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1367379, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1362623, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1362653, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1362524, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1362544, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1362555, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1362550, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1367038, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1366857, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1366903, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1367084, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1367131, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1366967, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1362184, "deficiencyNo": 1, "nivel": "profesional" },
  { "id": 1362231, "deficiencyNo": 1, "nivel": "profesional" },
  { "id": 1370756, "deficiencyNo": 1, "nivel": "profesional" },
  { "id": 1370789, "deficiencyNo": 1, "nivel": "profesional" },
  { "id": 1377984, "deficiencyNo": 1, "nivel": "liceal" },
  { "id": 1377993, "deficiencyNo": 1, "nivel": "liceal" },
  { "id": 1349956, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1376686, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1376517, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1365236, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1365188, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1365190, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1376809, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1376822, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1362564, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1362405, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1362296, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1362300, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1378070, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1378062, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1379023, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1379048, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1379054, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1379060, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1378844, "deficiencyNo": 1, "nivel": "primar" },
  { "id": 1371866, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1371880, "deficiencyNo": 1, "nivel": "gimnazial" },
  { "id": 1376920, "deficiencyNo": 2, "nivel": "gimnazial" },
  { "id": 1353498, "deficiencyNo": 2, "nivel": "profesional" },
  { "id": 1357139, "deficiencyNo": 2, "nivel": "gimnazial" },
  { "id": 1358667, "deficiencyNo": 2, "nivel": "gimnazial" },
  { "id": 1370856, "deficiencyNo": 2, "nivel": "gimnazial" },
  { "id": 1370709, "deficiencyNo": 2, "nivel": "primar" },
  { "id": 1380870, "deficiencyNo": 2, "nivel": "primar" },
  { "id": 1370948, "deficiencyNo": 2, "nivel": "gimnazial" },
  { "id": 1353328, "deficiencyNo": 2, "nivel": "gimnazial" },
  { "id": 1353420, "deficiencyNo": 2, "nivel": "gimnazial" },
  { "id": 1369860, "deficiencyNo": 2, "nivel": "gimnazial" },
  { "id": 1369892, "deficiencyNo": 2, "nivel": "gimnazial" },
  { "id": 1365053, "deficiencyNo": 2, "nivel": "liceal" },
  { "id": 1365083, "deficiencyNo": 2, "nivel": "liceal" },
  { "id": 1368113, "deficiencyNo": 2, "nivel": "profesional" },
  { "id": 1369766, "deficiencyNo": 2, "nivel": "primar" },
  { "id": 1369822, "deficiencyNo": 2, "nivel": "gimnazial" },
  { "id": 1355285, "deficiencyNo": 2, "nivel": "gimnazial" },
  { "id": 1355316, "deficiencyNo": 2, "nivel": "gimnazial" },
  { "id": 1354864, "deficiencyNo": 2, "nivel": "profesional" },
  { "id": 1399707, "deficiencyNo": 2, "nivel": "primar" },
  { "id": 1379484, "deficiencyNo": 2, "nivel": "gimnazial" },
  { "id": 1382639, "deficiencyNo": 2, "nivel": "primar" },
  { "id": 1352596, "deficiencyNo": 2, "nivel": "gimnazial" },
  { "id": 1379392, "deficiencyNo": 2, "nivel": "gimnazial" },
  { "id": 1379210, "deficiencyNo": 2, "nivel": "primar" },
  { "id": 1380264, "deficiencyNo": 2, "nivel": "gimnazial" },
  { "id": 1381533, "deficiencyNo": 2, "nivel": "gimnazial" },
  { "id": 1381485, "deficiencyNo": 2, "nivel": "gimnazial" },
  { "id": 1381588, "deficiencyNo": 2, "nivel": "liceal" },
  { "id": 1381451, "deficiencyNo": 2, "nivel": "primar" },
  { "id": 1381656, "deficiencyNo": 2, "nivel": "profesional" },
  { "id": 1359289, "deficiencyNo": 2, "nivel": "profesional" },
  { "id": 1380755, "deficiencyNo": 2, "nivel": "liceal" },
  { "id": 1380782, "deficiencyNo": 2, "nivel": "profesional" },
  { "id": 1356978, "deficiencyNo": 2, "nivel": "gimnazial" },
  { "id": 1357123, "deficiencyNo": 2, "nivel": "liceal" },
  { "id": 1382337, "deficiencyNo": 2, "nivel": "primar" },
  { "id": 1354589, "deficiencyNo": 2, "nivel": "primar" },
  { "id": 1370149, "deficiencyNo": 2, "nivel": "primar" },
  { "id": 1367465, "deficiencyNo": 2, "nivel": "gimnazial" },
  { "id": 1367471, "deficiencyNo": 2, "nivel": "gimnazial" },
  { "id": 1367477, "deficiencyNo": 2, "nivel": "gimnazial" },
  { "id": 1367486, "deficiencyNo": 2, "nivel": "gimnazial" },
  { "id": 1367490, "deficiencyNo": 2, "nivel": "gimnazial" },
  { "id": 1367421, "deficiencyNo": 2, "nivel": "primar" },
  { "id": 1367419, "deficiencyNo": 2, "nivel": "primar" },
  { "id": 1367428, "deficiencyNo": 2, "nivel": "primar" },
  { "id": 1362664, "deficiencyNo": 2, "nivel": "gimnazial" },
  { "id": 1362535, "deficiencyNo": 2, "nivel": "primar" },
  { "id": 1366766, "deficiencyNo": 2, "nivel": "prescolar" },
  { "id": 1370920, "deficiencyNo": 2, "nivel": "liceal" },
  { "id": 1370657, "deficiencyNo": 2, "nivel": "profesional" },
  { "id": 1377987, "deficiencyNo": 2, "nivel": "liceal" },
  { "id": 1377646, "deficiencyNo": 2, "nivel": "prescolar" },
  { "id": 1376689, "deficiencyNo": 2, "nivel": "gimnazial" },
  { "id": 1376718, "deficiencyNo": 2, "nivel": "primar" },
  { "id": 1378078, "deficiencyNo": 2, "nivel": "gimnazial" },
  { "id": 1351252, "deficiencyNo": 2, "nivel": "gimnazial" },
  { "id": 1379036, "deficiencyNo": 2, "nivel": "gimnazial" },
  { "id": 1378991, "deficiencyNo": 2, "nivel": "primar" },
  { "id": 1379008, "deficiencyNo": 2, "nivel": "primar" },
  { "id": 1378832, "deficiencyNo": 2, "nivel": "primar" },
  { "id": 1357080, "deficiencyNo": 3, "nivel": "primar" },
  { "id": 1379247, "deficiencyNo": 3, "nivel": "gimnazial" },
  { "id": 1378999, "deficiencyNo": 3, "nivel": "primar" },
  { "id": 1359382, "deficiencyNo": 3, "nivel": "gimnazial" },
  { "id": 1355312, "deficiencyNo": 3, "nivel": "gimnazial" },
  { "id": 1399721, "deficiencyNo": 3, "nivel": "primar" },
  { "id": 1379381, "deficiencyNo": 3, "nivel": "gimnazial" },
  { "id": 1359240, "deficiencyNo": 3, "nivel": "profesional" },
  { "id": 1359253, "deficiencyNo": 3, "nivel": "profesional" },
  { "id": 1380780, "deficiencyNo": 3, "nivel": "profesional" },
  { "id": 1380787, "deficiencyNo": 3, "nivel": "profesional" },
  { "id": 1382339, "deficiencyNo": 3, "nivel": "gimnazial" },
  { "id": 1382338, "deficiencyNo": 3, "nivel": "primar" },
  { "id": 1354599, "deficiencyNo": 3, "nivel": "primar" },
  { "id": 1367479, "deficiencyNo": 3, "nivel": "gimnazial" },
  { "id": 1367436, "deficiencyNo": 3, "nivel": "primar" },
  { "id": 1379042, "deficiencyNo": 3, "nivel": "gimnazial" },
  { "id": 1379063, "deficiencyNo": 3, "nivel": "gimnazial" },
  { "id": 1353424, "deficiencyNo": 4, "nivel": "profesional" },
  { "id": 1357169, "deficiencyNo": 4, "nivel": "gimnazial" },
  { "id": 1368146, "deficiencyNo": 4, "nivel": "profesional" },
  { "id": 1399725, "deficiencyNo": 4, "nivel": "gimnazial" },
  { "id": 1399736, "deficiencyNo": 4, "nivel": "gimnazial" },
  { "id": 1379388, "deficiencyNo": 4, "nivel": "gimnazial" },
  { "id": 1359302, "deficiencyNo": 4, "nivel": "profesional" },
  { "id": 1380788, "deficiencyNo": 4, "nivel": "profesional" },
  { "id": 1354604, "deficiencyNo": 4, "nivel": "primar" },
  { "id": 1357146, "deficiencyNo": 5, "nivel": "gimnazial" },
  { "id": 1357153, "deficiencyNo": 5, "nivel": "gimnazial" },
  { "id": 1357086, "deficiencyNo": 5, "nivel": "primar" },
  { "id": 1357053, "deficiencyNo": 5, "nivel": "primar" },
  { "id": 1379248, "deficiencyNo": 5, "nivel": "gimnazial" },
  { "id": 1354545, "deficiencyNo": 5, "nivel": "gimnazial" },
  { "id": 1354512, "deficiencyNo": 6, "nivel": "gimnazial" },
  { "id": 1354540, "deficiencyNo": 7, "nivel": "gimnazial" },
  { "id": 1381901, "deficiencyNo": 12, "nivel": "profesional" },
  { "id": 1381887, "deficiencyNo": 14, "nivel": "profesional" }];

  for (const line of data) {
    const plan = line.nivel;
    const deficiencyNo = line.deficiencyNo;
    const planId = line.id;
    const schoolYearId = 24;

    //const listUrl=`https://www.siiir.edu.ro/siiir/list/${plan}.json?_dc=${_dc}`
    //const listParam=`generatorKey=${plan}_GRID_GENERATOR_KEY&filter=%5B%7B%22property%22%3A%22schoolYear.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B24%5D%7D%2C%7B%22property%22%3A%22school.internalId%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B11003153%5D%7D%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A${schoolYearId}%7D&page=1&start=0&limit=25`
    const readUrl = `https://www.siiir.edu.ro/siiir/management/${plan}/${planId}?_dc=${new Date().getTime()}&schoolYearId=${schoolYearId}&id=${planId}`;

    const readResult = await fetch(readUrl, {
      method: "get",
      credentials: "same-origin",
      headers: {
        Accept: "application/json",
      }
    })

    const readJSON = await readResult.json();

    const payload = readJSON.baseEntity;
    //increase version
    //payload.version = payload.version + 1;
    //set data
    payload.deficiencyNo = deficiencyNo;

    const editUrl = `https://www.siiir.edu.ro/siiir/management/${plan}/${planId}?_dc=${new Date().getTime()}&schoolYearId=${schoolYearId}`

    const editResult = await fetch(editUrl, {
      method: "put",
      credentials: "same-origin",
      headers: {
        Accept: "application/json",
        "Content-type": "application/json;charset=UTF-8"
      },
      body: JSON.stringify(payload)
    })
  }
})();