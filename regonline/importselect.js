(async () => {

  const div = document.createElement("div");
  div.style.top = 0;
  div.style.right = 0;
  div.style.height = "300px";
  div.style.width = "400px";
  div.style.position = "fixed";

  const inputField = document.createElement("textarea");
  inputField.classList.add("ui-inputfield", "ui-inputtext", "ui-widget", "ui-state-default", "ui-corner-all");
  inputField.style.width = "400px";

  const button = document.createElement("button");
  button.classList.add(
    "ui-button",
    "ui-widget",
    "ui-state-default",
    "ui-corner-all",
    "ui-button-text-only"
  );
  button.style.top = 0;
  button.style.right = 0;
  button.style.height = "30px";
  button.style.width = "100px";

  button.onclick = async () => {
    let selected = 0;
    const searched = inputField.value;
    const needles = searched.split("\n");
    const scoli = document.querySelectorAll('td[role="gridcell"]');
    Array.from(scoli).forEach(item => {
      if (item.innerText.length > 0 && needles.includes(item.innerText)) {
        selected++;
        console.log(selected, ": ", item.innerText);
        const rowIndex = parseInt(item.parentElement.getAttribute('data-ri'));
        PrimeFaces.widgets.widget_targeting.selectRow(PrimeFaces.widgets.widget_targeting.findRow(rowIndex));
      }
    })
  }

  const buttonTextSpan = document.createElement("span");
  buttonTextSpan.classList.add("ui-button-text", "ui-c");

  const buttonText = document.createTextNode("Selectează");

  buttonTextSpan.appendChild(buttonText);
  button.appendChild(buttonTextSpan);

  div.appendChild(inputField);
  div.appendChild(button);
  document.getElementsByTagName("body")[0].appendChild(div);


})()