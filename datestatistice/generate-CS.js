(async () => {
  //Se rulează cu cont de ISJ pe pagina Cost Standard
  //MARK: Settings
  //setați pe false ca să sară peste situațiile finalizate
  const unlockBeforeGeneration = true;

  //setați pe true ca să finalizeze unitățile generate
  const lockAfterGeneration = false;

  /*date de importat. Formatul este codSIIR: { codRând: {rr: 0, ru: 0, mr: 0, mu: 0}
  codRând este codul rândului din SIIIR, așadar începe cu 0 și se termină cu 22. Decalat față de HG.
  unde: 
    rr = româna rural
    ru = româna urbană
    mr = minoritate rurală
    mu = minoritate urbană
  
  nu trebuie completate toate codurile de rânduri

  exemplu:
    const data = {
    "1461100015": { "0": { rr: 0, ru: 0, mr: 0, mu: 0 }, "1": { rr: 0, ru: 0, mr: 0, mu: 0 }, "2": { rr: 0, ru: 0, mr: 0, mu: 0 }, "3": { rr: 0, ru: 0, mr: 0, mu: 0 }, "3,1": { rr: 0, ru: 0, mr: 0, mu: 0 }, "4": { rr: 0, ru: 0, mr: 0, mu: 0 }, "5": { rr: 0, ru: 0, mr: 0, mu: 0 }, "6": { rr: 0, ru: 0, mr: 0, mu: 0 }, "7": { rr: 0, ru: 0, mr: 0, mu: 0 }, "7,1": { rr: 0, ru: 0, mr: 0, mu: 0 }, "8": { rr: 0, ru: 0, mr: 0, mu: 0 }, "8,1": { rr: 0, ru: 0, mr: 0, mu: 0 }, "9": { rr: 0, ru: 0, mr: 0, mu: 0 }, "10": { rr: 0, ru: 0, mr: 0, mu: 0 }, "10,1": { rr: 0, ru: 0, mr: 0, mu: 0 }, "11": { rr: 0, ru: 0, mr: 0, mu: 0 }, "11,1": { rr: 0, ru: 0, mr: 0, mu: 0 }, "12": { rr: 0, ru: 0, mr: 0, mu: 0 }, "13": { rr: 0, ru: 356, mr: 0, mu: 0 }, "14": { rr: 0, ru: 0, mr: 0, mu: 0 }, "15": { rr: 0, ru: 0, mr: 0, mu: 0 }, "16": { rr: 0, ru: 0, mr: 0, mu: 0 }, "17": { rr: 0, ru: 0, mr: 0, mu: 0 }, "18": { rr: 0, ru: 0, mr: 0, mu: 0 }, "19": { rr: 0, ru: 0, mr: 0, mu: 0 }, "19,1": { rr: 0, ru: 4, mr: 0, mu: 0 }, "20": { rr: 0, ru: 0, mr: 0, mu: 0 }, "21": { rr: 0, ru: 0, mr: 0, mu: 0 }, "21,1": { rr: 0, ru: 0, mr: 0, mu: 0 }, "22": { rr: 0, ru: 0, mr: 0, mu: 0 }, "...": { rr: 0, ru: 360, mr: 0, mu: 0 } },
    };
  */

  const data = {};

  const pageLimit = 500;

  //MARK: Config

  const schoolYearId = AMS.util.AppConfig.selectedYear.data.id;

  const listUrl = "/siiir/list/standardCostSchoolDTO.json";
  const listParams = `generatorKey=SCSDQG&filter=%5B%5D&sort=%5B%7B%22property%22%3A%22school.longName%22%2C%22direction%22%3A%22ASC%22%7D%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A${schoolYearId}%2C%22userLevel%22%3A%22COUNTY%22%7D`;

  const readUrl = "/siiir/list/sc/standardCostSchool";
  const readParams = schoolId => `generatorKey=standardCostSchool_GRID_GENERATOR_KEY&filter=%5B%7B%22property%22%3A%22school.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B${schoolId}%5D%7D%5D&sort=%5B%7B%22property%22%3A%22order%22%2C%22direction%22%3A%22ASC%22%7D%5D&filterName=%5B%5D&requestParams=%7B%7D&page=1&start=0&limit=50`;

  const getGenerateUrl = schoolId => `/siiir/generatedStandardCostSchool?schoolId=${schoolId}`

  const writeUrl = schoolId => `/siiir/management/sc/standardCostSchool?generatorKey=standardCostSchool_GRID_GENERATOR_KEY&filter=%5B%7B%22property%22%3A%22school.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B${schoolId}%5D%7D%5D&sort=%5B%7B%22property%22%3A%22order%22%2C%22direction%22%3A%22ASC%22%7D%5D&filterName=%5B%5D&requestParams=%7B%7D`;

  const lockUrl = "/siiir/closeSchoolCost.json";
  const lockUrlParams = schoolId => `id=${schoolId}&schoolYearId=${schoolYearId}&configKey=close_standard_cost_key`;

  const deblockUrl = "/siiir/openSchoolCost.json";
  const deblockUrlParams = lockUrlParams;

  //restore console
  let iFrame = document.createElement('iframe');
  iFrame.style.display = 'none';
  document.body.appendChild(iFrame);
  window.console = iFrame.contentWindow.console;

  //MARK: payloadTemplate
  const payloadTemplate = [{
    "code": "...",
    "id": null,
    "lineDescription": "TOTAL GENERAL",
    "order": 1,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 29,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 29,
    "teachingUrbanTotal": null
  },
  {
    "code": "0",
    "id": null,
    "lineDescription": "Antepreşcolar",
    "order": 2,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "1",
    "id": null,
    "lineDescription": "Preşcolar cu program normal - zi",
    "order": 3,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "2",
    "id": null,
    "lineDescription": "Preşcolar cu program prelungit/saptamanal - zi",
    "order": 4,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "3",
    "id": null,
    "lineDescription": "Primar - zi",
    "order": 5,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "3,1",
    "id": null,
    "lineDescription": "Primar - integrat - zi",
    "order": 6,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "4",
    "id": null,
    "lineDescription": "Primar \"step-by-step\" -zi",
    "order": 7,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "5",
    "id": null,
    "lineDescription": "Primar \"A doua şansă\"",
    "order": 8,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "6",
    "id": null,
    "lineDescription": "Primar vocational(altul decat specializarea muzică) - zi",
    "order": 9,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "7",
    "id": null,
    "lineDescription": "Primar cu specializarea muzică - zi",
    "order": 10,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "7,1",
    "id": null,
    "lineDescription": "Primar cu specializarea muzică in regim suplimentar - zi",
    "order": 11,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "8",
    "id": null,
    "lineDescription": "Gimnazial - zi",
    "order": 12,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "8,1",
    "id": null,
    "lineDescription": "Gimnazial - integrat - zi",
    "order": 13,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "9",
    "id": null,
    "lineDescription": "Gimnazial \"A doua şansă\"",
    "order": 14,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "10",
    "id": null,
    "lineDescription": "Gimnazial vocational(altul decat specializarea muzică) - zi",
    "order": 15,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "10,1",
    "id": null,
    "lineDescription": "Gimnazial vocational(altul decat specializarea muzică) regim suplimentar - zi",
    "order": 16,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "11",
    "id": null,
    "lineDescription": "Gimnazial cu specializarea muzică - zi",
    "order": 17,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "11,1",
    "id": null,
    "lineDescription": "Gimnazial cu specializarea muzică in regim suplimentar - zi",
    "order": 18,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "12",
    "id": null,
    "lineDescription": "Gimnazial - FR",
    "order": 19,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "13",
    "id": null,
    "lineDescription": "Liceal teoretic - zi",
    "order": 20,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "14",
    "id": null,
    "lineDescription": "Liceal teoretic - seral",
    "order": 21,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "15",
    "id": null,
    "lineDescription": "Liceal tehnologic, militar, pedagogic si teologic - zi",
    "order": 22,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "16",
    "id": null,
    "lineDescription": "Liceal tehnologic - seral",
    "order": 23,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "17",
    "id": null,
    "lineDescription": "Liceal artistic(toate specializarile, exceptie, specializarea muzică) şi sportiv - zi",
    "order": 24,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "18",
    "id": null,
    "lineDescription": "Liceal specializarea muzică - zi",
    "order": 25,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "19",
    "id": null,
    "lineDescription": "Liceal - FR",
    "order": 26,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "19,1",
    "id": null,
    "lineDescription": "Liceal integrat - zi",
    "order": 27,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "20",
    "id": null,
    "lineDescription": "Invatamant profesional(inclusiv stagiile de pregatire practică) - zi",
    "order": 28,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "21",
    "id": null,
    "lineDescription": "Postliceal/maistri - zi",
    "order": 29,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "21,1",
    "id": null,
    "lineDescription": "Postliceal/maistri - seral",
    "order": 30,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "22",
    "id": null,
    "lineDescription": "Cantine-camine (elevi cazati)",
    "order": 31,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  }];

  //MARK: Helper Functions
  const siiirGet = async (url) => fetch(`${url}${url.includes("?") ? "&" : "?"}_dc=${new Date().getTime()}`, {
    method: "get",
    credentials: "same-origin",
  });

  const siiirFetch = async (url, body) => fetch(`${url}${url.includes("?") ? "&" : "?"}_dc=${new Date().getTime()}`, {
    method: "post",
    credentials: "same-origin",
    headers: {
      Accept: "application/json",
      "Content-type": "application/x-www-form-urlencoded",
    },
    body: body,
  });

  const siiirPost = async (url, body) => fetch(`${url}${url.includes("?") ? "&" : "?"}_dc=${new Date().getTime()}`, {
    method: "post",
    credentials: "same-origin",
    headers: {
      Accept: "application/json",
      "Content-type": "application/json",
    },
    body: JSON.stringify(body),
  });

  const asyncLimit = async (tasks, concurrencyLimit) => {
    const results = [];

    const runTasks = async (tasksIterator) => {
      for (const [index, task] of tasksIterator) {
        try {
          results[index] = await task();
        } catch (error) {
          results[index] = new Error(`Failed with: ${error.message}`);
        }
      }
    }

    const workers = new Array(concurrencyLimit)
      .fill(tasks.entries())
      .map(runTasks);

    await Promise.allSettled(workers);

    return results;
  }

  const makePayloadWithExistingData = (template, existingValues, schoolId, school) => {
    //clone existing values so we can modify it later
    const dataArray = existingValues.slice(0);
    //copy template and set schoolId
    const payload = template.map(templateLine => { return { ...templateLine, "schoolId": schoolId, school } });
    const returnArray = [];
    for (const row of payload) {
      const dataForRowIndex = dataArray.findIndex(el => el.code === row.code);
      const dataForRow = dataArray[dataForRowIndex];
      if (dataForRow) {
        row.id = dataForRow.id;
        row.roTeachingRural = dataForRow.roTeachingRural;
        row.roTeachingUrban = dataForRow.roTeachingUrban;
        row.otherTeachingRural = dataForRow.otherTeachingRural;
        row.otherTeachingUrban = dataForRow.otherTeachingUrban;
        row.teachingRuralTotal = dataForRow.teachingRuralTotal;
        row.teachingUrbanTotal = dataForRow.teachingUrbanTotal;
      } else {
        row.id = null;
        row.roTeachingRural = null;
        row.roTeachingUrban = null;
        row.otherTeachingRural = null;
        row.otherTeachingUrban = null;
        row.teachingRuralTotal = null;
        row.teachingUrbanTotal = null;
      }
      dataArray.splice(dataForRowIndex, 1);
      returnArray.push(row);
    }
    return returnArray;
  }

  const mergePayloadWithData = (payload, data) => {
    if (!data) return payload;
    //iterate over data object
    for (const [key, row] of Object.entries(data)) {
      //if key is in payload
      if (payload[key]) {
        const { rr, ru, mu, mr } = row;
        //if value is an object
        payload[key] = {
          ...payload[key],
          "roTeachingRural": rr,
          "roTeachingUrban": ru,
          "otherTeachingRural": mr,
          "otherTeachingUrban": mu,
          "teachingRuralTotal": rr + mr,
          "teachingUrbanTotal": ru + mu
        }
      }
    }
  }

  const mergePayloadWithGenerated = (payload, generatedData) => {
    const dataArray = generatedData.slice(0);
    const returnArray = [];
    for (const row of payload) {
      const dataForRowIndex = dataArray.findIndex(el => el.code === row.code);
      const dataForRow = dataArray[dataForRowIndex];
      if (dataForRow) {
        row.roTeachingRural = dataForRow.roTeachingRural;
        row.roTeachingUrban = dataForRow.roTeachingUrban;
        row.otherTeachingRural = dataForRow.otherTeachingRural;
        row.otherTeachingUrban = dataForRow.otherTeachingUrban;
        row.teachingRuralTotal = dataForRow.teachingRuralTotal;
        row.teachingUrbanTotal = dataForRow.teachingUrbanTotal;
      }
      dataArray.splice(dataForRowIndex, 1);
      returnArray.push(row);
    }
    return returnArray;
  }

  //MARK: MAIN
  //priming list for pagination
  const primingListResult = await siiirFetch(listUrl, listParams + "&limit=1&page=1&start=0");
  const primingListJSON = await primingListResult.json();

  const maxItems = primingListJSON.page.total;
  const maxPages = Math.ceil(maxItems / pageLimit); //calculate nr of pages

  let counter = 0;
  for (let page = 1; page <= maxPages; page++) {
    counter++;
    const start = (page - 1) * pageLimit;
    console.log(`Processing page ${page} of ${maxPages}`);
    //list all cost standard
    const listResult = await siiirFetch(listUrl, listParams + `&limit=${pageLimit}&page=${page}&start=${start}`);
    const listJSON = await listResult.json();
    //for each:
    for (const line of listJSON.page.content) {
      //check if finalized and needs to be opened
      const { id, finalized, school } = line;

      if (finalized) {
        if (unlockBeforeGeneration) {
          await siiirFetch(deblockUrl, deblockUrlParams(school.internalId));
          console.log(`[${counter}/${maxItems}] ${school.longName} Unlocked`);
        } else {
          console.log(`[${counter}/${maxItems}] ${school.longName} Skipped (locked)`);
          continue;
        }
      }

      //read existing data
      const currentIdResult = await siiirFetch(readUrl, readParams(school.id));
      const currentIdJSON = await currentIdResult.json();
      const currentValues = currentIdJSON.page.content;

      //merge existing data with payload template
      const payload = makePayloadWithExistingData(payloadTemplate, currentValues, school.id, school);
      //press generate
      const generatedResult = await siiirGet(getGenerateUrl(school.id));
      const generatedJSON = await generatedResult.json();
      const generatedArray = generatedJSON.standardCostSchoolList;
      //merge import data with generated data
      const payloadWithGenerated = mergePayloadWithGenerated(payload, generatedArray);
      //merge payload with predefined data
      const finalPayload = mergePayloadWithData(payloadWithGenerated, data[school.code]);
      //save the data
      await siiirPost(writeUrl(school.id), finalPayload);
      //if lockAfterGeneration is true, lock the school
      if (lockAfterGeneration) {
        await siiirFetch(lockUrl, lockUrlParams(school.internalId));
        console.log(`[${counter}/${maxItems}] ${school.longName} Locked`);
      }
      console.log(`[${counter}/${maxItems}] ${school.longName} DONE`);
    }
  }
  console.log("DONE!");
})();
