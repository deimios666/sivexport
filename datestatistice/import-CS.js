(async () => {
  const pageLimit = 500;
  const schoolYearId = AMS.util.AppConfig.selectedYear.data.id;
  const listUrl = "/siiir/list/standardCostSchoolDTO.json";
  const listParams = `generatorKey=SCSDQG&filter=%5B%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A${schoolYearId}%2C%22userLevel%22%3A%22COUNTY%22%7D`;

  const readUrl = "/siiir/list/sc/standardCostSchool";
  const readParams = ["generatorKey=standardCostSchool_GRID_GENERATOR_KEY&filter=%5B%7B%22property%22%3A%22school.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B", "%5D%7D%5D&sort=%5B%7B%22property%22%3A%22order%22%2C%22direction%22%3A%22ASC%22%7D%5D&filterName=%5B%5D&requestParams=%7B%7D&page=1&start=0&limit=25"]; //school.id

  const writeUrl = "/siiir/management/sc/standardCostSchool";

  //data has the format of siiirCode: { lineCode: {} }
  //rr = romanian rural, ru = romanian urban, mr = minority rural, mu = minority urban
  //lineCode "..." is TOTAL


  const dataTemplate = {
    "1461100015": { "0": { rr: 0, ru: 0, mr: 0, mu: 0 }, "1": { rr: 0, ru: 0, mr: 0, mu: 0 }, "2": { rr: 0, ru: 0, mr: 0, mu: 0 }, "3": { rr: 0, ru: 0, mr: 0, mu: 0 }, "3,1": { rr: 0, ru: 0, mr: 0, mu: 0 }, "4": { rr: 0, ru: 0, mr: 0, mu: 0 }, "5": { rr: 0, ru: 0, mr: 0, mu: 0 }, "6": { rr: 0, ru: 0, mr: 0, mu: 0 }, "7": { rr: 0, ru: 0, mr: 0, mu: 0 }, "7,1": { rr: 0, ru: 0, mr: 0, mu: 0 }, "8": { rr: 0, ru: 0, mr: 0, mu: 0 }, "8,1": { rr: 0, ru: 0, mr: 0, mu: 0 }, "9": { rr: 0, ru: 0, mr: 0, mu: 0 }, "10": { rr: 0, ru: 0, mr: 0, mu: 0 }, "10,1": { rr: 0, ru: 0, mr: 0, mu: 0 }, "11": { rr: 0, ru: 0, mr: 0, mu: 0 }, "11,1": { rr: 0, ru: 0, mr: 0, mu: 0 }, "12": { rr: 0, ru: 0, mr: 0, mu: 0 }, "13": { rr: 0, ru: 356, mr: 0, mu: 0 }, "14": { rr: 0, ru: 0, mr: 0, mu: 0 }, "15": { rr: 0, ru: 0, mr: 0, mu: 0 }, "16": { rr: 0, ru: 0, mr: 0, mu: 0 }, "17": { rr: 0, ru: 0, mr: 0, mu: 0 }, "18": { rr: 0, ru: 0, mr: 0, mu: 0 }, "19": { rr: 0, ru: 0, mr: 0, mu: 0 }, "19,1": { rr: 0, ru: 4, mr: 0, mu: 0 }, "20": { rr: 0, ru: 0, mr: 0, mu: 0 }, "21": { rr: 0, ru: 0, mr: 0, mu: 0 }, "21,1": { rr: 0, ru: 0, mr: 0, mu: 0 }, "22": { rr: 0, ru: 0, mr: 0, mu: 0 }, "...": { rr: 0, ru: 360, mr: 0, mu: 0 } },
  };

  const data = {
    "1461100101":{"0":{rr:null,ru:null,mr:null,mu:null,},
"1":{rr:null,ru:null,mr:null,mu:null,},
"2":{rr:null,ru:null,mr:null,mu:null,},
"3":{rr:null,ru:148,mr:null,mu:null,},
"3,1":{rr:null,ru:9,mr:null,mu:null,},
"4":{rr:null,ru:103,mr:null,mu:null,},
"5":{rr:null,ru:null,mr:null,mu:null,},
"6":{rr:null,ru:null,mr:null,mu:null,},
"7":{rr:null,ru:null,mr:null,mu:null,},
"7,1":{rr:null,ru:null,mr:null,mu:null,},
"8":{rr:null,ru:253,mr:null,mu:null,},
"8,1":{rr:null,ru:2,mr:null,mu:null,},
"9":{rr:null,ru:null,mr:null,mu:null,},
"10":{rr:null,ru:null,mr:null,mu:null,},
"10,1":{rr:null,ru:null,mr:null,mu:null,},
"11":{rr:null,ru:null,mr:null,mu:null,},
"11,1":{rr:null,ru:null,mr:null,mu:null,},
"12":{rr:null,ru:null,mr:null,mu:null,},
"13":{rr:null,ru:405,mr:null,mu:null,},
"14":{rr:null,ru:null,mr:null,mu:null,},
"15":{rr:null,ru:85,mr:null,mu:null,},
"16":{rr:null,ru:null,mr:null,mu:null,},
"17":{rr:null,ru:null,mr:null,mu:null,},
"18":{rr:null,ru:null,mr:null,mu:null,},
"19":{rr:null,ru:null,mr:null,mu:null,},
"19,1":{rr:null,ru:4,mr:null,mu:null,},
"20":{rr:null,ru:null,mr:null,mu:null,},
"21":{rr:null,ru:null,mr:null,mu:null,},
"21,1":{rr:null,ru:null,mr:null,mu:null,},
"21,2":{rr:null,ru:null,mr:null,mu:null,},
"22":{rr:null,ru:53,mr:null,mu:null,},},
  }


  //restore console
  let iFrame = document.createElement('iframe');
  iFrame.style.display = 'none';
  document.body.appendChild(iFrame);
  window.console = iFrame.contentWindow.console;

  const payloadTemplate = [{
    "code": "...",
    "id": null,
    "lineDescription": "TOTAL GENERAL",
    "order": 1,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 29,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 29,
    "teachingUrbanTotal": null
  },
  {
    "code": "0",
    "id": null,
    "lineDescription": "Antepreşcolar",
    "order": 2,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "1",
    "id": null,
    "lineDescription": "Preşcolar cu program normal - zi",
    "order": 3,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "2",
    "id": null,
    "lineDescription": "Preşcolar cu program prelungit/saptamanal - zi",
    "order": 4,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "3",
    "id": null,
    "lineDescription": "Primar - zi",
    "order": 5,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "3,1",
    "id": null,
    "lineDescription": "Primar - integrat - zi",
    "order": 6,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "4",
    "id": null,
    "lineDescription": "Primar \"step-by-step\" -zi",
    "order": 7,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "5",
    "id": null,
    "lineDescription": "Primar \"A doua şansă\"",
    "order": 8,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "6",
    "id": null,
    "lineDescription": "Primar vocational(altul decat specializarea muzică) - zi",
    "order": 9,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "7",
    "id": null,
    "lineDescription": "Primar cu specializarea muzică - zi",
    "order": 10,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "7,1",
    "id": null,
    "lineDescription": "Primar cu specializarea muzică in regim suplimentar - zi",
    "order": 11,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "8",
    "id": null,
    "lineDescription": "Gimnazial - zi",
    "order": 12,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "8,1",
    "id": null,
    "lineDescription": "Gimnazial - integrat - zi",
    "order": 13,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "9",
    "id": null,
    "lineDescription": "Gimnazial \"A doua şansă\"",
    "order": 14,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "10",
    "id": null,
    "lineDescription": "Gimnazial vocational(altul decat specializarea muzică) - zi",
    "order": 15,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "10,1",
    "id": null,
    "lineDescription": "Gimnazial vocational(altul decat specializarea muzică) regim suplimentar - zi",
    "order": 16,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "11",
    "id": null,
    "lineDescription": "Gimnazial cu specializarea muzică - zi",
    "order": 17,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "11,1",
    "id": null,
    "lineDescription": "Gimnazial cu specializarea muzică in regim suplimentar - zi",
    "order": 18,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "12",
    "id": null,
    "lineDescription": "Gimnazial - FR",
    "order": 19,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "13",
    "id": null,
    "lineDescription": "Liceal teoretic - zi",
    "order": 20,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "14",
    "id": null,
    "lineDescription": "Liceal teoretic - seral",
    "order": 21,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "15",
    "id": null,
    "lineDescription": "Liceal tehnologic, militar, pedagogic si teologic - zi",
    "order": 22,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "16",
    "id": null,
    "lineDescription": "Liceal tehnologic - seral",
    "order": 23,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "17",
    "id": null,
    "lineDescription": "Liceal artistic(toate specializarile, exceptie, specializarea muzică) şi sportiv - zi",
    "order": 24,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "18",
    "id": null,
    "lineDescription": "Liceal specializarea muzică - zi",
    "order": 25,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "19",
    "id": null,
    "lineDescription": "Liceal - FR",
    "order": 26,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "19,1",
    "id": null,
    "lineDescription": "Liceal integrat - zi",
    "order": 27,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "20",
    "id": null,
    "lineDescription": "Invatamant profesional(inclusiv stagiile de pregatire practică) - zi",
    "order": 28,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "21",
    "id": null,
    "lineDescription": "Postliceal/maistri - zi",
    "order": 29,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "21,1",
    "id": null,
    "lineDescription": "Postliceal/maistri - seral",
    "order": 30,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  },
  {
    "code": "22",
    "id": null,
    "lineDescription": "Cantine-camine (elevi cazati)",
    "order": 31,
    "otherTeachingRural": null,
    "otherTeachingUrban": null,
    "properties": {},
    "roTeachingRural": 1,
    "roTeachingUrban": null,
    "school": null,
    "schoolId": 11349492,
    "teachingRuralTotal": 1,
    "teachingUrbanTotal": null
  }];

  const siiirFetch = async (url, body) => fetch(`${url}?_dc=${new Date().getTime()}`, {
    method: "post",
    credentials: "same-origin",
    headers: {
      Accept: "application/json",
      "Content-type": "application/x-www-form-urlencoded",
    },
    body: body,
  });

  const siiirPost = async (url, body) => fetch(`${url}?_dc=${new Date().getTime()}`, {
    method: "post",
    credentials: "same-origin",
    headers: {
      Accept: "application/json",
      "Content-type": "application/json",
    },
    body: JSON.stringify(body),
  });

  const makePayload = (template, values, schoolId, currentIds) => template.map(
    oneLine => {
      const currentValues = values[oneLine.code.toString()];
      let rt = currentValues.rr + currentValues.mr;
      if (rt === 0) rt = null;
      let ut = currentValues.ru + currentValues.mu;
      if (ut === 0) ut = null;
      return {
        ...oneLine,
        "id": currentIds[oneLine.code],
        "schoolId": schoolId,
        "roTeachingRural": currentValues.rr,
        "roTeachingUrban": currentValues.ru,
        "otherTeachingRural": currentValues.mr,
        "otherTeachingUrban": currentValues.mu,
        "teachingRuralTotal": rt,
        "teachingUrbanTotal": ut
      }
    }
  );

  //prepare data, generate totals

  Object.keys(data).forEach(key => {
    const currentData = data[key];
    const totals = Object.keys(currentData).reduce((acc, lineCode) => {
      if (lineCode.toString() === "22") return acc;
      const currentLine = currentData[lineCode];
      if (currentLine.rr !== null) {
        acc.rr += currentLine.rr;
      }
      if (currentLine.ru !== null) {
        acc.ru += currentLine.ru;
      }
      if (currentLine.mr !== null) {
        acc.mr += currentLine.mr;
      }
      if (currentLine.mu !== null) {
        acc.mu += currentLine.mu;
      }
      return acc;
    }, { rr: null, ru: null, mr: null, mu: null });
    data[key]["..."] = totals;
  })

  //get nr of results
  const primingListResult = await siiirFetch(listUrl, listParams + "&limit=1&page=1&start=0");

  const primingListJSON = await primingListResult.json();

  const maxItems = primingListJSON.page.total;
  const maxPages = Math.ceil(maxItems / pageLimit); //calculate nr of pages

  for (let page = 1; page <= maxPages; page++) {


    const start = (page - 1) * pageLimit;

    console.log(`Processing page ${page} of ${maxPages}`);

    const listResult = await siiirFetch(listUrl, listParams + `&limit=${pageLimit}&page=${page}&start=${start}`);

    const listJSON = await listResult.json();

    //do the actions
    for (const item of listJSON.page.content) {
      //for each list element do write
      console.log(`Processing: ${item.school.longName}`);
      const currentValues = data[item.school.code];
      if (currentValues) {
        const currentIdResult = await siiirFetch(readUrl, readParams[0] + item.school.id + readParams[1]);
        const currentIdJSON = await currentIdResult.json();
        const currentIds = currentIdJSON.page.content.reduce((acc, idline) => {
          acc[idline.code] = idline.id;
          return acc;
        }, {});
        const payload = makePayload(payloadTemplate, currentValues, item.school.id, currentIds);
        await siiirPost(writeUrl, payload);
      } else {
        console.log(`${item.school.longName} not found in data!`);
      }
    }
  }
  console.log("DONE!");
})()