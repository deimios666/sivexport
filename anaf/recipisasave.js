//Se utilizează pe pagina cu tabelul cu recipise după autentificarea cu index și CIF
(async () => {
  const clientZip = await import("https://cdn.jsdelivr.net/npm/client-zip/index.js");
  const results = Array.from(document.querySelectorAll("table table tbody tr"))
    .filter(tr => tr.querySelector("td + td") !== null && tr.querySelector("td + td").innerText.includes("D112"))
    .map(tr => tr.querySelector("td").innerText)
    .map(async id => await fetch(`https://www.anaf.ro/StareD112/ObtineRecipisa?numefisier=${id}.pdf`));
  const url = window.URL.createObjectURL(await clientZip.downloadZip(await Promise.all(results)).blob());
  const a = document.createElement("a");
  a.href = url;
  a.download = "recipisa.zip";
  a.click();
  a.remove();
  window.URL.revokeObjectURL(url);
})();
