 //SVG loading rgb(167, 197, 255)
    const svgDoc = new DOMParser().parseFromString(
      `<svg style="position: absolute; right:20px; bottom:20px;" width="38" height="38" viewBox="0 0 38 38" xmlns="http://www.w3.org/2000/svg" stroke="#a7c5ff">
          <g fill="none" fill-rule="evenodd">
              <g transform="translate(1 1)" stroke-width="4">
                  <circle stroke-opacity=".5" cx="18" cy="18" r="18"/>
                  <path d="M36 18c0-9.94-8.06-18-18-18">
                      <animateTransform
                          attributeName="transform"
                          type="rotate"
                          from="0 18 18"
                          to="360 18 18"
                          dur="1s"
                          repeatCount="indefinite"/>
                  </path>
              </g>
          </g>
      </svg>`, 'application/xml');

    const body = document.getElementsByTagName("body")[0];
    const svg = body.ownerDocument.importNode(svgDoc.documentElement, true);
    body.appendChild(svg);