//https://maximorlov.com/parallel-tasks-with-pure-javascript/

/**
 * Executes the given tasks in parallel with a given concurrency limit
 * @async
 * @function limit
 * @param {Array<function(): Promise<any>>} tasks Array of the async tasks to be executed
 * @param {Number} concurrencyLimit Must be a positive integer
 * @returns {Any[]} Array of return values and/or errors
 */
const limit = async (tasks, concurrencyLimit) => {
  const results = [];

  const runTasks = async (tasksIterator) => {
    for (const [index, task] of tasksIterator) {
      try {
        results[index] = await task();
      } catch (error) {
        results[index] = new Error(`Failed with: ${error.message}`);
      }
    }
  }

  const workers = new Array(concurrencyLimit)
    .fill(tasks.entries())
    .map(runTasks);

  await Promise.allSettled(workers);

  return results;
}
