(async ()=>{
  const headers = ["Candidat", "Nota"];
  let csvData = [];
  const names = Array.from(document.querySelectorAll(".card table tbody>tr td")).map(e=>e.innerText);
  const values = Array.from(document.querySelectorAll(".card table tbody>tr td input")).map(e=>e.value);
for (let i=0;i<names.length;i=i+2){
  csvData.push(`${names[i]},${values[i+1]}`);
}

const fileName = "export_note.csv";
  const buffer = "\uFEFF" + headers.join(",") + "\n" + csvData.join("\n");
  const blob = new Blob([buffer], {
    type: "text/csv;charset=utf8;",
  });

  const url = window.URL.createObjectURL(blob);
  const a = document.createElement("a");
  a.href = url;
  a.download = fileName;
  a.click();


})();