(async () => {
  //Se rulează pe pagina Candidați în interiorul unei probe
  const token = localStorage.getItem('token').replace(/"/g, '');
  const probaId = window.location.pathname.split('/').at(-1);
  const centerId = window.location.pathname.split('/').at(-3);
  const headers = {
    'Credentials': 'include',
    'Content-Type': 'application/json;charset=utf-8',
    'Authorization': `Bearer ${token}`,
  }
  const eleviUrl = `https://ev.edu.ro/api/v1/exam-mgmt/center-operator/centers/${centerId}/events/${probaId}/students?filter={"limit":500,"skip":0,"order":"fullName asc","personalNumber":"","classroomId":[]}`;
  const csvData = [];
  const eleviResponse = await fetch(eleviUrl, { headers });
  const elevi = await eleviResponse.json();

  const csvHeaders = ["studentId", "fullName", "siiirCode", "personalNumber", "discipline", "classroomId", "classroomName", "uploaded", "isAbsent"];
  csvData.push("\uFEFF" + csvHeaders.join(','));
  for (const elev of elevi) {
    const values = csvHeaders.map(itemHeader => {
      const value = elev[itemHeader] || "";
      //if value contains comma, then put it in quotes
      return `${value}`.includes(",") ? `"${value}"` : value;
    });
    csvData.push(values.join(','));
  }
  //generate CSV
  const fileName = "export_elevi_ev.csv";
  const buffer = csvData.join("\n");
  const blob = new Blob([buffer], {
    type: "text/csv;charset=utf8;",
  });
  const url = window.URL.createObjectURL(blob);
  const a = document.createElement("a");
  a.href = url;
  a.download = fileName;
  a.click();
})()