(async () => {
    //Se rulează pe pagina Candidați în interiorul unei probe
    const token = localStorage.getItem('token').replace(/"/g, '');
    const probaId = window.location.pathname.split('/').at(-1);
    const centerId = window.location.pathname.split('/').at(-3);
    const headers = {
        'Credentials': 'include',
        'Content-Type': 'application/json;charset=utf-8',
        'Authorization': `Bearer ${token}`,
    }
    const maxRecords = 30000;
    const saliUrl = (eventId) => `https://ev.edu.ro/api/v1/exam-mgmt/commission-operator/commissions/${centerId}/events/${eventId}/files-check`;
    const eleviUrl = `https://ev.edu.ro/api/v1/exam-mgmt/commission-operator/commissions/${centerId}/events/${probaId}/students?filter={"limit":${maxRecords},"skip":0,"order":"fullName asc","personalNumber":"","classroomId":[]}`;
    const csvData = [];

    const getSaliMapForProba = async (eventId) => {
        const saliResponse = await fetch(saliUrl(eventId), { headers });
        const sali = await saliResponse.json();
        const saliMap = new Map();
        for (const sal of sali) {
            saliMap.set(sal._id.classroomId, sal.classroom);
        }
        return saliMap;
    }

    const saliMaps = {};

    const eleviResponse = await fetch(eleviUrl, { headers });
    const elevi = await eleviResponse.json();

    const csvHeaders = ["Centru Examen", "Scoala provenienta", "Clasa", "CNP", "Nume complet", "Cod SIIIR"];
    const csvValues = ["center.name", "student.originSchool", "student.grade", "student.personalNumber", "student.fullName", "siiirCode"];
    const eventSubscriptionIds = new Map();

    const getDeepValue = (obj, path) => path.split('.').reduce((o, k) => o[k], obj);
    const escapeCsvValue = (value) => `"${(value + "").split('"').join('""')}"`

    for (const elev of elevi) {
        const values = csvValues.map(valuePath => {
            return escapeCsvValue(getDeepValue(elev, valuePath));
        });
        //parse eventSubscriptions
        let eventResults = [];
        const eventSubscriptions = elev.eventSubscriptions;
        for (const eventSubscription of eventSubscriptions) {
            if (!eventSubscriptionIds.has(eventSubscription.eventId)) {
                eventSubscriptionIds.set(eventSubscription.eventId, eventSubscriptionIds.size + 1);
            }
            const currentId = eventSubscriptionIds.get(eventSubscription.eventId);
            if (!saliMaps[eventSubscription.eventId]) {
                saliMaps[eventSubscription.eventId] = await getSaliMapForProba(eventSubscription.eventId);
            }
            const eventResult = {
                discipline: eventSubscription.discipline,
                isAbsent: eventSubscription.isAbsent,
                isRemoved: eventSubscription.isRemoved,
                language: eventSubscription.language,
                uploadFilename: eventSubscription.uploadFilename,
                //classroomId: eventSubscription.classroomId,
                classroomId: saliMaps[eventSubscription.eventId].get(eventSubscription.classroomId),

            }
            eventResults[currentId] = eventResult;
        }

        eventSubscriptionIds.size > 0 && eventSubscriptionIds.forEach((mapVal, mapIndex) => {
            const index = parseInt(mapVal);
            const eventResult = eventResults[index];
            console.log(eventResult);
            if (!eventResult) {
                values.push("", "", "", "", "", "");
            } else {
                values.push(
                    escapeCsvValue(eventResult.discipline),
                    eventResult.isAbsent ? "Absent" : "",
                    eventResult.isRemoved ? "Eliminat" : "",
                    eventResult.language,
                    eventResult.uploadFilename,
                    escapeCsvValue(eventResult.classroomId)
                );
            }
        });

        csvData.push(values.join(','));
    }
    //generate CSV
    const fileName = "export_elevi_ev.csv";
    const extraHeaders = [];
    for (let i = 0; i < eventSubscriptionIds.size; i++) {
        extraHeaders.push(`Disciplina${i + 1}`);
        extraHeaders.push(`Absent${i + 1}`);
        extraHeaders.push(`Eliminat${i + 1}`);
        extraHeaders.push(`Limba${i + 1}`);
        extraHeaders.push(`FisierÎncărcat${i + 1}`);
        extraHeaders.push(`IDClasă${i + 1}`);
    }
    const buffer = "\uFEFF" + csvHeaders.join(',') + "," + extraHeaders.join(',') + "\n" + csvData.join("\n");
    const blob = new Blob([buffer], {
        type: "text/csv;charset=utf8;",
    });
    const url = window.URL.createObjectURL(blob);
    const a = document.createElement("a");
    a.href = url;
    a.download = fileName;
    a.click();
})()