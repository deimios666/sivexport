(async () => {
  //Se rulează pe pagina Centre de examen -> Probe -> Proba scrisă-> Repartizează candidați

  const token = localStorage.getItem('token').replace(/"/g, '');
  const urlParamsRegex = /\/centre\/([^\/\?]+)\/candidati\/([^\/\?]+)/;
  const urlParams = urlParamsRegex.exec(window.location.pathname);
  const centerId = urlParams[1];
  const eventId = urlParams[2];

  const headers = {
    'Credentials': 'include',
    'Content-Type': 'application/json;charset=utf-8',
    'Authorization': `Bearer ${token}`,
  }

  //GET
  const listSaliUrl = `https://ev.edu.ro/api/v1/exam-mgmt/center-operator/centers/${centerId}/classrooms?filter={%22limit%22:1000,%22skip%22:0,%22order%22:%22name%20asc%22,%22where%22:{%22and%22:[{%22or%22:[{%22name%22:{%22like%22:%22.*.*%22,%22options%22:%22i%22}}]},{}]}}`;

  //POST
  const addSalaUrl = `https://ev.edu.ro/api/v1/exam-mgmt/center-operator/centers/${centerId}/classrooms`;
  const addSalaPayload = (name, seats) => JSON.stringify({ name, seats, centerId: centerId });

  //GET
  const assocListUrl = `https://ev.edu.ro/api/v1/exam-mgmt/center-operator/centers/${centerId}/events/${eventId}/students?filter={%22limit%22:10000,%22skip%22:0,%22order%22:%22fullName%20asc%22,%22personalNumber%22:%22%22,%22classroomId%22:[]}`;

  //PUT  
  const assocUrl = `https://ev.edu.ro/api/v1/exam-mgmt/center-operator/centers/${centerId}/events/${eventId}/students-assign`;
  const assocPayload = (classroomId, studentIds) => JSON.stringify({ classroomId, studentIds });

  //read existing sali into saliMap
  const saliMap = new Map();

  const loadSali = async () => {
    saliMap.clear();
    const saliResponse = await fetch(listSaliUrl, { headers });
    const saliJson = await saliResponse.json();
    for (const sala of saliJson) {
      saliMap.set(sala.name, { id: sala._id, seats: sala.seats });
    }
  }
  await loadSali();
  //read existing elevi into eleviMap
  const eleviMap = new Map();

  const loadElevi = async () => {
    eleviMap.clear();
    const eleviResponse = await fetch(assocListUrl, { headers });
    const eleviJson = await eleviResponse.json();
    for (const elev of eleviJson) {
      eleviMap.set(elev.personalNumber, elev.studentId);
    }
  }
  await loadElevi();

  //show textarea for input
  //create close button
  //<svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeSmall" focusable="false" viewBox="0 0 24 24" aria-hidden="true"><path d="M12 2C6.47 2 2 6.47 2 12s4.47 10 10 10 10-4.47 10-10S17.53 2 12 2zm5 13.59L15.59 17 12 13.41 8.41 17 7 15.59 10.59 12 7 8.41 8.41 7 12 10.59 15.59 7 17 8.41 13.41 12 17 15.59z"></path></svg>
  const closeButton = document.createElement('svg');
  closeButton.innerHTML = `<svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeSmall" focusable="false" viewBox="0 0 24 24" aria-hidden="true"><path d="M12 2C6.47 2 2 6.47 2 12s4.47 10 10 10 10-4.47 10-10S17.53 2 12 2zm5 13.59L15.59 17 12 13.41 8.41 17 7 15.59 10.59 12 7 8.41 8.41 7 12 10.59 15.59 7 17 8.41 13.41 12 17 15.59z"></path></svg>`;

  //create text node
  const salaText = document.createTextNode('Atenție scriptul se rulează pe ecranul Repartizează candidați cu utilizatorul de Centru Examen!!! \n\nCopiați din Excel două coloane cu capul de tabel: CNP\tNumeSala');

  const salaInput = document.createElement('textarea');
  salaInput.style.width = '40em';
  salaInput.style.height = '20em';
  salaInput.style.border = '1px solid black';

  //create button
  const button = document.createElement('button');
  button.innerText = 'Importă Asocieri Săli';
  button.style.marginTop = '1em';
  //MuiButtonBase-root MuiButton-root MuiButton-contained h-full MuiButton-containedPrimary MuiButton-fullWidth
  button.classList.add('MuiButtonBase-root', 'MuiButton-root', 'MuiButton-contained', 'h-full', 'MuiButton-containedPrimary', 'MuiButton-fullWidth');
  //create dialog box
  //MuiPaper-root MuiDialog-paper rounded-8 MuiDialog-paperScrollPaper MuiDialog-paperWidthSm MuiDialog-paperFullWidth MuiPaper-elevation24 MuiPaper-rounded
  const inputDialog = document.createElement('div');
  inputDialog.classList.add('MuiPaper-root', 'MuiDialog-paper', 'rounded-8', 'MuiDialog-paperScrollPaper', 'MuiDialog-paperWidthSm', 'MuiDialog-paperFullWidth', 'MuiPaper-elevation24', 'MuiPaper-rounded');
  inputDialog.style.position = 'fixed';
  inputDialog.style.top = '2em';
  inputDialog.style.padding = '2em';
  inputDialog.appendChild(closeButton);
  inputDialog.appendChild(salaText);
  inputDialog.appendChild(salaInput);
  inputDialog.appendChild(button);
  inputDialog.style.zIndex = 9999;
  const body = document.querySelector('body');
  body.appendChild(inputDialog);


  const doAssoc = async () => {
    const salaToCreateMap = new Map();
    const salaAssocMap = new Map();
    //read data from textarea
    const salaInputText = salaInput.value;
    const salaInputLines = salaInputText.split('\n');
    for (const salaInputLine of salaInputLines) {
      const salaInputLineData = salaInputLine.split('\t');
      const cnp = salaInputLineData[0];
      if (cnp.length !== 13) continue;
      const salaName = salaInputLineData[1];
      if (!saliMap.has(salaName)) {
        if (!salaToCreateMap.has(salaName)) {
          salaToCreateMap.set(salaName, { name: salaName, seats: 1 });
        } else {
          salaToCreateMap.set(salaName, { name: salaName, seats: salaToCreateMap.get(salaName).seats + 1 });
        }
      }
      if (!eleviMap.has(cnp)) {
        throw new Error(`CNP ${cnp} nu a fost găsit în lista de elevi`);
      }
      if (!salaAssocMap.has(salaName)) {
        salaAssocMap.set(salaName, [eleviMap.get(cnp)]);
      } else {
        salaAssocMap.set(salaName, [...salaAssocMap.get(salaName), eleviMap.get(cnp)]);
      }
    }
    //create new sali
    for (const [salaName, sala] of salaToCreateMap) {
      const payload = addSalaPayload(salaName, sala.seats);
      await fetch(addSalaUrl, {
        method: 'POST',
        headers,
        body: payload
      });
    }

    //realod saliMap
    await loadSali();

    //create new assoc
    for (const [salaName, cnps] of salaAssocMap) {
      const payload = assocPayload(saliMap.get(salaName).id, cnps);
      await fetch(assocUrl, {
        method: 'PUT',
        headers,
        body: payload
      });
    }

    salaInput.value = 'Importul a fost efectuat cu succes! Reîncarcă pagina.';
    //reload page
    window.location.reload();
  }

  button.onclick = doAssoc;

})();