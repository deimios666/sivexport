(async () => {
    //Se rulează cu cont de ISJ pe pagina de Probe -> Proba Respectivă -> Candidați
    const token = localStorage.getItem('token').replace(/"/g, '');

    //https://ev.edu.ro/comisii/659facdc50a1c2d2f3323137/proba/659facdc50a1c20791323152
    const urlParamsRegex = /\/comisii\/([^\/]+)\/proba\/([^\/?]+)/;
    const urlParams = urlParamsRegex.exec(window.location.pathname);
    const commissionId = urlParams[1];
    const probaId = urlParams[2];
    const headers = {
        'Credentials': 'include',
        'Content-Type': 'application/json;charset=utf-8',
        'Authorization': `Bearer ${token}`,
    }

    //https://ev.edu.ro/api/v1/exam-mgmt/commission-operator/commissions/659facdc50a1c2d2f3323137/events/659facdc50a1c20791323152/grade-report/download
    const downloadUrl = `https://ev.edu.ro/api/v1/exam-mgmt/commission-operator/commissions/${commissionId}/events/${probaId}/grade-report/download`;
    const downloadResponse = await fetch(downloadUrl, { headers });
    const downloadData = await downloadResponse.blob();
    const url = window.URL.createObjectURL(downloadData);
    const a = document.createElement("a");
    a.href = url;
    a.download = `catalog_${probaId}.xlsx`;
    a.click();

})()
