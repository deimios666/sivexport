//MUST BE USED ON THE PROBA SELECTION PAGE
(async () => {
    await import('https://unpkg.com/jszip@3.10.1/dist/jszip.min.js');
    const token = localStorage.getItem('token').replace(/"/g, '');
    const headers = {
        'Credentials': 'include',
        'Content-Type': 'application/json;charset=utf-8',
        'Authorization': `Bearer ${token}`,
    }

    const userCommissionId = window.location.pathname.split('/').at(-1);

    const zip = JSZip();

    const eventsUrl = `https://ev.edu.ro/api/v1/exam-mgmt/commission-operator/commissions/${userCommissionId}/events`;

    const eventsResponse = await fetch(eventsUrl, { headers });
    const events = await eventsResponse.json();

    for (const event of events) {
        const eventId = event._id;

        const eleviUrl = `https://ev.edu.ro/api/v1/exam-mgmt/commission-operator/commissions/${userCommissionId}/events/${eventId}/students?filter={%22limit%22:10000,%22skip%22:0,%22order%22:%22fullName%20asc%22,%22personalNumber%22:%22%22,%22isAppealed%22:false,%22pendingEvaluations%22:false,%22completedEvaluations%22:false}`;
        const eleviResponse = await fetch(eleviUrl, { headers });
        const elevi = await eleviResponse.json();

        let elevCounter = 0;
        const eleviMax = elevi.length;
        console.log(`Processing ${eleviMax} elevi la proba ${event.name}`);

        for (const elev of elevi) {
            const { commission, student, eventSubscriptions } = elev;
            let eventIndex = 0;
            for (eventIndex = 0; eventIndex < eventSubscriptions.length; eventIndex++) {
                if (eventSubscriptions[eventIndex].eventId === eventId) {
                    break;
                }
            }
            const eventDetails = eventSubscriptions[eventIndex];
            const studentId = elev._id;
            const comissionId = commission._id;
            if (eventDetails.isAbsent) {
                elevCounter++;
                console.log(`[${elevCounter}/${eleviMax}]: Absent`);
                continue;
            }

            const discipline = eventDetails.discipline.split('/').join('-');
            const studentName = student.fullName;
            const downloadUrl = `https://ev.edu.ro/api/v1/exam-mgmt/commission-operator/commissions/${comissionId}/events/${eventId}/students/${studentId}/answers?fullFile=true`;

            const downloadResponse = await fetch(downloadUrl, { headers });
            const pdfBlob = await downloadResponse.blob();
            const fileName = `${discipline}_${studentName}.pdf`.replace(/[/\\?%*:|"<>]/g, '-');
            zip.file(fileName, pdfBlob);

            elevCounter++;
            console.log(`[${elevCounter}/${eleviMax}]: ${fileName}`);
        }
        console.log(`Finished processing ${eleviMax} elevi la proba ${event.name}`);
    }
    const zipcontent = await zip.generateAsync({ type: "blob" });
    const link = document.createElement("a");
    link.href = URL.createObjectURL(zipcontent);
    link.download = "lucrari_ev.zip";
    link.click();
    link.remove();
    console.log("===DONE===");
})()