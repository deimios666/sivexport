(async () => {

    //create close button
    //<svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeSmall" focusable="false" viewBox="0 0 24 24" aria-hidden="true"><path d="M12 2C6.47 2 2 6.47 2 12s4.47 10 10 10 10-4.47 10-10S17.53 2 12 2zm5 13.59L15.59 17 12 13.41 8.41 17 7 15.59 10.59 12 7 8.41 8.41 7 12 10.59 15.59 7 17 8.41 13.41 12 17 15.59z"></path></svg>
    const closeButton = document.createElement('svg');
    closeButton.innerHTML = `<svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeSmall" focusable="false" viewBox="0 0 24 24" aria-hidden="true"><path d="M12 2C6.47 2 2 6.47 2 12s4.47 10 10 10 10-4.47 10-10S17.53 2 12 2zm5 13.59L15.59 17 12 13.41 8.41 17 7 15.59 10.59 12 7 8.41 8.41 7 12 10.59 15.59 7 17 8.41 13.41 12 17 15.59z"></path></svg>`;

    //create text node
    const salaText = document.createTextNode('Atenție scriptul se rulează pe ecranul cu lista sălilor!!! \n\nLipiți exportul excel al centrului de examen din aplicația Bac, ecranul cu săli de examen, butonul Export excel, începând cu "Sală de clasă", includeți capul de tabel:');

    const salaInput = document.createElement('textarea');
    salaInput.style.width = '40em';
    salaInput.style.height = '20em';
    salaInput.style.border = '1px solid black';

    //create text node
    const text = document.createTextNode('Lipiți exportul excel al centrului de examen din aplicația Bac, raportul CE-16, începând cu "CNP", includeți capul de tabel:');

    //create input textarea
    const input = document.createElement('textarea');
    input.style.width = '40em';
    input.style.height = '20em';
    input.style.border = '1px solid black';

    //create button
    const button = document.createElement('button');
    button.innerText = 'Importă Asocieri Săli';
    button.style.marginTop = '1em';
    //MuiButtonBase-root MuiButton-root MuiButton-contained h-full MuiButton-containedPrimary MuiButton-fullWidth
    button.classList.add('MuiButtonBase-root', 'MuiButton-root', 'MuiButton-contained', 'h-full', 'MuiButton-containedPrimary', 'MuiButton-fullWidth');

    //create dialog box
    //MuiPaper-root MuiDialog-paper rounded-8 MuiDialog-paperScrollPaper MuiDialog-paperWidthSm MuiDialog-paperFullWidth MuiPaper-elevation24 MuiPaper-rounded
    const inputDialog = document.createElement('div');
    inputDialog.classList.add('MuiPaper-root', 'MuiDialog-paper', 'rounded-8', 'MuiDialog-paperScrollPaper', 'MuiDialog-paperWidthSm', 'MuiDialog-paperFullWidth', 'MuiPaper-elevation24', 'MuiPaper-rounded');
    inputDialog.style.position = 'fixed';
    inputDialog.style.top = '2em';
    inputDialog.style.padding = '2em';
    inputDialog.appendChild(closeButton);
    inputDialog.appendChild(salaText);
    inputDialog.appendChild(salaInput);
    inputDialog.appendChild(text);
    inputDialog.appendChild(input);
    inputDialog.appendChild(button);
    inputDialog.style.zIndex = 9999;
    const body = document.querySelector('body');
    body.appendChild(inputDialog);

    closeButton.onclick = () => { body.removeChild(inputDialog); };

    button.onclick = async () => {
        const token = localStorage.getItem('token').replace(/"/g, '');
        const centerId = window.location.pathname.split('/').at(-1);
        const headers = {
            'Credentials': 'include',
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': `Bearer ${token}`,
        }
        const saliUrl = `https://ev.edu.ro/api/v1/exam-mgmt/center-operator/centers/${centerId}/classrooms?filter={"limit":1000,"skip":0,"order":"name asc","where":{"and":[{"or":[{"name":{"like":".*.*","options":"i"}}]},{}]}}`;
        const saliMap = new Map();

        //read existing sali into saliMap
        const saliResponse = await fetch(saliUrl, { headers });
        const saliJson = await saliResponse.json();
        console.log('Săli existente: ', saliJson.length);
        for (const sala of saliJson) {
            saliMap.set(sala.name, { id: sala._id, seats: sala.seats });
        }

        //read new sali into saliMap
        const newSali = salaInput.value.split('\n');
        //remove last empty line
        newSali.pop();
        const nrNewSali = newSali.length;
        const newSaliMap = new Map();
        for (let i = 1; i < nrNewSali; i++) {
            const newSala = newSali[i].split('\t');
            const salaName = newSala[0];
            const seats = parseInt(newSala[1]);
            if (!saliMap.has(salaName)) {
                newSaliMap.set(salaName, seats);
            }
        }
        console.log('Săli noi: ', newSaliMap.size);

        //create new sali
        for ([key, value] of newSaliMap) {
            const addSalaUrl = `https://ev.edu.ro/api/v1/exam-mgmt/center-operator/centers/${centerId}/classrooms`;
            const payload = {
                name: key,
                seats: value,
                centerId
            };
            const addSalaResponse = await fetch(addSalaUrl, {
                method: 'POST',
                headers,
                body: JSON.stringify(payload)
            });
            const addSalaJson = await addSalaResponse.json();
            saliMap.set(key, { id: addSalaJson._id, seats: addSalaJson.seats });
        }
        console.log('Total săli după adăugare: ', saliMap.size);

        //read existing elevi into eleviMap
        /*const eleviUrl = `https://ev.edu.ro/api/v1/exam-mgmt/center-operator/centers/${centerId}/students`;
        const eleviMap = new Map();
        const eleviResponse = await fetch(eleviUrl, { headers });
        const eleviJson = await eleviResponse.json();
        for (let i = 0; i < eleviJson.length; i++) {
            const elev = eleviJson[i];
            eleviMap.set(elev.personalNumber, elev.studentId);
        }*/

        const elevSalaMap = new Map();

        //for each row in table
        const rows = input.value.split('\n');
        //remove last empty line
        rows.pop();
        const nrRows = rows.length;
        //read cnp, salaEa,salaEb, salaEc, salaEd but skip first row
        for (let i = 1; i < nrRows; i++) {
            const rowData = rows[i].split('\t');
            const cnp = rowData[0];
            const salaEa = rowData[78];
            const salaEb = rowData[79];
            const salaEc = rowData[80];
            const salaEd = rowData[81];
            //create a map with cnp and salaEa, salaEb, salaEc, salaEd
            elevSalaMap.set(cnp, { salaEa, salaEb, salaEc, salaEd });
        }
        console.log('Total elevi în export: ', elevSalaMap.size);

        //get probe
        //GET https://ev.edu.ro/api/v1/exam-mgmt/center-operator/centers/64d11a6b8a1593adeed940d5/events
        const probeUrl = `https://ev.edu.ro/api/v1/exam-mgmt/center-operator/centers/${centerId}/events`;
        const probeResponse = await fetch(probeUrl, { headers });
        const probeJson = await probeResponse.json();
        const probeMap = new Map();
        for (const proba of probeJson) {
            //E.a), E.b), E.c), E.d)
            probeMap.set(proba.name.substring(proba.name.length - 4), proba._id);
        }

        //for each proba
        for ([probaName, probaId] of probeMap) {
            console.log('Procesez proba: ', probaName);

            let probaIndex = "";
            if (probaName === "E.a)") probaIndex = "salaEa";
            if (probaName === "E.b)") probaIndex = "salaEb";
            if (probaName === "E.c)") probaIndex = "salaEc";
            if (probaName === "E.d)") probaIndex = "salaEd";

            //get elevi for proba
            //GET https://ev.edu.ro/api/v1/exam-mgmt/center-operator/centers/64d11a6b8a1593adeed940d5/events/64d118a1c58e22a7e9e3d76e/students
            const eleviProbaUrl = `https://ev.edu.ro/api/v1/exam-mgmt/center-operator/centers/${centerId}/events/${probaId}/students?filter={"limit":1000,"skip":0,"order":"fullName asc","personalNumber":"","classroomId":[]}`;
            const eleviProbaResponse = await fetch(eleviProbaUrl, { headers });
            const eleviProbaJson = await eleviProbaResponse.json();
            const clasaElevAssoc = {}
            const eleviDeassoc = [];
            console.log(`Total elevi în proba ${probaName}: `, eleviProbaJson.length);
            //for each elev
            for (const elev of eleviProbaJson) {
                const cnp = elev.personalNumber;
                const newElevSala = elevSalaMap.get(cnp)[probaIndex];
                //if new sala is empty and elev has a sala assigned, deassign elev
                if (newElevSala.length === 0) {
                    if (elev.classroomName.length > 0) eleviDeassoc.push(elev.studentId);
                } else {
                    //if new sala differs from old sala, associate elev with new sala
                    const newElevSalaId = saliMap.get(newElevSala).id;
                    if (elev.classroomId !== newElevSalaId) {
                        if (!clasaElevAssoc[newElevSalaId]) {
                            clasaElevAssoc[newElevSalaId] = [elev.studentId];
                        } else {
                            clasaElevAssoc[newElevSalaId].push(elev.studentId);
                        }
                    }
                }
            }

            //deassign elevi
            //PUT https://ev.edu.ro/api/v1/exam-mgmt/center-operator/centers/64d11a6b8a1593adeed940d5/events/64d118a1c58e22a7e9e3d76e/students-deassign
            //{"studentIds":["64d1230d92dc6c31208d4276"]}
            if (eleviDeassoc.length > 0) {
                const eleviDeassocUrl = `https://ev.edu.ro/api/v1/exam-mgmt/center-operator/centers/${centerId}/events/${probaId}/students-deassign`;
                const payload = {
                    studentIds: eleviDeassoc
                };
                await fetch(eleviDeassocUrl, {
                    method: 'PUT',
                    headers,
                    body: JSON.stringify(payload)
                });
                console.log(`Deasociat ${eleviDeassoc.length} elevi de la clase`);
            }

            //for each cnp associate elevi with sala
            //PUT https://ev.edu.ro/api/v1/exam-mgmt/center-operator/centers/64d11a6b8a1593adeed940d5/events/64d118a1c58e22fbafe3d770/students-assign
            //{"classroomId":"64d32ef440b1b0e3b6b91a20","studentIds":["64d1230d92dc6c31208d42ac"]}
            for (const [salaId, elevi] of Object.entries(clasaElevAssoc)) {
                const eleviAssocUrl = `https://ev.edu.ro/api/v1/exam-mgmt/center-operator/centers/${centerId}/events/${probaId}/students-assign`;
                const payload = {
                    classroomId: salaId,
                    studentIds: elevi
                };
                await fetch(eleviAssocUrl, {
                    method: 'PUT',
                    headers,
                    body: JSON.stringify(payload)
                });
                console.log(`Asociat ${elevi.length} elevi la sala ${salaId}`);
            }
        }
        console.log('====Done====');
    }

})()