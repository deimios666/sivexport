(async () => {
    //Se rulează pe pagina Candidați în interiorul unei probe

    await import("https://cdn.sheetjs.com/xlsx-0.20.2/package/dist/xlsx.mini.min.js");

    const urlParamsRegex = /\/comisii\/([^\/?]+)\/proba\/([^\/?]+)$/;
    const urlParams = urlParamsRegex.exec(window.location.pathname);
    const centerId = urlParams[1];
    const probaId = urlParams[2];

    const token = localStorage.getItem('token').replace(/"/g, '');
    const headers = {
        'Credentials': 'include',
        'Content-Type': 'application/json;charset=utf-8',
        'Authorization': `Bearer ${token}`,
    }
    const maxRecords = 30000;

    const probeUrl = `https://ev.edu.ro/api/v1/exam-mgmt/commission-operator/commissions/${centerId}/events`;
    const probeResponse = await fetch(probeUrl, { headers });
    const probeList = await probeResponse.json();

    const probaIds = new Map();
    for (const proba of probeList) {
        probaIds.set(proba._id, probaIds.size + 1);
    }

    const saliUrl = (eventId) => `https://ev.edu.ro/api/v1/exam-mgmt/commission-operator/commissions/${centerId}/events/${eventId}/files-check`;
    const eleviUrl = `https://ev.edu.ro/api/v1/exam-mgmt/commission-operator/commissions/${centerId}/events/${probaId}/students?filter={"limit":${maxRecords},"skip":0,"order":"fullName asc","personalNumber":"","classroomId":[]}`;
    const csvHeaders = ["Centru Examen", "Scoala provenienta", "Clasa provenienta", "CNP", "Nume complet", "Cod candidat"];

    const extraHeaders = [];
    for (let i = 0; i < probaIds.size; i++) {
        extraHeaders.push(`Disciplina${i + 1}`);
        extraHeaders.push(`Absent${i + 1}`);
        extraHeaders.push(`Eliminat${i + 1}`);
        extraHeaders.push(`Limba${i + 1}`);
        extraHeaders.push(`FisierÎncărcat${i + 1}`);
        extraHeaders.push(`IDClasă${i + 1}`);
    }

    const csvValues = ["center.name", "student.originSchool", "student.grade", "student.personalNumber", "student.fullName", "siiirCode"];
    const csvData = [];
    csvData.push(csvHeaders.concat(extraHeaders));

    const getSaliMapForProba = async (eventId) => {
        const saliResponse = await fetch(saliUrl(eventId), { headers });
        const sali = await saliResponse.json();
        const saliMap = new Map();
        for (const sal of sali) {
            saliMap.set(sal._id.classroomId, sal.classroom);
        }
        return saliMap;
    }

    const saliMaps = {};

    const eleviResponse = await fetch(eleviUrl, { headers });
    const elevi = await eleviResponse.json();

    const getDeepValue = (obj, path) => path.split('.').reduce((o, k) => o[k], obj);

    for (const elev of elevi) {
        const values = csvValues.map(valuePath => {
            return getDeepValue(elev, valuePath);
        });
        //parse eventSubscriptions
        let eventResults = [];
        const eventSubscriptions = elev.eventSubscriptions;
        for (const eventSubscription of eventSubscriptions) {
            if (!probaIds.has(eventSubscription.eventId)) {
                probaIds.set(eventSubscription.eventId, probaIds.size + 1);
            }
            const currentId = probaIds.get(eventSubscription.eventId);
            if (!saliMaps[eventSubscription.eventId]) {
                saliMaps[eventSubscription.eventId] = await getSaliMapForProba(eventSubscription.eventId);
            }
            const eventResult = {
                discipline: eventSubscription.discipline,
                isAbsent: eventSubscription.isAbsent,
                isRemoved: eventSubscription.isRemoved,
                language: eventSubscription.language,
                uploadFilename: eventSubscription.uploadFilename,
                classroomId: saliMaps[eventSubscription.eventId].get(eventSubscription.classroomId) || "",

            }
            eventResults[currentId] = eventResult;
        }

        probaIds.size > 0 && probaIds.forEach((mapVal, mapIndex) => {
            const index = parseInt(mapVal);
            const eventResult = eventResults[index];
            if (!eventResult) {
                values.push("", "", "", "", "", "");
            } else {
                values.push(
                    eventResult.discipline,
                    eventResult.isAbsent ? "Absent" : "",
                    eventResult.isRemoved ? "Eliminat" : "",
                    eventResult.language,
                    eventResult.uploadFilename,
                    eventResult.classroomId
                );
            }
        });
        
        csvData.push(values);
    }

    //E)a)
    const SecRomLbMinEa = [];
    const SecMinLbRomEa = [];
    const SecRomLbMinEc = [];
    const SecMinLbRomEc = [];
    const SecRomLbMinEd = [];
    const SecMinLbRomEd = [];
    const SecMinLbRomEb = [];
    SecRomLbMinEa.push(csvHeaders.concat(extraHeaders));
    SecMinLbRomEa.push(csvHeaders.concat(extraHeaders));
    SecRomLbMinEc.push(csvHeaders.concat(extraHeaders));
    SecMinLbRomEc.push(csvHeaders.concat(extraHeaders));
    SecRomLbMinEd.push(csvHeaders.concat(extraHeaders));
    SecMinLbRomEd.push(csvHeaders.concat(extraHeaders));
    SecMinLbRomEb.push(csvHeaders.concat(extraHeaders));
    for (const elev of csvData) {
        //if elev has FisierÎncărcat1 and Disciplina4 is empty and Limba1 is not empty
        if (elev[10] && elev[24].length === 0 && elev[9]) {
            SecRomLbMinEa.push(elev);
        }
        //if elev has FisierÎncărcat1 and Disciplina4 is not empty and Limba1 is empty
        if (elev[10] && elev[24].length > 0 && elev[9] === undefined) {
            SecMinLbRomEa.push(elev);
        }
        //if elev has FisierÎncărcat2 and Disciplina4 is empty and Limba2 is not empty
        if (elev[16] && elev[24].length === 0 && elev[15]) {
            SecRomLbMinEc.push(elev);
        }
        //if elev has FisierÎncărcat2 and Disciplina4 is not empty and Limba2 is empty
        if (elev[16] && elev[24].length > 0 && elev[15] === undefined) {
            SecMinLbRomEc.push(elev);
        }
        //if elev has FisierÎncărcat3 and Disciplina4 is empty and Limba3 is not empty
        if (elev[22] && elev[24].length === 0 && elev[21]) {
            SecRomLbMinEd.push(elev);
        }
        //if elev has FisierÎncărcat3 and Disciplina4 is not empty and Limba3 is empty
        if (elev[22] && elev[24].length > 0 && elev[21] === undefined) {
            SecMinLbRomEd.push(elev);
        }
        //if elev has FisierÎncărcat4 and Disciplina4 is not empty and Limba4 is empty
        if (elev[28] && elev[24].length > 0 && elev[27] === undefined) {
            SecMinLbRomEb.push(elev);
        }

    }

    const legend = [
        ["EXPORT", "Export date la proba selectată. Atenție apar doar elevii care trebuie să participe la proba selectată."],
        ["SecRomLbMinEa", "Secția Română cu lucrare încărcată în limba Minorității la proba Ea"],
        ["SecMinLbRomEa", "Secția Minorităților cu lucrare încărcată în limba Română la proba Ea"],
        ["SecRomLbMinEc", "Secția Română cu lucrare încărcată în limba Minorității la proba Ec"],
        ["SecMinLbRomEc", "Secția Minorităților cu lucrare încărcată în limba Română la proba Ec"],
        ["SecRomLbMinEd", "Secția Română cu lucrare încărcată în limba Minorității la proba Ed"],
        ["SecMinLbRomEd", "Secția Minorităților cu lucrare încărcată în limba Română la proba Ed"],
        ["SecMinLbRomEb", "Secția Minorităților cu lucrare încărcată în limba Română la proba Eb"],
    ];
    let legendSheet = XLSX.utils.aoa_to_sheet(legend);
    let worksheet = XLSX.utils.aoa_to_sheet(csvData);
    let wsSecRomLbMinEa = XLSX.utils.aoa_to_sheet(SecRomLbMinEa);
    let wsSecMinLbRomEa = XLSX.utils.aoa_to_sheet(SecMinLbRomEa);
    let wsSecRomLbMinEc = XLSX.utils.aoa_to_sheet(SecRomLbMinEc);
    let wsSecMinLbRomEc = XLSX.utils.aoa_to_sheet(SecMinLbRomEc);
    let wsSecRomLbMinEd = XLSX.utils.aoa_to_sheet(SecRomLbMinEd);
    let wsSecMinLbRomEd = XLSX.utils.aoa_to_sheet(SecMinLbRomEd);
    let wsSecMinLbRomEb = XLSX.utils.aoa_to_sheet(SecMinLbRomEb);
    let workbook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workbook, legendSheet, "Legendă");
    XLSX.utils.book_append_sheet(workbook, worksheet, "EXPORT");
    XLSX.utils.book_append_sheet(workbook, wsSecRomLbMinEa, "SecRomLbMinEa");
    XLSX.utils.book_append_sheet(workbook, wsSecMinLbRomEa, "SecMinLbRomEa");
    XLSX.utils.book_append_sheet(workbook, wsSecRomLbMinEc, "SecRomLbMinEc");
    XLSX.utils.book_append_sheet(workbook, wsSecMinLbRomEc, "SecMinLbRomEc");
    XLSX.utils.book_append_sheet(workbook, wsSecRomLbMinEd, "SecRomLbMinEd");
    XLSX.utils.book_append_sheet(workbook, wsSecMinLbRomEd, "SecMinLbRomEd");
    XLSX.utils.book_append_sheet(workbook, wsSecMinLbRomEb, "SecMinLbRomEb");
    XLSX.writeFile(workbook, `export_ev${Date.now()}.xlsx`);
})()
