(async () => {
    //Se rulează cu cont de ISJ pe pagina Evaluatori din pagina principală al comisiei
    const token = localStorage.getItem('token').replace(/"/g, '');

    const urlParamsRegex = /\/comisii\/([^\/]+)\//;
    const urlParams = urlParamsRegex.exec(window.location.pathname);
    const commissionId = urlParams[1];
    const headers = {
        'Credentials': 'include',
        'Content-Type': 'application/json;charset=utf-8',
        'Authorization': `Bearer ${token}`,
    }
    //https://ev.edu.ro/api/v1/exam-mgmt/commission-operator/commissions/65b8db857c4eea0dffe87217/evaluators?filter={%22limit%22:25,%22skip%22:0,%22order%22:%22fullName%20asc%22,%22where%22:{%22and%22:[{%22or%22:[{%22familyName%22:{%22like%22:%22.*.*%22,%22options%22:%22i%22}},{%22givenName%22:{%22like%22:%22.*.*%22,%22options%22:%22i%22}}]}]}}
    const listUrl = `https://ev.edu.ro/api/v1/exam-mgmt/commission-operator/commissions/${commissionId}/evaluators?filter={"limit":1000000,"skip":0,"order":"fullName asc","where":{"and":[{"or":[{"familyName":{"like":".*.*","options":"i"}},{"givenName":{"like":".*.*","options":"i"}}]},{}]}}`;
    const listResponse = await fetch(listUrl, { headers });
    const items = await listResponse.json();

    for (const item of items) {
        const evaluatorId = item.id;
        //https://ev.edu.ro/api/v1/exam-mgmt/commission-operator/commissions/65b8db857c4eea0dffe87217/evaluators/64d606af155602d844231b6c
        const deleteUrl = `https://ev.edu.ro/api/v1/exam-mgmt/commission-operator/commissions/${commissionId}/evaluators/${evaluatorId}`;
        await fetch(deleteUrl, { method: 'DELETE', headers });
        console.log(`Evaluatorul ${item.familyName} ${item.givenName} a fost șters`);
    }
})()

