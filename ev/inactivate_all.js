(async () => {
    //Se rulează cu cont de ISJ pe pagina Evaluatori din interiorul Centrului de Evaluare
    const token = localStorage.getItem('token').replace(/"/g, '');

    const urlParamsRegex = /\/comisii\/([^\/]+)\/centru-evaluare\/([^\/?]+)/;
    const urlParams = urlParamsRegex.exec(window.location.pathname);
    const commissionId = urlParams[1];
    const centerId = urlParams[2];
    const headers = {
        'Credentials': 'include',
        'Content-Type': 'application/json;charset=utf-8',
        'Authorization': `Bearer ${token}`,
    }
    const listUrl = `https://ev.edu.ro/api/v1/exam-mgmt/commission-operator/commissions/${commissionId}/evaluation-centers/${centerId}/evaluators?filter={"limit":1000000,"skip":0,"order":"fullName asc","where":{"and":[{"or":[{"familyName":{"like":".*.*","options":"i"}},{"givenName":{"like":".*.*","options":"i"}}]},{}]}}`;
    const listResponse = await fetch(listUrl, { headers });
    const items = await listResponse.json();

    for (const item of items) {
        if (item.backup === true) {
            //console.log(`Evaluatorul ${item.familyName} ${item.givenName} este deja inactiv`);
            continue;
        }
        const evaluatorId = item.id;
        const replaceUrl = `https://ev.edu.ro/api/v1/exam-mgmt/commission-operator/commissions/${commissionId}/evaluation-centers/${centerId}/evaluators/${evaluatorId}/replace`;
        await fetch(replaceUrl, { method: 'PUT', headers });
        console.log(`Evaluatorul ${item.familyName} ${item.givenName} a fost inactivat`);
    }
})()

