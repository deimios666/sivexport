(async () => {
    //Se rulează pe pagina Asistenți în interiorul Comisiei
    const token = localStorage.getItem('token').replace(/"/g, '');
    const urlParamsRegex = /\/comisii\/([^\/]+)\//;
    const urlParams = urlParamsRegex.exec(window.location.pathname);
    const comisiiId = urlParams[1];

    const headers = {
        'Credentials': 'include',
        'Content-Type': 'application/json;charset=utf-8',
        'Authorization': `Bearer ${token}`,
    }
    const maxRecords = 30000;
    const asistentUrl = `https://ev.edu.ro/api/v1/exam-mgmt/commission-operator/commissions/${comisiiId}/assistants?filter={%22limit%22:${maxRecords},%22skip%22:0,%22order%22:%22familyName%20asc%22,%22where%22:{%22and%22:[{%22or%22:[{%22familyName%22:{%22like%22:%22.*.*%22,%22options%22:%22i%22}},{%22givenName%22:{%22like%22:%22.*.*%22,%22options%22:%22i%22}}]}]}}`;
    const csvData = [];
    const asistentResponse = await fetch(asistentUrl, { headers });
    const asistenti = await asistentResponse.json();

    const csvHeaders = ["Centru Examen", "Nume", "Prenume", "Telefon", "Email", "Status"];
    const csvValues = ["center.name", "user.familyName", "user.givenName", "user.phoneNumber", "user.email", "user.state"];
    const getDeepValue = (obj, path) => path.split('.').reduce((o, k) => o[k], obj);
    const escapeCsvValue = (value) => `"${(value + "").split('"').join('""')}"`

    for (const asistent of asistenti) {
        const values = csvValues.map(valuePath => {
            try {
                return escapeCsvValue(getDeepValue(asistent, valuePath));
            } catch (error) {
                return "NULL";
            }
        });
        csvData.push(values.join(','));
    }
    //generate CSV
    const fileName = "export_assitenti.csv";

    const buffer = "\uFEFF" + csvHeaders.join(',') + "\n" + csvData.join("\n");
    const blob = new Blob([buffer], {
        type: "text/csv;charset=utf8;",
    });
    const url = window.URL.createObjectURL(blob);
    const a = document.createElement("a");
    a.href = url;
    a.download = fileName;
    a.click();
})()