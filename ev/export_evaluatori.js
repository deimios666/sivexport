(async () => {
    //Se rulează cu cont de ISJ pe pagina Evaluatori din interiorul Centrului de Evaluare
    const token = localStorage.getItem('token').replace(/"/g, '');

    const urlParamsRegex = /\/comisii\/([^\/]+)\/centru-evaluare\/([^\/?]+)/;
    const urlParams = urlParamsRegex.exec(window.location.pathname);
    const commissionId = urlParams[1];
    const centerId = urlParams[2];
    const headers = {
        'Credentials': 'include',
        'Content-Type': 'application/json;charset=utf-8',
        'Authorization': `Bearer ${token}`,
    }
    const maxRecords = 30000;
    const listUrl = `https://ev.edu.ro/api/v1/exam-mgmt/commission-operator/commissions/${commissionId}/evaluation-centers/${centerId}/evaluators?filter={"limit":${maxRecords},"skip":0,"order":"familyName asc","where":{}}`;
    const csvData = [];
    const csvHeaders = [
        "ID",
        "Email",
        "Nume",
        "Prenume",
        "Telefon",
        "Inactiv",
        "Liceal",
        "Prezent",
        "Discipline",
        "Limbi",
        "Lucrări Corectate",
        "Lucrări Total",
        "Lucrări Inițiale Corectate",
        "Lucrări Inițiale Total",
        "Contestații Corectate",
        "Contestații Total",
        "Lucrări Super Corectate",
        "Lucrări Super Total"
    ];
    const csvValues = [
        "id",
        "email",
        "familyName",
        "givenName",
        "phoneNumber",
        "backup",
        "highschool",
        "present",
        "disciplines",
        "languages",
        "reviews.done",
        "reviews.total",
        "initialReviews.done",
        "initialReviews.total",
        "appeals.done",
        "appeals.total",
        "superReviews.done",
        "superReviews.total"
    ];
    const listResponse = await fetch(listUrl, { headers });
    const items = await listResponse.json();

    const getDeepValue = (obj, path) => path.split('.').reduce((o, k) => o[k], obj);
    const escapeCsvValue = (value) => `"${(value + "").split('"').join('""')}"`;

    for (let i = 0; i < items.length; i++) {
        const item = items[i];
        const values = csvValues.map(valuePath => {
            const value = getDeepValue(item, valuePath);
            if (value instanceof Array) {
                return value.join('; ');
            }
            return value;
        });
        csvData.push(values.map(escapeCsvValue).join(','));
    }

    //generate CSV
    const fileName = "export_evaluatori.csv";
    const buffer = "\uFEFF" + csvHeaders.join(',') + "\n" + csvData.join("\n");
    const blob = new Blob([buffer], {
        type: "text/csv;charset=utf8;",
    });
    const url = window.URL.createObjectURL(blob);
    const a = document.createElement("a");
    a.href = url;
    a.download = fileName;
    a.click();

})()

