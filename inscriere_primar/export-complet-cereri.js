//IIFE for async
(async() => {
    const listUrl = "https://www.siiir.edu.ro/siiir/list/registrations.json";
    const pageLimit = 500;
    const listParamsPrefix = `generatorKey=REQG&filter=%5B5D&sort=%5B%7B%22property%22%3A%22desiredSchool.county.code%22%2C%22direction%22%3A%22ASC%22%7D%2C%7B%22property%22%3A%22studentRegistration.lastName%22%2C%22direction%22%3A%22ASC%22%7D%2C%7B%22property%22%3A%22studentRegistration.firstName%22%2C%22direction%22%3A%22ASC%22%7D%2C%7B%22property%22%3A%22studentRegistration.firstName1%22%2C%22direction%22%3A%22ASC%22%7D%2C%7B%22property%22%3A%22studentRegistration.firstName2%22%2C%22direction%22%3A%22ASC%22%7D%5D&filterName=%5B%5D&requestParams=%7B%7D`;
    const headers = [
        ["An inscriere", "desiredSchool.schoolYear.code"],
        ["Judet inscriere", "desiredSchool.county.code"],
        ["Loc. inscriere", "desiredSchool.locality.description"],
        ["SIIIR inscriere", "desiredSchool.code"],
        ["Unitate inscriere", "desiredSchool.longName"],
        ["ID Plan UI", "desiredSchoolPlan.id"],
        ["Loc. plan UI", "desiredSchoolPlan.schoolForPlan.locality.description"],
        ["SIIIR plan UI", "desiredSchoolPlan.schoolForPlan.code"],
        ["Unit. plan UI", "desiredSchoolPlan.schoolForPlan.longName"],
        ["Statut UI", "desiredSchoolPlan.schoolForPlan.statut.code"],
        ["Forma finantare plan UI", "desiredSchoolPlan.schoolForPlan.fundingForm.code"],
        ["Tip invatamant UI", "desiredSchoolPlan.educationType.description"],
        ["Alternativa educationala UI", "desiredSchoolPlan.teachingMode.description"],
        ["Tip predare UI", "desiredSchoolPlan.teachingType.description"],
        ["Forma invatamant UI", "desiredSchoolPlan.educationForm.description"],
        ["Limba predare UI", "desiredSchoolPlan.teachingLanguage.description"],
        ["CNP copil", "studentRegistration.nin"],
        ["Nume copil", "studentRegistration.lastName"],
        ["Prenume copil", "studentRegistration.firstName"],
        ["Prenume2 copil", "studentRegistration.firstName1"],
        ["Prenume3 copil", "studentRegistration.firstName2"],
        ["Faza", "registrationPhase"],
        ["Cerere valida", "registrationValidity.valid"],
        ["Cerere admisa", "registered"]
    ];

    //restore console
    let iFrame = document.createElement('iframe');
    iFrame.style.display = 'none';
    document.body.appendChild(iFrame);
    window.console = iFrame.contentWindow.console;

    const csvData = [];

    //get nr of results
    const primingListResult = await fetch(listUrl + "?_dc=" + new Date().getTime(), {
        method: "post",
        credentials: "same-origin",
        headers: {
            Accept: "application/json",
            "Content-type": "application/x-www-form-urlencoded",
        },
        body: listParamsPrefix + "&limit=1&page=1&start=0",
    });

    const primingListJSON = await primingListResult.json();

    const maxItems = primingListJSON.page.total;
    const maxPages = Math.ceil(maxItems / pageLimit); //calculate nr of pages

    for (let page = 1; page <= maxPages; page++) {

        const start = (page - 1) * pageLimit;

        console.log(`Downloading page ${page} of ${maxPages}`);

        const listResult = await fetch(listUrl + "?_dc=" + new Date().getTime(), {
            method: "post",
            credentials: "same-origin",
            headers: {
                Accept: "application/json",
                "Content-type": "application/x-www-form-urlencoded",
            },
            body: listParamsPrefix + `&limit=${pageLimit}&page=${page}&start=${start}`,
        });

        const listJSON = await listResult.json();

        const pageCSVData = await Promise.all(
            listJSON.page.content.map(
                async(item, index) => {
                    return headers.map(key => {
                        const keyPath = key[1];
                        try {
                            const value = keyPath.split('.')
                                .reduce((o, i) => o[i], item) //resolve value
                                .toString()
                                .split('"').join('""'); //escape quotes
                            return `"${value}"`; //return the formatted field
                        } catch (err) {
                            return `""`;
                        }
                    }).join(',');
                }
            )
        );
        csvData.push(pageCSVData.join("\n"));
    }

    const dateString = (new Date()).toISOString().substring(0,10);
    const fileName = `export_inscriși_c0_${dateString}.csv`;
    const buffer = "\uFEFF" + headers.map(h => `"${h[0]}"`).join(",") + "\n" + csvData.join("\n");
    const blob = new Blob([buffer], {
        type: "text/csv;charset=utf8;",
    });

    const url = window.URL.createObjectURL(blob);
    const a = document.createElement("a");
    a.href = url;
    a.download = fileName;
    a.click();
})();