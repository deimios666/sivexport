(async () => {

  const cnps = [
    "1234567890123",
  ];

  const listUrl = "/siiir/list/registrations.json";

  const pageLimit = 1000;

  const phases = ["ONE", "TWO", "THREE", "FOUR", "FIVE"];
  const phase = phases[0];

  const primingListParams = `generatorKey=REQG&filter=%5B%7B%22property%22%3A%22registrationValidity.valid%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5Btrue%5D%7D%2C%7B%22property%22%3A%22registered%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5Bfalse%5D%7D%2C%7B%22property%22%3A%22registrationPhase%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B%22${phase}%22%5D%7D%5D&filterName=%5B%5D&requestParams=%7B%7D&sort=%5B%7B%22property%22%3A%22desiredSchool.longName%22%2C%22direction%22%3A%22ASC%22%7D%5D&page=1&start=0&limit=1`;
  const listParamsTemplate = `generatorKey=REQG&filter=%5B%7B%22property%22%3A%22registrationValidity.valid%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5Btrue%5D%7D%2C%7B%22property%22%3A%22registered%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5Bfalse%5D%7D%2C%7B%22property%22%3A%22registrationPhase%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B%22${phase}%22%5D%7D%5D&filterName=%5B%5D&requestParams=%7B%7D&sort=%5B%7B%22property%22%3A%22desiredSchool.longName%22%2C%22direction%22%3A%22ASC%22%7D%5D&limit=${pageLimit}`;
  const actionUrl = "/siiir/management/enroll.json";

  const actionParamsPrefix = "idRegistration=";
  const actionParamsSuffix = "&registered=2";

  const primigResult = await fetch(listUrl + "?_dc=" + new Date().getTime(), {
    method: "post",
    credentials: "same-origin",
    headers: {
      Accept: "application/json",
      "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
    },
    body: primingListParams,
  });

  const primingJSON = await primigResult.json();
  const maxRecords = primingJSON.page.total;

  const maxPages = Math.ceil(maxRecords / pageLimit);

  for (let page = 1; page <= maxPages; page++) {

    const start = (page - 1) * pageLimit;

    const listParams = `${listParamsTemplate}&page=${page}&start=${start}`;

    const listResult = await fetch(listUrl + "?_dc=" + new Date().getTime(), {
      method: "post",
      credentials: "same-origin",
      headers: {
        Accept: "application/json",
        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
      },
      body: listParams,
    });

    const listJSON = await listResult.json();

    for (const elev of listJSON.page.content) {
      //if on the list we set to admis

      const cnp = elev.studentRegistration.nin;

      if (cnps.includes(cnp)) {
        await fetch(actionUrl + "?_dc=" + new Date().getTime(), {
          method: "post",
          credentials: "same-origin",
          headers: {
            Accept: "application/json",
            "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
          },
          body: actionParamsPrefix + elev.id + actionParamsSuffix,
        });
      }
    }
  }


})();