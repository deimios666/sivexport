(async () => {
  const pageSize = 100;

  const schoolYearId = AMS.util.AppConfig.selectedYear.data.id;
  const listUrl = `https://www.siiir.edu.ro/siiir/list/userAem.json`
  const listParam = `generatorKey=UQG&filter=%5B%7B%22property%22%3A%22userLevel%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B%22SCHOOL%22%5D%7D%2C%7B%22property%22%3A%22active%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5Btrue%5D%7D%5D&filterName=%5B%5D&requestParams=%7B%7D`;
  const getUrl = id => `https://www.siiir.edu.ro/siiir/management/userAem/${id}?schoolYearId=${schoolYearId}&id=${id}`;

  const siiirFetch = async (url, body) => fetch(`${url}?_dc=${new Date().getTime()}`, {
    method: "post",
    credentials: "same-origin",
    headers: {
      Accept: "application/json",
      "Content-type": "application/x-www-form-urlencoded",
    },
    body: body,
  });

  const siiirGetWithURLFunctionId = async (urlFunction, id) => fetch(`${urlFunction(id)}&_dc=${new Date().getTime()}`, {
    method: "get",
    credentials: "same-origin",
    headers: {
      Accept: "application/json",
      "Content-type": "application/json",
    }
  });

  //restore console
  let iFrame = document.createElement('iframe');
  iFrame.style.display = 'none';
  document.body.appendChild(iFrame);
  window.console = iFrame.contentWindow.console;

  //priming fetch
  const primingListResult = await siiirFetch(listUrl, listParam + "&limit=1&page=1&start=0");
  const primingListJSON = await primingListResult.json();
  const maxItems = primingListJSON.page.total;

  //calculate nr of pages
  const maxPages = Math.ceil(maxItems / pageSize);

  const csvData = [];

  //for each page
  for (let pageNr = 1; pageNr <= maxPages; pageNr++) {
    console.log(`==Processing page ${pageNr} of ${maxPages}==`);
    const listResult = await siiirFetch(listUrl, listParam + `&limit=${pageSize}&page=${pageNr}&start=${(pageNr - 1) * pageSize}`);
    const listJSON = await listResult.json();
    //for each result on page
    for (const item of listJSON.page.content) {
      //get
      const getResult = await siiirGetWithURLFunctionId(getUrl, item.id);
      const getJSON = await getResult.json();
      const payload = getJSON.baseEntity;

      //add to csv
      const roles = payload.roles.map(x => x.roleName).join(";");
      csvData.push(`${payload.id},${payload.username},"${payload.school.longName.split('"').join('""')}",${roles}`);
      console.log(`${item.school.longName}/${item.username}`);
    }
  }

  const fileName = "export_utilizatori.csv";
  const buffer = "\uFEFF" + "ID,numeUtilizator,Unitatea,Roluri" + "\n" + csvData.join("\n");
  const blob = new Blob([buffer], {
    type: "text/csv;charset=utf8;",
  });
  const url = window.URL.createObjectURL(blob);
  const a = document.createElement("a");
  a.href = url;
  a.download = fileName;
  a.click();

  console.log("==DONE==");
})();