(async () => {

  const pageSize = 100;

  const schoolYearId = AMS.util.AppConfig.selectedYear.data.id;
  const listUrl = `https://www.siiir.edu.ro/siiir/list/userAem.json`
  const listParam = `generatorKey=UQG&filter=%5B%7B%22property%22%3A%22userLevel%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B%22SCHOOL%22%5D%7D%2C%7B%22property%22%3A%22active%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5Btrue%5D%7D%5D&filterName=%5B%5D&requestParams=%7B%7D`;
  const getUrl = id => `https://www.siiir.edu.ro/siiir/management/userAem/${id}?schoolYearId=${schoolYearId}&id=${id}`;
  const putUrl = id => `https://www.siiir.edu.ro/siiir/management/userAem/${id}?schoolYearId=${schoolYearId}`;


  const siiirFetch = async (url, body) => fetch(`${url}?_dc=${new Date().getTime()}`, {
    method: "post",
    credentials: "same-origin",
    headers: {
      Accept: "application/json",
      "Content-type": "application/x-www-form-urlencoded",
    },
    body: body,
  });

  const siiirGetWithURLFunctionId = async (urlFunction, id) => fetch(`${urlFunction(id)}&_dc=${new Date().getTime()}`, {
    method: "get",
    credentials: "same-origin",
    headers: {
      Accept: "application/json",
      "Content-type": "application/json",
    }
  });

  const siiirPutWithURLFunctionId = async (urlFunction, body, id) => fetch(`${urlFunction(id)}&_dc=${new Date().getTime()}`, {
    method: "put",
    credentials: "same-origin",
    headers: {
      Accept: "application/json",
      "Content-type": "application/json",
    },
    body: JSON.stringify(body),
  });

  //restore console
  let iFrame = document.createElement('iframe');
  iFrame.style.display = 'none';
  document.body.appendChild(iFrame);
  window.console = iFrame.contentWindow.console;

  //map
  // const pnrasMap = new Map();
  // pnras.forEach(item => pnrasMap.set(item, true));

  //priming fetch
  const primingListResult = await siiirFetch(listUrl, listParam + "&limit=1&page=1&start=0");
  const primingListJSON = await primingListResult.json();
  const maxItems = primingListJSON.page.total;

  //calculate nr of pages
  const maxPages = Math.ceil(maxItems / pageSize);

  //for each page
  for (let pageNr = 1; pageNr <= maxPages; pageNr++) {
    console.log(`==Processing page ${pageNr} of ${maxPages}==`);
    const listResult = await siiirFetch(listUrl, listParam + `&limit=${pageSize}&page=${pageNr}&start=${(pageNr - 1) * pageSize}`);
    const listJSON = await listResult.json();
    //for each result on page
    for (const item of listJSON.page.content) {
        //get
        const getResult = await siiirGetWithURLFunctionId(getUrl, item.id);
        const getJSON = await getResult.json();
        const payload = getJSON.baseEntity;
        const findMATE = payload.roles.filter(x => x.id === 401);
        const findInfo = payload.roles.filter(x => x.id === 10);
        const findSecretar = payload.roles.filter(x => x.id === 9);
        if (findMATE.length === 0) payload.roles.push({
          "id": 401,
          "roleName": "Modul MATE Responsabil școală",
          "level": "SCHOOL"
        });

        if (findInfo.length === 0) payload.roles.push({
          "id": 10,
          "roleName": "Informatician",
          "level": "SCHOOL"
        });

        if (findSecretar.length === 0) payload.roles.push({
          "id": 9,
          "roleName": "Secretar",
          "level": "SCHOOL"
        });

        if (findMATE.length === 0 || findInfo.length === 0 || findSecretar.length === 0) {
          //put
          await siiirPutWithURLFunctionId(putUrl, payload, item.id);
        }
        console.log(`${item.school.longName}/${item.username}`);
    }
  }
  console.log("==DONE==");
})();