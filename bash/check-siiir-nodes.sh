#!/bin/bash
SIIIRUSERNAME="****"
SIIIRPASSWORD="****"
SIIIRNODEURL="https://www.siiir.edu.ro/siiir/login.jsp;jsessionid=E582701919A0ABC6827753CC72BBDD80.siiirNode"
SIIIRHEADERS1="Content-Type: application/x-www-form-urlencoded; charset=UTF-8"
SIIIRHEADERS2="Accept: application/json"
SIIIRHEADERS3="X-Requested-With: XMLHttpRequest"

SIIIRAUTHURL="https://www.siiir.edu.ro/siiir/j_spring_security_check"
SIIIRAUTHPOSTBODY="j_username=$SIIIRUSERNAME&j_password=$SIIIRPASSWORD"
SIIIROKRESPONSE="{success:true}"
SIIIRNODES="1 2 3 4 5 6 7"

echo "Last check:  `date --rfc-3339=seconds`"

for nodeID in $SIIIRNODES; do
        rm "cookies$nodeID.txt" 2> /dev/null
        echo -n "Node $nodeID"
        curl -s -m 10 -c "cookies$nodeID.txt" -o /dev/null "$SIIIRNODEURL$nodeID"
        STATUS=$?
        if [ "$STATUS" == "0" ]; then
                RESP=`curl -H "$SIIIRHEADERS1" -H "$SIIIRHEADERS2" -H "$SIIIRHEADERS3" -X POST -d "$SIIIRAUTHPOSTBODY" -s -m 10 -c "cookies$nodeID.txt" "$SIIIRAUTHURL"`
                if [ "$RESP" == "$SIIIROKRESPONSE" ]; then
                        echo " OK"
                else
                        echo " FAIL"
                fi
        else
                echo " FAIL"
        fi
done
