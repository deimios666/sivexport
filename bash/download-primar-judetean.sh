#!/bin/bash
SIIIRUSERNAME="bszilard@isj.educv.ro"
SIIIRPASSWORD="***"
SIIIRNODEURL="https://www.siiir.edu.ro/siiir/login.jsp"
SIIIRHEADERS1="Content-Type: application/x-www-form-urlencoded; charset=UTF-8"
SIIIRHEADERS2="Accept: application/json"
SIIIRHEADERS3="X-Requested-With: XMLHttpRequest"
SIIIRHEADERS4="Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8"

SIIIRAUTHURL="https://www.siiir.edu.ro/siiir/j_spring_security_check"
SIIIRAUTHPOSTBODY="j_username=$SIIIRUSERNAME&j_password=$SIIIRPASSWORD"
SIIIROKRESPONSE="{success:true}"

COUNTYID="16"
SIIIREXPORTURL="https://www.siiir.edu.ro/siiir/excel/624.jasper"
SIIIREXPORTPOSTBODY="type=xls&source=InscrieriPeJudet&countyId=$COUNTYID"

TIMESTAMP=`date "+%Y-%m-%d-%H.%M.%S"`
FILENAME="/tmp/inscrieri_primar_$TIMESTAMP.xlsx"
COOKIES="/tmp/cookies-$TIMESTAMP.txt"
rm "$COOKIES" 2> /dev/null
#get cookie
curl -s -c "$COOKIES" -o /dev/null "$SIIIRNODEURL"
STATUS=$?
if [ "$STATUS" == "0" ]; then
        #login
        RESP=`curl -H "$SIIIRHEADERS1" -H "$SIIIRHEADERS2" -H "$SIIIRHEADERS3" -X POST -d "$SIIIRAUTHPOSTBODY" -s -c "$COOKIES" "$SIIIRAUTHURL"`
        if [ "$RESP" == "$SIIIROKRESPONSE" ]; then
                #download exports
                curl -s -H "$SIIIRHEADERS1" -H "$SIIIRHEADERS4" -H "$SIIIRHEADERS3" -X POST -d "$SIIIREXPORTPOSTBODY" -b "$COOKIES" -o "$FILENAME" "$SIIIREXPORTURL"
                #send mail
                echo -e "Atașat aveți situația la $TIMESTAMP\n" | mutt -a "$FILENAME" -s "Inscrieri Pregatitoare $TIMESTAMP" -c banciumihai@isj.educv.ro -c bszilard@isj.educv.ro -- k_imre@isj.educv.ro
                #remove exports
                rm "$FILENAME" 2> /dev/null
                #echo "DONE"
        else
                echo " FAIL"
                echo "$RESP"
        fi
else
        echo " FAIL"
fi
