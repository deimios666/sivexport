#!/bin/bash
SIIIRUSERNAME="bszilard@isj.educv.ro"
SIIIRPASSWORD="****"
SIIIRNODEURL="https://www.siiir.edu.ro/siiir/login.jsp"
SIIIRHEADERS1="Content-Type: application/x-www-form-urlencoded; charset=UTF-8"
SIIIRHEADERS2="Accept: application/json"
SIIIRHEADERS3="X-Requested-With: XMLHttpRequest"
SIIIRHEADERS4="Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8"

SIIIRAUTHURL="https://www.siiir.edu.ro/siiir/j_spring_security_check"
SIIIRAUTHPOSTBODY="j_username=$SIIIRUSERNAME&j_password=$SIIIRPASSWORD"
SIIIROKRESPONSE="{success:true}"

COUNTYID="16"
SCHOOLYEARID="26"
SCHOOLYEAR="2023-2024"
SIIIREXPORTURL="https://www.siiir.edu.ro/siiir/excel/400.jasper"
SIIIREXPORTPOSTBODY="type=xls&source=StudyFormationStudentManagementSql&schoolYearId=$SCHOOLYEARID&schoolYearCode=$SCHOOLYEAR&schoolYearDescription=Anul+%C8%99colar+$SCHOOLYEAR&applicationVersion=1.26.1&schoolId=-1&locality=&localityId=-1&street=&streetNumber=&postalCode=&phoneNumber=&faxNumber=&schoolName=&countyId=$COUNTYID&internalSchoolId=-1&levelId=-1&propertyFormId=-1&studyFormationTypeId=-1&countyClause=+s2.id_county+%3D+16&localityClause=+1%3D1&internalIdClause=+1%3D1&levelClause=+1%3D1&propertyFormClause=+1%3D1&studyFormationTypeClause=+1%3D1&propertyForm=-1&level=-1&studyFormationType=-1&pagina="

TIMESTAMP=`date "+%Y-%m-%d-%H.%M.%S"`
YEAR=`date "+%Y"`
rm "cookies.txt" 2> /dev/null
#get cookie
curl -s -c "cookies.txt" -o /dev/null "$SIIIRNODEURL"
STATUS=$?
if [ "$STATUS" == "0" ]; then
        #login
        RESP=`curl -H "$SIIIRHEADERS1" -H "$SIIIRHEADERS2" -H "$SIIIRHEADERS3" -X POST -d "$SIIIRAUTHPOSTBODY" -s -c "cookies.txt" "$SIIIRAUTHURL"`
        if [ "$RESP" == "$SIIIROKRESPONSE" ]; then
                #download exports
                for PAGE in 1 2 3 4
                do
                    curl -s -H "$SIIIRHEADERS1" -H "$SIIIRHEADERS4" -H "$SIIIRHEADERS3" -X POST -d "$SIIIREXPORTPOSTBODY$PAGE" -b "cookies.txt" -o "export_elevi_$SCHOOLYEAR-p$PAGE.xls" "$SIIIREXPORTURL"
                done
                #pack exports
                7z a -t7z -mx9 -y -bsp0 -bso0 "export_elevi_$SCHOOLYEAR-$TIMESTAMP.7z" "export_elevi_$SCHOOLYEAR-p*.xls"
                #remove exports
                rm *.xls 2> /dev/null
                #post file with smbclient
                smbclient //192.168.10.251/deimios -U "deimios%****" -c "cd $YEAR/siiir ; put ""export_elevi_$SCHOOLYEAR-$TIMESTAMP.7z"""
                rm export_elevi_$SCHOOLYEAR-$TIMESTAMP.7z
                #echo "DONE"
        else
                echo " FAIL"
                echo "$RESP"
        fi
else
        echo " FAIL"
fi
