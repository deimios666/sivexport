(async () => {

  await import('https://unpkg.com/jszip@3.10.1/dist/jszip.min.js');

  const zip = JSZip();

  const limit = 1000;

  const primingListUrl = `https://forexe.mfinante.gov.ro/ForexeSNM/messages/loadAll.do?nomCategoryCode=4&_dc=${new Date().getTime()}&page=1&start=0&limit=1`

  const primingListResult = await fetch(primingListUrl, {
    method: "get",
    credentials: "same-origin"
  });
  const primingListJSON = await primingListResult.json();

  const total = primingListJSON.total;

  const maxPages = Math.ceil(total / limit);

  let counter = 0;

  for (let page = 1; page <= maxPages; page++) {
    console.log(`==PAGE ${page}/${maxPages}==`);
    const offset = (page - 1) * limit;
    const listUrl = `https://forexe.mfinante.gov.ro/ForexeSNM/messages/loadAll.do?nomCategoryCode=4&_dc=${new Date().getTime()}&page=${page}&start=${offset}&limit=${limit}`;

    const listResult = await fetch(listUrl, {
      method: "get",
      credentials: "same-origin"
    });
    const listJSON = await listResult.json();
    for (const row of listJSON.rows) {
      const { id, numeFisier, descriere } = row.mesajTab;
      const fileurl = `https://forexe.mfinante.gov.ro/ForexeSNM/messages/downloadFile.do?id=${id}&fileName=${numeFisier}&nomCategoryCode=4`;
      const descriereSafe = descriere.replaceAll(/[\\/:*?\"<>|]/g, "_");
      const result = await fetch(fileurl);
      const fileblob = await result.blob();
      const fileNameSuffix = `_${counter}_${numeFisier}`;
      const fileNamePrefix = descriereSafe.substring(0, 200 - fileNameSuffix.length);
      zip.file(`${fileNamePrefix}${fileNameSuffix}`, fileblob);
      counter++;
      console.log(`[${counter}/${total}]: ${fileNamePrefix}${fileNameSuffix}`);
    }
  }
  const zipcontent = await zip.generateAsync({ type: "blob" });
  const link = document.createElement("a");
  link.href = URL.createObjectURL(zipcontent);
  link.download = "forexe.zip";
  link.click();
  link.remove();
  console.log("===DONE===");
})()