(async () => {
  const pageSize = 100;

  const schoolYearId = AMS.util.AppConfig.selectedYear.data.id;
  const listUrl = `https://www.siiir.edu.ro/siiir/list/building.json`
  const listParam = `generatorKey=BQG&filter=%5B%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A${schoolYearId}%2C%22county%22%3A16%7D&sort=%5B%7B%22property%22%3A%22from%22%2C%22direction%22%3A%22ASC%22%7D%5D`;
  const getUrl = id => `https://www.siiir.edu.ro/siiir/management/building/${id}?schoolYearId=${schoolYearId}&id=${id}`;
  const putUrl = id => `https://www.siiir.edu.ro/siiir/management/building/${id}?schoolYearId=${schoolYearId}`;


  const siiirFetch = async (url, body) => fetch(`${url}?_dc=${new Date().getTime()}`, {
    method: "post",
    credentials: "same-origin",
    headers: {
      Accept: "application/json",
      "Content-type": "application/x-www-form-urlencoded",
    },
    body: body,
  });

  const siiirGetWithURLFunctionId = async (urlFunction, id) => fetch(`${urlFunction(id)}&_dc=${new Date().getTime()}`, {
    method: "get",
    credentials: "same-origin",
    headers: {
      Accept: "application/json",
      "Content-type": "application/json",
    }
  });

  const siiirPutWithURLFunctionId = async (urlFunction, body, id) => fetch(`${urlFunction(id)}&_dc=${new Date().getTime()}`, {
    method: "put",
    credentials: "same-origin",
    headers: {
      Accept: "application/json",
      "Content-type": "application/json",
    },
    body: JSON.stringify(body),
  });

  //restore console
  let iFrame = document.createElement('iframe');
  iFrame.style.display = 'none';
  document.body.appendChild(iFrame);
  window.console = iFrame.contentWindow.console;

  //priming fetch
  const primingListResult = await siiirFetch(listUrl, listParam + "&limit=1&page=1&start=0");
  const primingListJSON = await primingListResult.json();
  const maxItems = primingListJSON.page.total;

  //calculate nr of pages
  const maxPages = Math.ceil(maxItems / pageSize);

  //for each page
  for (let pageNr = 1; pageNr <= maxPages; pageNr++) {
    console.log(`==Processing page ${pageNr} of ${maxPages}==`);
    const listResult = await siiirFetch(listUrl, listParam + `&limit=${pageSize}&page=${pageNr}&start=${(pageNr - 1) * pageSize}`);
    const listJSON = await listResult.json();
    //for each result on page
    for (const item of listJSON.page.content) {
      if (!item["from"].includes("2023")) {
        //get
        const getResult = await siiirGetWithURLFunctionId(getUrl, item.id);
        const getJSON = await getResult.json();
        const payload = getJSON.baseEntity;
        payload.from = "2023-02-17";
        //put
        await siiirPutWithURLFunctionId(putUrl, payload, item.id);
        console.log(`${item.school.longName} - FIXED`);
      } else {
        console.log(`${item.school.longName} - OK`);
      }
    }
  }
  console.log("==DONE==");
})();