#!/bin/bash
input="shares.csv"
var_username="deimios"
var_password="********"
var_basepath="/scoli/"
#CSV header:
#Path,Username

while read -r line; do
    echo "Line: ${line}"

    var_path=$(echo "${line}" | cut -d"," -f1)
    var_shareusername=$(echo "${line}" | cut -d"," -f2)

    curl -u ${var_username}:${var_password} \
    -X POST \
    -H "Content-Type: application/x-www-form-urlencoded" \
    -H "OCS-APIRequest: true" \
    --data-urlencode "path=${var_basepath}${var_path}" \
    --data-urlencode "shareType=0" \
    --data-urlencode "shareWith=${var_shareusername}" \
    "https://cloud.educv.ro/ocs/v2.php/apps/files_sharing/api/v1/shares"
    
done <"$input"
exit 0
