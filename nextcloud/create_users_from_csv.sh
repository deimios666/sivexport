#!/bin/bash

#needs pwgen installed (apt install pwgen)
input="impfin.csv"
var_apache_user=www-data
var_path_nextcloud=/var/www/nextcloud
var_datum=$(date +"%Y%m%d")
var_result_file="usercreate_${var_datum}.csv"
var_csv_separator=","

#CSV header: Name,Username,Group1,Group2,Group3,Group4,Email,Quota

while read -r line; do
    echo "Line: ${line}"
    var_password=$(pwgen 8 -c -n -N 1)
    set -e
    export OC_PASS=$var_password
    echo "${var_password} ${OC_PASS}"
    var_username=$(echo "${line}" | cut -d"$var_csv_separator" -f2)
    var_name=$(echo "${line}" | cut -d"$var_csv_separator" -f1)
    var_group1=$(echo "${line}" | cut -d"$var_csv_separator" -f3)
    var_group2=$(echo "${line}" | cut -d"$var_csv_separator" -f4)
    var_group3=$(echo "${line}" | cut -d"$var_csv_separator" -f5)
    var_group4=$(echo "${line}" | cut -d"$var_csv_separator" -f6)
    var_email=$(echo "${line}" | cut -d"$var_csv_separator" -f7)
    var_quota=$(echo "${line}" | cut -d"$var_csv_separator" -f8)

    command="php ${var_path_nextcloud}/occ user:add ${var_username} --password-from-env --display-name='${var_name}' "

    if [ "${var_group1}" != "" ]; then
        command="${command} --group='${var_group1}'"
    fi
    if [ "${var_group2}" != "" ]; then
        command="${command} --group='${var_group2}'"
    fi
    if [ "${var_group3}" != "" ]; then
        command="${command} --group='${var_group3}'"
    fi
    if [ "${var_group4}" != "" ]; then
        command="${command} --group='${var_group4}'"
    fi

    su -s /bin/sh ${var_apache_user} -c "${command}"
    su -s /bin/sh ${var_apache_user} -c " php ${var_path_nextcloud}/occ user:setting ${var_username} settings email '${var_email}'"
    su -s /bin/sh ${var_apache_user} -c " php ${var_path_nextcloud}/occ user:setting ${var_username} files quota '${var_quota}'"
    echo "${var_username}${var_csv_separator}${var_password}" >>"${var_result_file}"
done <"$input"
exit 0
