(async () => {
    //adds tichet attribute to last parent of students that have tichet attribute
    const pageSize = 100;
    const schoolYearId = AMS.util.AppConfig.selectedYear.data.id;

    const studyFormationlistUrl = "/siiir/list/studyFormation.json";
    const studyFormationlistParamString = `generatorKey=SFQG&filter=%5B%5D&sort=%5B%7B%22property%22%3A%22studyFormationType.orderBy%22%2C%22direction%22%3A%22ASC%22%7D%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A${schoolYearId}%7D`;

    const studentListUrl = "/siiir/list/asocStudent.json";
    const studentListParam = studyFormationId => `generatorKey=asocStudent_GRID_GENERATOR_KEY&filter=%5B%7B%22property%22%3A%22studyFormation.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B${studyFormationId}%5D%7D%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%7D`;

    const studentAttributesUrl = "/siiir/list/entityAttributeValueStudent.json";
    const studentAttributesParam = (studentId, attributeId) => `generatorKey=entityAttributeValueStudent_GRID_GENERATOR_KEY&filter=%5B%7B%22property%22%3A%22entityAttribute.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B${attributeId}%5D%7D%2C%7B%22property%22%3A%22student.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B${studentId}%5D%7D%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%7D`;

    const parentListUrl = "/siiir/list/studentParentOrTutor.json";
    const parentListParam = studentId => `generatorKey=studentParentOrTutor_GRID_GENERATOR_KEY&filter=%5B%7B%22property%22%3A%22student.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B${studentId}%5D%7D%2C%7B%22property%22%3A%22to%22%2C%22criteria%22%3A%22IS_NULL%22%2C%22parameters%22%3A%5B%5D%7D%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%7D`;

    const parentAttributesUrl = "/siiir/list/entityAttributeValueParent.json";
    const parentAttributesParam = (parentId, attributeId) => `generatorKey=entityAttributeValueParent_GRID_GENERATOR_KEY&filter=%5B%7B%22property%22%3A%22entityAttribute.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B${attributeId}%5D%7D%2C%7B%22property%22%3A%22parent.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B${parentId}%5D%7D%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%7D`;

    const addParentAttributeUrl = "/siiir/management/entityAttributeValueParent";

    const addParentAttributePayload = (parent) => {
        return {
            "id": null,
            "valueDate": null,
            "dateTo": null,
            "numberValue": null,
            "entityAttribute": {
                "entityTypeEmployee": false,
                "entityTypeStudent": false,
                "listOfValues": "DA",
                "properties": {},
                "code": "A001_TITULAR_TICHET",
                "schoolType": null,
                "entityTypeStudyFormation": false,
                "entityTypeSchool": false,
                "id": 1578,
                "isUnique": true,
                "description": "TITULAR SOLICITARE TICHET AJUTOR CF OUG 83/2023",
                "name": "TITULAR SOLICITARE TICHET AJUTOR CF OUG 83/2023",
                "entityAttributeType": "STRING",
                "entityTypeParent": true,
                "lov": null,
                "eligibleEntity": null
            },
            "value": "DA",
            "parent": parent,
            "properties": null,
            "dateFrom": "2023-12-09"
        }
    };

    const deleteParentAttributeUrl = parentAttributeId => `/siiir/management/entityAttributeValueParent/${parentAttributeId}?schoolYearId=${schoolYearId}`;

    //restore console
    let iFrame = document.createElement("iframe");
    iFrame.style.display = "none";
    document.body.appendChild(iFrame);
    window.console = iFrame.contentWindow.console;

    const siiirFetch = async (url, body) => fetch(`${url}?_dc=${new Date().getTime()}`, {
        method: "post",
        credentials: "same-origin",
        headers: {
            Accept: "application/json",
            "Content-type": "application/x-www-form-urlencoded",
        },
        body: body,
    });

    const siiirPost = async (url, body) =>
        fetch(`${url}?_dc=${new Date().getTime()}`, {
            method: "post",
            credentials: "same-origin",
            headers: {
                Accept: "application/json",
                "Content-type": "application/json",
            },
            body: JSON.stringify(body),
        });

    const siiirDelete = async (url, body) =>
        fetch(`${url}`, {
            method: "delete",
            credentials: "same-origin",
            headers: {
                Accept: "application/json",
                "Content-type": "application/json",
            },
            body: JSON.stringify(body),
        });

    const sortParentAssoc = (a, b) => {
        //check if association with parent is closed
        if (a.parentAssoc.to === null && b.parentAssoc.to !== null) {
            return -1;
        }
        if (a.parentAssoc.to !== null && b.parentAssoc.to === null) {
            return 1;
        }
        //check if completion date is 2023-12-09 (automated attribute)
        if (a.attrib.dateFrom !== "2023-12-09" && b.attrib.dateFrom === "2023-12-09") {
            return -1;
        }
        if (a.attrib.dateFrom === "2023-12-09" && b.attrib.dateFrom !== "2023-12-09") {
            return 1;
        }
        //check if parent or tutelă
        if (a.parentAssoc.kinshipType.code === "PAR" && b.parentAssoc.kinshipType.code !== "PAR") {
            return -1;
        }
        if (a.parentAssoc.kinshipType.code !== "PAR" && b.parentAssoc.kinshipType.code === "PAR") {
            return 1;
        }
        return 0;
    }

    //for each studyformation
    const studyFormationPrimingListResponse = await siiirFetch(`${studyFormationlistUrl}`, `${studyFormationlistParamString}&limit=1&start=0&page=1`);
    const studyFormationPrimingListJSON = await studyFormationPrimingListResponse.json();
    const studyFormationMaxResults = studyFormationPrimingListJSON.page.total;

    let currentResult = 0;
    const maxPages = Math.ceil(studyFormationMaxResults / pageSize);

    for (let page = 1; page <= maxPages; page++) {
        const start = (page - 1) * pageSize;
        console.log(`Processing page ${page}/${maxPages}`);
        const studyFormationListResponse = await siiirFetch(`${studyFormationlistUrl}`, `${studyFormationlistParamString}&limit=${pageSize}&start=${start}&page=${page}`);
        const studyFormationListJSON = await studyFormationListResponse.json();
        const studyFormationList = studyFormationListJSON.page.content;
        for (const studyFormation of studyFormationList) {
            currentResult++;
            const studyFormationId = studyFormation.id;
            const studentListResponse = await siiirFetch(`${studentListUrl}`, `${studentListParam(studyFormationId)}`);
            const studentListJSON = await studentListResponse.json();
            const studentList = studentListJSON.page.content;
            console.log(`SF:[${currentResult}/${studyFormationMaxResults}]: ${studentList.length} students`);

            //for each student
            for (let studentCounter = 0; studentCounter < studentList.length; studentCounter++) {
                const student = studentList[studentCounter];
                const studentId = student.id;

                //get student attributes
                const studentAttributesResponse = await siiirFetch(`${studentAttributesUrl}`, `${studentAttributesParam(studentId, 1558)}`);
                const studentAttributesJSON = await studentAttributesResponse.json();
                const studentHasAttribute = studentAttributesJSON.page.total > 0;

                if (!studentHasAttribute) {
                    continue;
                }

                //get parents
                const parentListResponse = await siiirFetch(`${parentListUrl}`, `${parentListParam(studentId)}`);
                const parentListJSON = await parentListResponse.json();
                const parentList = parentListJSON.page.content;

                let attributeCounter = 0;

                let parentsAttribArray = [];

                for (let i = 0; i < parentList.length; i++) {
                    const parent = parentList[i].parent;
                    const parentId = parent.id;

                    //get parent attributes
                    const parentAttributesResponse = await siiirFetch(`${parentAttributesUrl}`, `${parentAttributesParam(parentId, 1578)}`);
                    const parentAttributesJSON = await parentAttributesResponse.json();
                    const parentAttributesNr = parentAttributesJSON.page.total;

                    if (parentAttributesNr > 0) {
                        attributeCounter++;
                        console.log(`SF:[${currentResult}/${studyFormationMaxResults}],ST:[${studentCounter}/${studentList.length}],P:[${i}/${parentList.length}] Parent:${parentId} has attribute`);
                        parentsAttribArray.push({ parentAssoc: parentList[i], attrib: parentAttributesJSON.page.content[0] });
                    }

                    //if it's last parent and no attribute until now, add attribute to parent
                    if (attributeCounter === 0 && i === (parentList.length - 1)) {
                        //add attribute to parent
                        const addAttributeResult = await siiirPost(`${addParentAttributeUrl}`, addParentAttributePayload(parent));
                        const addAttributeResultJSON = await addAttributeResult.json();
                        const attrib = addAttributeResultJSON.baseEntity;

                        console.log(`SF:[${currentResult}/${studyFormationMaxResults}],ST:[${studentCounter}/${studentList.length}] Added attribute to parentId:${parentId}`);
                        parentsAttribArray.push({ parentAssoc: parentList[i], attrib: attrib });
                    } else {
                        parentsAttribArray.push({ parentAssoc: parentList[i], attrib: null });
                    }
                }

                //delete attribute from other parents if more than 1 attribute
                if (attributeCounter > 1) {
                    const parentsWithAttrib = parentsAttribArray.filter(parentAttrib => parentAttrib.attrib !== null);
                    parentsWithAttrib.sort(sortParentAssoc);
                    for (let i = 0; i < parentsWithAttrib.length - 1; i++) {
                        const parent = parentsWithAttrib[i].parentAssoc.parent;
                        const parentAttributeId = parentsWithAttrib[i].attrib.id;
                        await siiirDelete(`${deleteParentAttributeUrl(parentAttributeId)}`, parentsWithAttrib[i].attrib);
                        console.log(`SF:[${currentResult}/${studyFormationMaxResults}],ST:[${studentCounter}/${studentList.length}] Deleted attribute from parentId:${parent.id}`);
                    }
                }
            }
        }
    }
    console.log("Done!");
})();