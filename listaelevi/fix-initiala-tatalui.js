(async () => {
    const listUrl = `https://siiir.edu.ro/siiir/list/asocStudentAtSchoolView.json`;
    const schoolYearId = AMS.util.AppConfig.selectedYear.data.id;
    const listParams = `generatorKey=ASSQG&filter=%5B%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A${schoolYearId}%7D`;


    const getActionUrl = (assocId) => `https://siiir.edu.ro/siiir/management/student/${assocId}/?schoolYearId=${schoolYearId}&id=${assocId}&_dc=${new Date().getTime()}`;
    const putActionUrl = (assocId) => `https://siiir.edu.ro/siiir/management/student/${assocId}/?schoolYearId=${schoolYearId}&id=${assocId}&_dc=${new Date().getTime()}`;

    const doubleLetters = ["CS", "DZ", "GY", "LY", "NY", "SZ", "TY", "ZS",];
    const tripleLetters = ["DZS",];
    const splitInitials = (initials) => {
        const trimmed = initials.toUpperCase().trim();
        if (trimmed === "") return "";

        //if last character is a dot return
        if (trimmed[trimmed.length - 1] === ".") return trimmed;

        //if length is 1 return with dot
        if (trimmed.length === 1) return trimmed + ".";

        const doSplit = (input, output) => {
            if (input.length === 0) return output;
            if (input.length === 1) return output + input + ".";
            if (input.length === 2) {
                if (doubleLetters.includes(input)) return output + input + ".";
                return output + input[0] + "." + input[1] + ".";
            }
            if (input.length === 3) {
                if (tripleLetters.includes(input)) return output + input + ".";
                //check if first two are a double letter
                if (doubleLetters.includes(input.slice(0, 2))) return output + input.slice(0, 2) + "." + input[2] + ".";
                //else check last two
                if (doubleLetters.includes(input.slice(1, 3))) return output + input[0] + "." + input.slice(1, 3) + ".";
                //else split into 3
                return output + input[0] + "." + input[1] + "." + input[2] + ".";
            }

            //check if first 3 are a triple letter
            if (tripleLetters.includes(input.slice(0, 3))) return doSplit(input.slice(3), output + input.slice(0, 3) + ".");
            //else check if first two are a double letter
            if (doubleLetters.includes(input.slice(0, 2))) return doSplit(input.slice(2), output + input.slice(0, 2) + ".");
            //else split first
            return doSplit(input.slice(1), output + input[0] + ".");
        };

        return doSplit(trimmed, "");
    }

    console.log(splitInitials("BSZLDZSTYL"));

})();