//IIFE for async
(async () => {
  const actionUrl =
    "https://www.siiir.edu.ro/siiir/management/entityAttributeValueEmployee";

  const deleteActionUrl =
    "https://www.siiir.edu.ro/siiir/management/entityAttributeValueEmployee/";

  const listUrl =
    "https://www.siiir.edu.ro/siiir/list/entityAttributeValueEmployee.json";
  const listParamPrefix = `generatorKey=entityAttributeValueEmployee_GRID_GENERATOR_KEY&filter=%5B%7B%22property%22%3A%22employee.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B`
  const listParamSuffix = `%5D%7D%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%7D&page=1&start=0&limit=25`;

  const attribValuesToGenerate = {
    "1281": {
      id: 1281,
      value: "DA",
    },
    "1282": {
      id: 1282,
      value: "L",
    },
  };

  const attribIdArray = Object.keys(attribValuesToGenerate);

  const elevIds = `1234
1235
1236
1237`;

  const attributeTemplate = {
    id: null,
    valueDate: null,
    employee: {
      id: 0,
    },
    dateTo: null,
    numberValue: null,
    entityAttribute: {
      id: 0,
    },
    value: "",
    properties: null,
    dateFrom: "2020-11-01",
  };

  //restore console
  let iFrame = document.createElement("iframe");
  iFrame.style.display = "none";
  document.body.appendChild(iFrame);
  window.console = iFrame.contentWindow.console;

  const siiirFetch = async (url, body) =>
    fetch(`${url}?_dc=${new Date().getTime()}`, {
      method: "post",
      credentials: "same-origin",
      headers: {
        Accept: "application/json",
        "Content-type": "application/x-www-form-urlencoded",
      },
      body: body,
    });

  const siiirPost = async (url, body) =>
    fetch(`${url}?_dc=${new Date().getTime()}`, {
      method: "post",
      credentials: "same-origin",
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
      },
      body: JSON.stringify(body),
    });

  const siiirDelete = async (url, id, body) =>
    fetch(`${url}${id}?_dc=${new Date().getTime()}`, {
      method: "delete",
      credentials: "same-origin",
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
      },
      body: JSON.stringify(body),
    });

  const ids = elevIds.split("\n");

  let progressMax = ids.length;
  let progress = 0;

  for (const elevId of ids) {
    //get attribute list
    const attribResult = await siiirFetch(
      listUrl,
      listParamPrefix + elevId + listParamSuffix
    );
    const attribJSON = await attribResult.json();

    const foundAttrib = {};
    for (const attribute of attribJSON.page.content) {
      const attribId = attribute.entityAttribute.id;
      const attribValue = attribute.value;
      //delete any bad attribute (internet=N)
      if (attribId == 1281 && attribValue == "N") {
        console.log("Found bad attribute, deleting");
        //await siiirDelete(deleteActionUrl, attribute.id, attribute);
      } else {
        //increase duplicate count
        foundAttrib["" + attribId] = (foundAttrib["" + attribId] + 1) || 1;
      }
    }

    for (const attribIdKey of attribIdArray) {
      //check if attribute already exists
      if (!foundAttrib.hasOwnProperty(attribIdKey)) {
        //create any missing attributes
        const payload = attributeTemplate;
        payload.employee.id = elevId;
        payload.entityAttribute.id = attribIdKey;
        payload.value = attribValuesToGenerate[attribIdKey].value;
        await siiirPost(actionUrl, payload);
      }
    }

    progress++;
    console.log(`${progress} / ${progressMax}`);
  }
  console.log("DONE!");
})();