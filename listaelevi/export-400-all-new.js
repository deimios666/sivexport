(async () => {
  const requestURL = "/siiir/excel/400.jasper";

  const pages = 4; //nr of max pages 1..x

  const requestParams = [
    "type=xls&source=StudyFormationStudentManagementSql&schoolYearId=",
    "&schoolYearCode=",
    "&schoolYearDescription=",
    "&previousSchoolYearId=",
    "&previousSchoolYearCode=",
    "&previousSchoolYearDescription=",
    "&nextSchoolYearId=",
    "&applicationVersion=1.8.0-rc126&schoolId=-1&locality=&localityId=-1&street=&streetNumber=&postalCode=&phoneNumber=&faxNumber=&schoolName=&countyId=",
    "&internalSchoolId=-1&levelId=-1&propertyFormId=-1&studyFormationTypeId=-1&countyClause=+s2.id_county+%3D+",
    "&localityClause=+1%3D1&internalIdClause=+1%3D1&levelClause=+1%3D1&propertyFormClause=+1%3D1&studyFormationTypeClause=+1%3D1&propertyForm=-1&level=-1&studyFormationType=-1&pagina="];

  /*params: 
    schoolyearId 23
    schoolYearCode 2020-2021
    schoolYearDescription Anul+%C5%9Fcolar+2020-2021
    previousSchoolYearId 22
    previousSchoolYearCode 2019-2020
    previousSchoolYearDescription Anul+%C5%9Fcolar+2019-2020
    nextSchoolYearId 24
    countyId 16
    countyId 16 again!
    pagina 1, 1-indexed, 10k per page
  */

  const countyId = 16;

  const schoolYears = [
    { id: 27, code: "2024-2025", download: false },
    { id: 26, code: "2023-2024", download: true },
    { id: 25, code: "2022-2023", download: false },
    { id: 24, code: "2021-2022", download: false },
    { id: 23, code: "2020-2021", download: false },
    { id: 22, code: "2019-2020", download: false },
    { id: 21, code: "2018-2019", download: false },
    { id: 20, code: "2017-2018", download: false },
    { id: 11, code: "2016-2017", download: false },
    { id: 10, code: "2015-2016", download: false },
    { id: 2, code: "2014-2015", download: false },
    { id: 1, code: "2013-2014", download: false }
  ];

  //restore console
  let iFrame = document.createElement('iframe');
  iFrame.style.display = 'none';
  document.body.appendChild(iFrame);
  window.console = iFrame.contentWindow.console;


  await import("https://cdnjs.cloudflare.com/ajax/libs/jszip/3.10.1/jszip.min.js");

  //include JSZip here
  const zip = new JSZip();

  for (let yearIndex = 0; yearIndex < schoolYears.length; yearIndex++) {
    const year = schoolYears[yearIndex];
    if (year.download) {
      const previousSchoolYear = schoolYears[yearIndex - 1];
      const nextSchoolYear = schoolYears[yearIndex + 1];
      for (let page = 1; page <= pages; page++) {
        console.log(`Downloading ${year.code}, page ${page}`);
        const requestParamString =
          requestParams[0] +
          year.id +//schoolyearId 23
          requestParams[1] +
          year.code +// schoolYearCode 2020-2021
          requestParams[2] +
          "Anul+%C5%9Fcolar+" + year.code +// schoolYearDescription Anul+%C5%9Fcolar+2020-2021
          requestParams[3] +
          previousSchoolYear.id +// previousSchoolYearId 22
          requestParams[4] +
          previousSchoolYear.code +// previousSchoolYearCode 2019-2020
          requestParams[5] +
          "Anul+%C5%9Fcolar+" + previousSchoolYear.code +// previousSchoolYearDescription Anul+%C5%9Fcolar+2019-2020
          requestParams[6] +
          nextSchoolYear.id +// nextSchoolYearId 24
          requestParams[7] +
          countyId +// countyId 16
          requestParams[8] +
          countyId +// countyId 16 again!
          requestParams[9] +
          page// pagina 1, 1-indexed, 10k per page
          ;

        const downloadResult = await fetch(
          requestURL,
          {
            method: 'post',
            credentials: 'same-origin',
            headers: {
              "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
              "Content-type": "application/x-www-form-urlencoded"
            },
            body: requestParamString
          }
        );

        const downloadBlob = await downloadResult.blob();
        const fileName = `an_${year.code}_p${page}.xlsx`;
        zip.file(fileName, new Blob([downloadBlob]));
      }
    }
  }
  const archive = await zip.generateAsync({ type: "blob" });
  const url = window.URL.createObjectURL(archive, {
    type: 'application/zip'
  });
  const a = document.createElement("a");
  a.href = url;
  a.download = 'arhiva.zip';
  a.click();
  URL.revokeObjectURL(url);
})();
