(async () => {
  const attribId = 1437; //id atribut 9-Venit, 1437 - Cost Standard
  const pageLimit = 100; //mărime pagină

  const schoolYearId = AMS.util.AppConfig.selectedYear.data.id;
  const listUrl = `https://www.siiir.edu.ro/siiir/list/asocStudentAtSchoolEntityAttributeView.json`;
  const listParam = `generatorKey=ASSEAQG&filter=%5B%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A${schoolYearId}%2C%22attributeId%22%3A${attribId}%7D&sort=%5B%7B%22property%22%3A%22value%22%2C%22direction%22%3A%22ASC%22%7D%5D`;

  //restore console
  let iFrame = document.createElement('iframe');
  iFrame.style.display = 'none';
  document.body.appendChild(iFrame);
  window.console = iFrame.contentWindow.console;

  const siiirFetch = async (url, body) => fetch(`${url}?_dc=${new Date().getTime()}`, {
    method: "post",
    credentials: "same-origin",
    headers: {
      Accept: "application/json",
      "Content-type": "application/x-www-form-urlencoded",
    },
    body: body,
  });


  //`&page=1&start=0&limit=1`

  const primingResponse = await siiirFetch(listUrl, `${listParam}&page=1&start=0&limit=1`);

  const primingResponseJSON = await primingResponse.json();
  const maxItems = primingResponseJSON.page.total;
  const maxPages = Math.ceil(maxItems / pageSize);

  console.log(`Candidates: ${maxItems}, Pages: ${maxPages}`);

  const csvData = [];

  for (let page = 1; page <= maxPages; page++) {
    console.log(`Processing page: ${page}/${maxPages}`);
    const listResponse = await siiirFetch(listUrl, `${listParam}&page=${page}&start=${(page - 1) * pageLimit}&limit=${pageLimit}`);
    const listJSON = await listResponse.json();

    for (const item of listJSON.page.content) {
      if (item.from !== null) {
        csvData.push(item.student.id);
      }
    }
  }



  const fileName = "export_eleviid.csv";
  const buffer = "\uFEFF" + "studentId" + "\n" + csvData.join("\n");
  const blob = new Blob([buffer], {
    type: "text/csv;charset=utf8;",
  });
  const url = window.URL.createObjectURL(blob);
  const a = document.createElement("a");
  a.href = url;
  a.download = fileName;
  a.click();

})();