//IIFE for async
(async() => {
    const listUrl = "https://www.siiir.edu.ro/siiir/list/asocStudentAtSchool.json";
    const limit = 1000;
    const fromDate = "2020-02-01";
    const toDate = "2020-09-01";
    const status = "străină";
    const listParams = `generatorKey=ALUMNIQG&filter=%5B%7B%22property%22%3A%22to%22%2C%22criteria%22%3A%22BETWEEN%22%2C%22dateProperty%22%3Atrue%2C%22parameters%22%3A%5B%22${fromDate}%22%2C%22${toDate}%22%5D%7D%2C%7B%22property%22%3A%22studentSchoolStatus.description%22%2C%22criteria%22%3A%22LIKE%22%2C%22parameters%22%3A%5B%22${status}%22%5D%7D%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A21%7D&page=1&start=0&limit=${limit}`;
    const headers = [
        ["CNP", "student.nin"],
        ["Unitatea", "school.longName"],
        ["Nume", "student.lastName"],
        ["Ini", "student.fatherInitial"],
        ["Prenume", "student.firstName"],
        ["Prenume2", "student.firstName1"],
        ["Prenume3", "student.firstName2"],
        ["Cetățenie", "student.citizenship.description"],
        ["Naționalitate", "student.nationality.description"],
        ["Limba maternă", "student.language.description"],
        ["Stare", "studentSchoolStatus.description"],
        ["Data dezasocierii", "to"]
    ];
    //const csvData = [];

    const listResult = await fetch(listUrl + "?_dc=" + new Date().getTime(), {
        method: "post",
        credentials: "same-origin",
        headers: {
            Accept: "application/json",
            "Content-type": "application/x-www-form-urlencoded",
        },
        body: listParams,
    });

    const listJSON = await listResult.json();

    const csvData = await Promise.all(
        listJSON.page.content.map(
            async(item, index) => {
                return headers.map(key => {
                    const keyPath = key[1];
                    try {
                    const value = keyPath.split('.')
                        .reduce((o, i) => o[i], item) //resolve value
                        .toString()
                        .split('"').join('""'); //escape quotes
                    return `"${value}"`; //return the formatted field
                    }catch(err){
                      return `""`;
                    }
                }).join(',');
            }
        )
    );

    const fileName = "export_alumni.csv";
    const buffer = "\uFEFF" + headers.map(h => h[0]).join(",") + "\n" + csvData.join("\n");
    const blob = new Blob([buffer], {
        type: "text/csv;charset=utf8;",
    });

    const url = window.URL.createObjectURL(blob);
    const a = document.createElement("a");
    a.href = url;
    a.download = fileName;
    a.click();
})();