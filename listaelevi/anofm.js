//IIFE for async
(async () => {
  const cnp = 1234567890123;
  const listUrl = "https://www.siiir.edu.ro/siiir/studentManagement/anofm.json";
  const listParams = `nin=${cnp}`;

  //restore console
  let iFrame = document.createElement('iframe');
  iFrame.style.display = 'none';
  document.body.appendChild(iFrame);
  window.console = iFrame.contentWindow.console;

  const siiirFetch = async (url, body) => fetch(`${url}?_dc=${new Date().getTime()}`, {
    method: "post",
    credentials: "same-origin",
    headers: {
      Accept: "application/json",
      "Content-type": "application/x-www-form-urlencoded",
    },
    body: body,
  });


  //get nr of results
  const listResult = await siiirFetch(listUrl, listParams);
  const listJSON = await listResult.json();
  console.log(listJSON);

})();