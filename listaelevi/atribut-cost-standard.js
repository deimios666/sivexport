(async () => {
  const listUrl = "https://siiir.edu.ro/siiir/list/asocStudentAtSchoolView.json";
  const currentSchoolYear = AMS.util.AppConfig.selectedYear.data.id;
  const listParams = `generatorKey=ASSQG&filter=%5B%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A${currentSchoolYear}%7D`;
  const formatiuniListURL = "https://siiir.edu.ro/siiir/list/studyFormation.json";
  const formatiuniListParams = `generatorKey=SFQG&filter=%5B%5D&sort=%5B%7B%22property%22%3A%22studyFormationType.orderBy%22%2C%22direction%22%3A%22ASC%22%7D%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A${currentSchoolYear}%7D`;
  const eleviListURL = "https://siiir.edu.ro/siiir/list/asocStudent.json";
  const eleviListParams = formationId => `generatorKey=asocStudent_GRID_GENERATOR_KEY&filter=%5B%7B%22property%22%3A%22studyFormation.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B${formationId}%5D%7D%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%7D`;
  const pageLimit = 1000;
  const maxParallel = 4;
  const attribListUrl = "https://siiir.edu.ro/siiir/list/entityAttributeValueStudent.json";
  const attribListParams = (studentId, attribId) => `generatorKey=entityAttributeValueStudent_GRID_GENERATOR_KEY&filter=%5B%7B%22property%22%3A%22entityAttribute.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B%22${attribId}%22%5D%7D%2C%7B%22property%22%3A%22student.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B${studentId}%5D%7D%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%7D&page=1&start=0&limit=25`
  const addAttribUrl = "https://siiir.edu.ro/siiir/management/entityAttributeValueStudent";

  const attributeTemplate = {
    id: null,
    valueDate: null,
    student: {
      id: 0,
    },
    dateTo: null, //Data "până la", format identic cu "de la"
    numberValue: null,
    entityAttribute: {
      id: 1437,
    },
    value: "DA",
    properties: null,
    dateFrom: "2024-09-01", //Data "de la"
  };

  const siiirFetch = async (url, body) => fetch(`${url}?_dc=${new Date().getTime()}`, {
    method: "post",
    credentials: "same-origin",
    headers: {
      Accept: "application/json",
      "Content-type": "application/x-www-form-urlencoded",
    },
    body: body,
  });

  const siiirPost = async (url, body) =>
    fetch(`${url}?_dc=${new Date().getTime()}`, {
      method: "post",
      credentials: "same-origin",
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
      },
      body: JSON.stringify(body),
    });

  const addAttribute = async (studentId) => {
    const payload = { ...attributeTemplate };
    payload.student.id = studentId;
    await siiirPost(addAttribUrl, payload);
  }

  const checkAndFixAttribute = async (studentId) => {
    const attributeResult = await siiirFetch(attribListUrl, attribListParams(studentId, 1437));
    const attributeJSON = await attributeResult.json();
    if (attributeJSON.page.total === 0) {
      //console.log(`e[${studentId}]: Adding attribute`);
      await addAttribute(studentId);
      return 1;
    }
    return 0;
  }

  //restore console
  let iFrame = document.createElement("iframe");
  iFrame.style.display = "none";
  document.body.appendChild(iFrame);
  window.console = iFrame.contentWindow.console;

  /**
 * Executes the given tasks in parallel with a given concurrency limit
 * @async
 * @function limit
 * @param {Array<function(): Promise<any>>} tasks Array of the async tasks to be executed
 * @param {Number} concurrencyLimit Must be a positive integer
 * @returns {Any[]} Array of return values and/or errors
 */
  const limit = async (tasks, concurrencyLimit) => {
    const results = [];

    const runTasks = async (tasksIterator) => {
      for (const [index, task] of tasksIterator) {
        try {
          results[index] = await task();
        } catch (error) {
          results[index] = new Error(`Failed with: ${error.message}`);
        }
      }
    }

    const workers = new Array(concurrencyLimit)
      .fill(tasks.entries())
      .map(runTasks);

    await Promise.allSettled(workers);

    return results;
  }

  //MAIN START
  const primingListResult = await siiirFetch(formatiuniListURL, formatiuniListParams + "&limit=1&page=1&start=0");
  const primingListJSON = await primingListResult.json();
  const maxItems = primingListJSON.page.total;
  const maxPages = Math.ceil(maxItems / pageLimit); //calculate nr of pages
  let totalToAdd = 0;

  for (let page = 1; page <= maxPages; page++) {
    const start = (page - 1) * pageLimit;
    console.log(`Processing page ${page} of ${maxPages}`);
    const formationResult = await siiirFetch(formatiuniListURL, formatiuniListParams + "&limit=" + pageLimit + "&page=" + page + "&start=" + start);
    const formationJSON = await formationResult.json();
    const formationList = formationJSON.page.content;
    for (let formation of formationList) {
      //console.log(`f[${formation.id}]: Processing`);
      //check if formation is ineligible for cost standard
      //this excludes învățământ special and privat postliceal
      let eligible = true;
      if (formation.educationType.code === "SPECIAL") {
        console.log(`f[${formation.id}]: Excluding Special formation`);
        eligible = false
      }
      if (formation.school.propertyForm.code === "PRIVATA") {
        eligible = false;
        console.log(`f[${formation.id}]: Excluding Privat`);
      }
      /*if (formation.level.code === "POST" && formation.school.propertyForm.code === "PRIVATA") {
        eligible = false;
        console.log(`f[${formation.id}]: Excluding Privat Postliceal formation`);
      }*/
      if (!eligible) continue;
      const eleviResult = await siiirFetch(eleviListURL, eleviListParams(formation.id));
      const eleviJSON = await eleviResult.json();
      const eleviList = eleviJSON.page.content;

      const result = await limit(eleviList.map(elev => async () => checkAndFixAttribute(elev.student.id)), maxParallel);
      totalToAdd += result.filter(r => r === 1).length;
      console.log(`Current total: ${totalToAdd}`);
      //for (let elev of eleviList) {
      //we skip Transferat
      //if (elev.studyStatus.code === "TRSF") continue;
      //console.log(`Processing elev ${elev.student.id}`);
      // await checkAndFixAttribute(elev.student.id);
      //}
    }
  }
  console.log(`FINAL Total added: ${totalToAdd}`);
}
)();