(async () => { //top level async

  const listUrl = "https://www.siiir.edu.ro/siiir/populateBDNEDataForNIN";
  //"nin=";
  const actionUrl = "https://www.siiir.edu.ro/siiir/management/student/";

  const asocDate = "2022-09-01";

  const cnps = [
    "5123456789012",
  ]

  //restore console
  const iFrame = document.createElement('iframe');
  iFrame.style.display = 'none';
  document.body.appendChild(iFrame);
  window.console = iFrame.contentWindow.console;

  const siiirFetch = async (url, body) => fetch(`${url}?_dc=${new Date().getTime()}`, {
    method: "post",
    credentials: "same-origin",
    headers: {
      Accept: "application/json",
      "Content-type": "application/x-www-form-urlencoded",
    },
    body: body,
  });

  const siiirPostId = async (url, body, id) => fetch(`${url}/${id}?_dc=${new Date().getTime()}`, {
    method: "post",
    credentials: "same-origin",
    headers: {
      Accept: "application/json",
      "Content-type": "application/json",
    },
    body: JSON.stringify(body),
  });


  const siiirDateToString = (dateNumber) => {
    const date = new Date(dateNumber);
    return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
  }

  const CSVData = [];
  for (const cnp of cnps) {
    const startTime = performance.now();
    const findResponse = await siiirFetch(listUrl, `nin=${cnp}`);
    const findData = await findResponse.json();
    if (findData.success) {
      const {
        birthPlace, birthDate, citizenship,
        country, firstName, firstName1,
        firstName2, language, lastName,
        nationality, id, registrationVolume,
        registrationNumber, registrationPage,
        serialNo, gender, from, fatherInitial
      } = findData.student;
      const payload = {
        "asocUnitDateFrom": asocDate,
        "birthDate": birthDate,
        "birthPlace": birthPlace,
        "citizenship": citizenship,
        "ciValid": null,
        "country": country,
        "descriptionWithCNP": null,
        "fatherInitial": fatherInitial,
        "firstName": firstName,
        "firstName1": firstName1,
        "firstName2": firstName2,
        "from": from,
        "gender": gender,
        "id": id,
        "language": language,
        "lastName": lastName,
        "nationality": nationality,
        "nin": cnp,
        "photoId": 0,
        "properties": {
          "FORM": "ENROLL-SPECIAL"
        },
        "registrationNumber": registrationNumber,
        "registrationPage": registrationPage,
        "registrationVolume": registrationVolume,
        "serialNo": serialNo,
        "to": null
      }


      await siiirPostId(actionUrl, payload, id);
      const endTime = performance.now();

      CSVData.push(`${cnp},OK,${endTime - startTime}`);
      console.log(`${cnp},OK,${endTime - startTime}`);
    } else {
      const endTime = performance.now();
      CSVData.push(`${cnp},EROARE,${endTime - startTime}`);
      console.log(`${cnp},ERORARE,${endTime - startTime}`);
    }
  }
  console.log("DONE!");
  const fileName = "export_asociere.csv";
  const buffer = "\uFEFF" + "CNP,STATUS,TIME" + "\n" + CSVData.join("\n");
  const blob = new Blob([buffer], {
    type: "text/csv;charset=utf8;",
  });
  const url = window.URL.createObjectURL(blob);
  const a = document.createElement("a");
  a.href = url;
  a.download = fileName;
  a.click();
})();
