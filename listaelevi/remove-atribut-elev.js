(async () => {

    const attribId = 1598;

    const elevattribListUrl = "/siiir/list/entityAttributeValueStudent.json";
    const elevattribParam = (studentId) => `generatorKey=entityAttributeValueStudent_GRID_GENERATOR_KEY&filter=%5B%7B%22property%22%3A%22entityAttribute.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B%22${attribId}%22%5D%7D%2C%7B%22property%22%3A%22student.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B${studentId}%5D%7D%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%7D&page=1&start=0&limit=25`;

    const attribDeleteUrl = (entityAttribAssocId) => `/siiir/management/entityAttributeValueStudent/${entityAttribAssocId}`;

    //restore console
    let iFrame = document.createElement("iframe");
    iFrame.style.display = "none";
    document.body.appendChild(iFrame);
    window.console = iFrame.contentWindow.console;

    const siiirFetch = async (url, body) => fetch(`${url}?_dc=${new Date().getTime()}`, {
        method: "post",
        credentials: "same-origin",
        headers: {
            Accept: "application/json",
            "Content-type": "application/x-www-form-urlencoded",
        },
        body: body,
    });

    const siiirDelete = async (url, body) =>
        fetch(`${url}`, {
            method: "delete",
            credentials: "same-origin",
            headers: {
                Accept: "application/json",
                "Content-type": "application/json",
            },
            body: JSON.stringify(body),
        });


    const studentIds =
        [
            14168000,
        ]

    for (const studentId of studentIds) {
        const response = await siiirFetch(elevattribListUrl, elevattribParam(studentId));
        const elevattribList = await response.json();

        elevattribList.page.content.filter(elevattrib => elevattrib.entityAttribute.id === attribId).forEach(async (elevattrib) => {
            console.log(`Deleting ${elevattrib.id}`);
            await siiirDelete(attribDeleteUrl(elevattrib.id), elevattrib);
        });
    }

    console.log("Done!");

})();