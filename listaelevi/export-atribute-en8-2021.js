//IIFE for async
(async () => {

  const propertiesToExport = [
    { title: "Judet", value: "school.county.code" },
    { title: "Localitate", value: "studyFormation.school.locality.description" },
    { title: "Cod SIIIR unitate elev", value: "studyFormation.school.code" },
    { title: "Denumire unitate elev", value: "studyFormation.school.longName" },
    { title: "Forma de proprietate", value: "studyFormation.school.propertyForm.code" },
    { title: "CNP", value: "student.nin" },
    { title: "Nume elev", value: "student.lastName" },
    { title: "Initiala tata elev", value: "student.fatherInitial" },
    { title: "Prenume elev 1", value: "student.firstName" },
    { title: "Prenume elev 2", value: "student.firstName1" },
    { title: "Prenume elev 3", value: "student.firstName2" },
    { title: "Limba materna elev", value: "studyFormation.teachingLanguage.description" },
    { title: "Forma de invatamant", value: "studyFormation.educationForm.description" },
    { title: "Clasa", value: "studyFormation.studyFormationType.code" },
    { title: "Nume Clasa", value: "studyFormation.name" },
    { title: "Nivel", value: "studyFormation.level.description" },
  ];
  const attributesToExport = {
    1146: { title: "STATUS LROM", col: 0 },
    1145: { title: "STATUS LMAT", col: 1 },
    1147: { title: "STATUS MATEM", col: 2 },
    1023: { title: "NOTA LROM", col: 3 },
    1043: { title: "NOTA LMAT", col: 4 },
    1025: { title: "NOTA MATEM", col: 5 },
  };

  const filter = "VIII"; //only search 8th grade
  const pageSize = 500; //lower if the platform is overloaded

  const listUrl = `https://www.siiir.edu.ro/siiir/list/asocStudentAtSchoolView.json`;
  const schoolYearId = AMS.util.AppConfig.selectedYear.data.id;
  //const listParam = `generatorKey=ASSQG&filter=%5B%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A${schoolYearId}%7D`;
  const listParam = `generatorKey=ASSQG&filter=%5B%7B%22property%22%3A%22formationCode%22%2C%22criteria%22%3A%22LIKE%22%2C%22parameters%22%3A%5B%22${filter}%22%5D%7D%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A${schoolYearId}%7D`;
  const attribListUrl =
    "https://www.siiir.edu.ro/siiir/list/entityAttributeValueStudent.json";
  const attribListParamPrefix = `generatorKey=entityAttributeValueStudent_GRID_GENERATOR_KEY&filter=%5B%7B%22property%22%3A%22student.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B`;
  const attribListParamSuffix = `%5D%7D%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%7D&page=1&start=0&limit=50`;

  const attribHeaders = [];
  Object.keys(attributesToExport).forEach(key => {
    attribHeaders[attributesToExport[key].col] = attributesToExport[key].title;
  });

  const CSVData = [];

  //restore console
  let iFrame = document.createElement("iframe");
  iFrame.style.display = "none";
  document.body.appendChild(iFrame);
  window.console = iFrame.contentWindow.console;

  const siiirFetch = async (url, body) =>
    fetch(`${url}?_dc=${new Date().getTime()}`, {
      method: "post",
      credentials: "same-origin",
      headers: {
        Accept: "application/json",
        "Content-type": "application/x-www-form-urlencoded",
      },
      body: body,
    });

  const siiirPost = async (url, body) =>
    fetch(`${url}?_dc=${new Date().getTime()}`, {
      method: "post",
      credentials: "same-origin",
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
      },
      body: JSON.stringify(body),
    });

  const listParamLimited = listParam + "&page=1&start=0&limit=1";
  const primingResponse = await siiirFetch(listUrl, listParamLimited);

  const primingJSON = await primingResponse.json();
  const progressMax = primingJSON.page.total;
  let progress = 0;

  //calculate pages
  const pages = [];
  const maxPages = Math.ceil(progressMax / pageSize);

  for (let i = 0; i < maxPages; i++) {
    pages.push({
      page: i + 1,
      start: i * pageSize
    });
  }

  for (const page of pages) {
    const listParamFilled = `${listParam}&page=${page.page}&start=${page.start}&limit=${pageSize}`;
    const elevListResult = await siiirFetch(listUrl, listParamFilled);
    const eleviListResultJSON = await elevListResult.json();
    const eleviList = eleviListResultJSON.page.content;
    for (const elev of eleviList) {
      const elevId = elev.student.id;
      const oneCSVLine = propertiesToExport.map(item => {
        try {
          let value = item.value.split('.').reduce((o, i) => o[i], elev);
          if (typeof value == "string") {
            value = value.replaceAll('"', '""')
          }
          if (value == null) {
            return "";
          } else {
            return value;
          }
        } catch (err) {
          return "";
        }
      });

      //initialize the attributes array with empty values
      const attribCSVLine = Array(Object.keys(attributesToExport).length).fill("");

      //get attribute list
      const attribResult = await siiirFetch(
        attribListUrl,
        attribListParamPrefix + elevId + attribListParamSuffix
      );
      const attribJSON = await attribResult.json();
      for (const attribute of attribJSON.page.content) {
        const attribId = attribute.entityAttribute.id;
        const attribValue = attribute.value;
        if (attribId in attributesToExport) {
          const attribute = attributesToExport[attribId];
          attribCSVLine[attribute.col] = attribValue;
        }
      }

      CSVData.push(
        `"` + oneCSVLine.join('","') + `","` + attribCSVLine.join('","') + `"`
      )

      progress++;
      console.log(`${progress} / ${progressMax}`);
    }
  }

  console.log("DONE!");
  
  const dateString = (new Date()).toISOString().substring(0, 10);
  const fileName = `export_atribute_${dateString}.csv`;
  const buffer = "\uFEFF" + propertiesToExport.map(h => `"${h.title}"`).join(",") + "," + attribHeaders.join(",") + "\n" + CSVData.join("\n");
  const blob = new Blob([buffer], {
    type: "text/csv;charset=utf8;",
  });

  const url = window.URL.createObjectURL(blob);
  const a = document.createElement("a");
  a.href = url;
  a.download = fileName;
  a.click();

})();