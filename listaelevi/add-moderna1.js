(async () => {
    //this script adds limba moderna1 engleza to all elevi without limba moderna1
    const schoolYearId = AMS.util.AppConfig.selectedYear.data.id;
    const pageSize = 500;
    const listUrl = "/siiir/list/studyFormation.json";
    const listParam = `generatorKey=SFQG&filter=%5B%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A${schoolYearId}%7D`;

    const assocListUrl = "/siiir/list/asocStudent.json";

    //only list those with no language
    const assocListParam = id => `generatorKey=asocStudent_GRID_GENERATOR_KEY&filter=%5B%7B%22property%22%3A%22studyFormation.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B${id}%5D%7D%2C%7B%22property%22%3A%22firstLanguage%22%2C%22criteria%22%3A%22IS_NULL%22%2C%22parameters%22%3A%5B%5D%7D%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%7D&page=1&start=0&limit=1000`;

    const studentGetUrl = id => `/siiir/management/asocStudent/${id}?_dc=${new Date().getTime()}&schoolYearId=${schoolYearId}&id=${id}`;
    const studentPutUrl = id => `/siiir/management/asocStudent/${id}?_dc=${new Date().getTime()}&schoolYearId=${schoolYearId}`;

    //restore console
    let iFrame = document.createElement("iframe");
    iFrame.style.display = "none";
    document.body.appendChild(iFrame);
    window.console = iFrame.contentWindow.console;

    const siiirFetch = async (url, body) => fetch(`${url}?_dc=${new Date().getTime()}`, {
        method: "post",
        credentials: "same-origin",
        headers: {
            Accept: "application/json",
            "Content-type": "application/x-www-form-urlencoded",
        },
        body: body,
    });

    const siiirGet = async (url, body) => fetch(`${url}`, {
        method: "get",
        credentials: "same-origin",
        headers: {
            Accept: "application/json",
            "Content-type": "application/x-www-form-urlencoded",
        },
    });

    const firstLanguage = { "id": 12 }
    const firstLanguageKnowledgeLevel = { "id": 1 }

    const primingListResponse = await siiirFetch(`${listUrl}`, `${listParam}&limit=1&start=0&page=1`);
    const primingListJSON = await primingListResponse.json();
    const maxResults = primingListJSON.page.total;
    let currentResult = 0;

    const maxPages = Math.ceil(maxResults / pageSize);

    for (let page = 1; page <= maxPages; page++) {
        const start = (page - 1) * pageSize;
        console.log(`Downloading page ${page}/${maxPages}`);
        const studyFormationListResponse = await siiirFetch(`${listUrl}`, `${listParam}&limit=${pageSize}&start=${start}&page=${page}`);
        const studyFormationListJSON = await studyFormationListResponse.json();
        const studyFormationList = studyFormationListJSON.page.content;
        for (const studyFormation of studyFormationList) {
            currentResult++;
            if (studyFormation.level.id > 2) {
                console.log(`[${currentResult}/${maxResults}] processing`);
                const assocListResponse = await siiirFetch(`${assocListUrl}`, `${assocListParam(studyFormation.id)}`);
                const assocListJSON = await assocListResponse.json();
                const assocList = assocListJSON.page.content;
                for (const assoc of assocList) {
                    const studentGetResponse = await siiirGet(`${studentGetUrl(assoc.id)}`);
                    const studentGetJSON = await studentGetResponse.json();
                    const student = studentGetJSON.baseEntity;
                    student.firstLanguage = firstLanguage;
                    student.firstLanguageKnowledgeLevel = firstLanguageKnowledgeLevel;
                    const studentPutResponse = await fetch(`${studentPutUrl(assoc.id)}`,
                        {
                            method: "put",
                            credentials: "same-origin",
                            headers: {
                                Accept: "application/json",
                                "Content-type": "application/json",
                            },
                            body: JSON.stringify(student),
                        });
                    await studentPutResponse.json();
                    console.log(`[${currentResult}/${maxResults}] student:${student.id} fixed`);
                }
            } else {
                console.log(`[${currentResult}/${maxResults}] skipped`);
            }
        }
    }

})()