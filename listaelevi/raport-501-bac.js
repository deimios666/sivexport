(async () => {
  const idJudet = AMS.util.AppConfig.filtersConfig['county'][0].parameters[0];
  const requestURL = "https://www.siiir.edu.ro/siiir/excel/501.jasper";
  const requestParams = `type=xls&source=PrezentaSimulareBAC&countyId=${idJudet}`;

  //restore console
  let iFrame = document.createElement('iframe');
  iFrame.style.display = 'none';
  document.body.appendChild(iFrame);
  window.console = iFrame.contentWindow.console;

  const reportResult = await fetch(requestURL, {
    method: 'post',
    credentials: 'same-origin',
    headers: {
      "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
      "Content-type": "application/x-www-form-urlencoded"
    },
    body: requestParams
  });

  const dataBlob = await reportResult.blob();

  const contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
  
  const url = window.URL.createObjectURL(dataBlob, {
    type: contentType
  });
  const a = document.createElement("a");
  a.href = url;
  a.download = 'raport_501.xlsx';
  a.click();
  URL.revokeObjectURL(url);
})()
