(async () => {

  const venitId = 9;

  const elevUrl = "https://www.siiir.edu.ro/siiir/list/asocStudentAtSchoolView.json";

  const parinteUrl = "https://www.siiir.edu.ro/siiir/list/studentParentOrTutor.json";

  const addressUrl = "https://www.siiir.edu.ro/siiir/list/studentAddress.json";

  const attribUrl = "https://www.siiir.edu.ro/siiir/list/entityAttributeValueStudent.json";


  const siiirFetch = async (url, param) => {
    const result = await fetch(url, {
      method: "post",
      credentials: 'same-origin',
      headers: {
        "Accept": "application/json",
        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
      },
      body: param
    });

    return await result.json();
  }

  const csvData = [];

  const headers = [
    "ID Elev",
    "Judet",
    //elev
    "Nume si prenume elev",
    "CNP elev",
    "Unitate de invatamant",
    "PJ/AR",
    "Nivel",
    "Clasa",
    "Localitate unitate",
    "Localitate superioara",
    //parinte
    "Nume si prenume parinte",
    "CNP parinte",
    //adresa
    "Adresa elev",
    "Localitate adresa",
    "Localitate superioara adresa",
    //attrib
    "Venit"
  ];

  const studentIds = [752251,
    752301, 752351, 752401, 752551, 752601, 752651, 752701, 752851, 752901, 753751, 754051, 755001,
    866951, 867851, 867901, 868101, 868151, 868251, 868501, 868551, 869001, 869051, 985701, 1015651,
    1016751, 1017651, 1018451, 1018501, 1018551, 1018601, 1018651, 1018751, 1018801, 1018851, 1018901,
    1018951, 1171401, 1173251, 1173301, 1173501, 1173601, 1173951, 1174151, 1174201, 1175001, 1640451,
    1643001, 1643151, 1740751, 1741851, 1746451, 1748251, 1879851, 1891801, 2187851, 2191351, 2191901,
    2192001, 2192051, 2192151, 2193151, 2193451, 2198401, 2198451, 2198501, 2198551, 2198601, 2198651,
    2198701, 2198851, 2198901, 2198951, 2199501, 2199551, 2199601, 2199701, 2199751, 2199801, 2199851,
    2200101, 2200151, 2200201, 2200301, 2200351, 2200401, 2200501, 2200551, 2200601, 2200651, 2200801, 2200951, 2201001, 2201051, 2201101, 2201201, 2201251, 2201301, 2201351, 2201501, 2201551, 2201651,
    2201751, 2201851, 2201951, 2202001, 2202101, 2202251, 2202451, 2202501, 2213701, 2213751, 2213801, 2213851, 2213901, 2213951, 2215501, 2215551, 2215601, 2215751, 2215801, 2215851, 2215951, 2216151, 2216201, 2216351, 2216451, 2216501, 2217251, 2342101, 2567701, 2567751, 2568951, 2569151, 2569951, 2570901, 2570951, 2571101, 2572001, 2572301, 2572351, 2572451, 2572551, 2572601, 2572701, 2572801, 2573001, 2573051, 2573101, 2573151, 2573251, 2573351, 11195948, 12148596, 12148663, 12148842, 12148849, 12148882, 12148887, 12148892, 12148896, 12148905, 12149050, 12149066, 12149071, 12149075, 12149090, 12149093, 12149107, 12149111, 12149144, 12149160, 12149171, 12149221, 12149257, 12149259, 12149270, 12149287, 12149288, 12149294, 12149299, 12149325, 12149326, 12149328, 12168352, 12168355, 12170107, 12190118, 12200899, 12201017, 12281327, 12309416, 12377666, 12377990, 12379086, 12379656, 12379859, 12379874, 12380125, 12380155, 12380352, 12380372, 12380702, 12380768, 12380883, 12380923, 12381071, 12381099, 12381445, 12381450, 12381495, 12381496, 12381498, 12381500, 12381506, 12382058, 12382063, 12382075, 12382356, 12382728, 12382777, 12382978, 12383009, 12384039, 12384981, 12387000, 12387290, 12387292, 12387297, 12388633, 12388635, 12389520, 12389524, 12389529, 12447364, 12638434, 12640547, 12642127, 12642488, 12642521, 12650934, 12650937, 12651681, 12653197, 12653201, 12679479, 12683488, 12684194, 12684288, 12685901, 12685904, 12685915, 12687871, 12687937, 12694980, 12695177, 12695827, 12695838, 12696027, 12696495, 12697067, 12697072, 12697156, 12697301, 12697325, 12699168, 12699173, 12699217, 12714341, 13000962, 13002686, 13004542, 13005179, 13006104, 13012304, 13012318, 13013669, 13016448, 13016593, 13016616, 13016623, 13017156, 13017359, 13017377, 13018091, 13018789, 13018796, 13020660, 13020673, 13020691, 13021416, 13021444, 13021801, 13022706, 13023446, 13023451, 13025325, 13025331, 13028079, 13034714, 13039280, 13044160, 13044166, 13044171, 13128874, 13242583, 13243204, 13255370, 13257286, 13257965, 13258653, 13266779, 13267939, 13279612, 13280314, 13285304, 13285314, 13285319, 13287430, 13288149, 13298343, 13299276, 13310021, 13374513, 13403592, 13451009, 13479807, 13497577, 13498291, 13519855, 13568889, 13577989, 13598655, 13598719, 13599139, 13600902, 13600945, 13601007, 13601130, 13606999, 13622417, 13668545, 13672428,
    13676984];

  for (const studentId of studentIds) {

    const csvLine = [];
    csvLine.push(studentId);
    csvLine.push("CV");

    const elevParam = `generatorKey=ASSQG&filter=%5B%7B%22property%22%3A%22student.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B%22${studentId}%22%5D%7D%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A24%7D&page=1&start=0&limit=25`;

    const elevResult = await siiirFetch(elevUrl, elevParam);

    if (elevResult.page.content.length > 0) {
      const item = elevResult.page.content[0];
      // elev
      // "Nume si prenume elev",
      let elevName = `${item.student.lastName} ${item.student.firstName}`;
      if (item.student.firstName1) {
        elevName = elevName.concat(` ${item.student.firstName1}`);
      }
      if (item.student.firstName2) {
        elevName = elevName.concat(` ${item.student.firstName2}`);
      }
      csvLine.push(elevName);

      // "CNP elev",
      csvLine.push(item.student.nin);

      // "Unitate de invatamant",
      csvLine.push(item.studyFormation.schoolPlanForStudyFormation.schoolForPlan.longName.split(`"`).join(`""`)); //needs escaping of quotes

      // "PJ/AR",
      if (item.studyFormation.schoolPlanForStudyFormation.schoolForPlan.statut.id === 1) {
        csvLine.push("PJ");
      } else {
        csvLine.push("AR");
      }

      // "Nivel",
      csvLine.push(item.studyFormation.level.description);

      // "Clasa",
      csvLine.push(item.studyFormation.code);

      // "Localitate unitate",
      csvLine.push(item.school.locality.description);

      // "Localitate superioara",
      csvLine.push(item.school.locality.parentLocality.description);


    } else {
      csvLine.push("NEGĂSIT");
      csvLine.push("");
      csvLine.push("");
      csvLine.push("");
      csvLine.push("");
      csvLine.push("");
      csvLine.push("");
      csvLine.push("");
    }


    const parinteParam = `generatorKey=studentParentOrTutor_GRID_GENERATOR_KEY&filter=%5B%7B%22property%22%3A%22student.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B${studentId}%5D%7D%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%7D&page=1&start=0&limit=25`;
    // //parinte

    const parinteResult = await siiirFetch(parinteUrl, parinteParam);

    if (parinteResult.page.content.length > 0) {
      const parinte = parinteResult.page.content[0].parent;
      // "Nume si prenume parinte",
      csvLine.push(`${parinte.lastName} ${parinte.firstName}`);
      // "CNP parinte",
      csvLine.push(`${parinte.nin}`);

    } else {
      csvLine.push("");
      csvLine.push("");
    }

    const addressParam = `generatorKey=studentAddress_GRID_GENERATOR_KEY&filter=%5B%7B%22property%22%3A%22student.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B${studentId}%5D%7D%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%7D&page=1&start=0&limit=25`;

    const addressResult = await siiirFetch(addressUrl, addressParam);
    if (addressResult.page.content.length > 0) {
      const address = addressResult.page.content[0];
      // //adresa
      // "Adresa elev",
      let addressLine = `${address.streetName} Nr. ${address.streetNumber}`;

      if (address.building) {
        addressLine = addressLine + ` Bl.${address.building}`
      }

      if (address.staircase) {
        addressLine = addressLine + ` Sc.${address.staircase}`
      }

      if (address.floor) {
        addressLine = addressLine + ` Et.${address.floor}`
      }

      if (address.apartment) {
        addressLine = addressLine + ` Ap.${address.apartment}`
      }
      csvLine.push(addressLine)

      // "Localitate adresa",
      csvLine.push(address.locality.description);

      // "Localitate superioara adresa",
      csvLine.push(address.locality.parentLocality.description);
    } else {
      csvLine.push("");
      csvLine.push("");
      csvLine.push("");
    }



    const attribParam = `generatorKey=entityAttributeValueStudent_GRID_GENERATOR_KEY&filter=%5B%7B%22property%22%3A%22entityAttribute.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B%22${venitId}%22%5D%7D%2C%7B%22property%22%3A%22student.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B${studentId}%5D%7D%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%7D&page=1&start=0&limit=25`;

    const attribResult = await siiirFetch(attribUrl, attribParam);

    if (attribResult.page.content.length > 0) { //get last attribute
      const attrib = attribResult.page.content[attribResult.page.content.length - 1];
      // //attrib
      // "Venit"

      csvLine.push(attrib.value);
    } else {
      csvLine.push("");
    }
    csvData.push(`"` + csvLine.join(`","`) + `"`);
  }

  //generate CSV
  const fileName = "export_oug133.csv";
  const buffer = "\uFEFF" + headers.join(",") + "\n" + csvData.join("\n");
  const blob = new Blob([buffer], {
    type: "text/csv;charset=utf8;",
  });
  const url = window.URL.createObjectURL(blob);
  const a = document.createElement("a");
  a.href = url;
  a.download = fileName;
  a.click();

})();