(async () => { //top level async

  const schoolYearId = AMS.util.AppConfig.selectedYear.data.id;
  const listUrl = "https://www.siiir.edu.ro/siiir/list/asocStudentAtSchoolView.json";
  const listParamString = `generatorKey=ASSQG&filter=%5B%7B%22property%22%3A%22associated%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5Bfalse%5D%7D%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A${schoolYearId}%7D&page=1&start=0&limit=1000`;
  const actionUrl = "https://www.siiir.edu.ro/siiir/studentManagement/closeStudentAtSchool.json";

  const statusCode = 5; //5 = transferat
  const closeDate = "2022-08-31T00%3A00%3A00";
  const cnps = {
    "5123456789012": true,
  }

  //restore console
  const iFrame = document.createElement('iframe');
  iFrame.style.display = 'none';
  document.body.appendChild(iFrame);
  window.console = iFrame.contentWindow.console;

  const siiirFetch = async (url, body) => fetch(`${url}?_dc=${new Date().getTime()}`, {
    method: "post",
    credentials: "same-origin",
    headers: {
      Accept: "application/json",
      "Content-type": "application/x-www-form-urlencoded",
    },
    body: body,
  });

  const siiirGet = async (url, body) => fetch(`${url}?_dc=${new Date().getTime()}${body}`, {
    method: "get",
    credentials: "same-origin",
    headers: {
      Accept: "application/json",
      "Content-type": "application/x-www-form-urlencoded",
    },
  });


  const listResult = await siiirFetch(listUrl, listParamString);
  const listJson = await listResult.json();
  const list = listJson.page.content;

  for (const item of list) {
    if (cnps[item.student.nin]) {
      console.log("Found cnp: ", item.student.nin);
      await siiirGet(actionUrl, `&ssId=${item.id}&ssStatusId=${statusCode}&closeDate=${closeDate}`);
      //https://www.siiir.edu.ro/siiir/studentManagement/closeStudentAtSchool.json?_dc=1663137038706&ssId=8575320&ssStatusId=5&closeDate=2022-08-31T00:00:00
    }
  }

  console.log("Done");

})();