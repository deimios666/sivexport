(async () => {
    //for each parent (parental relationships only), lists all kids, and adds all kids as siblings to each other
    const pageSize = 500;
    const schoolYearId = AMS.util.AppConfig.selectedYear.data.id;
    const listUrl = "/siiir/list/asocParentAtSchool.json";
    const listParamString = `generatorKey=APASQG&filter=%5B%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A${schoolYearId}%7D`;
    const kidListUrl = "/siiir/list/studentParentOrTutor.json";
    const kidListParamString = parentId => `generatorKey=ASAPFSSQG&filter=%5B%7B%22property%22%3A%22parent.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B${parentId}%5D%7D%2C%7B%22property%22%3A%22kinshipType.description%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B%22Parental%5Cu0103%22%5D%7D%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%22parentId%22%3A${parentId}%7D&viewName=parentView&page=1&start=0&limit=100`;

    const getSiblingUrl = "/siiir/list/studentSibling.json";
    const getSiblingParamString = studentId => `generatorKey=studentSibling_GRID_GENERATOR_KEY&filter=%5B%7B%22property%22%3A%22student.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B${studentId}%5D%7D%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%7D&page=1&start=0&limit=100`;

    const addSiblingUrl = "/siiir/management/studentSibling";

    //restore console
    let iFrame = document.createElement("iframe");
    iFrame.style.display = "none";
    document.body.appendChild(iFrame);
    window.console = iFrame.contentWindow.console;

    const siiirFetch = async (url, body) => fetch(`${url}?_dc=${new Date().getTime()}`, {
        method: "post",
        credentials: "same-origin",
        headers: {
            Accept: "application/json",
            "Content-type": "application/x-www-form-urlencoded",
        },
        body: body,
    });

    const siiirPost = async (url, body) =>
        fetch(`${url}?_dc=${new Date().getTime()}`, {
            method: "post",
            credentials: "same-origin",
            headers: {
                Accept: "application/json",
                "Content-type": "application/json",
            },
            body: JSON.stringify(body),
        });

    const primingListResponse = await siiirFetch(`${listUrl}`, `${listParamString}&limit=1&start=0&page=1`);
    const primingListJSON = await primingListResponse.json();
    const maxResults = primingListJSON.page.total;
    let currentResult = 0;
    const maxPages = Math.ceil(maxResults / pageSize);

    for (let page = 1; page <= maxPages; page++) {
        const start = (page - 1) * pageSize;
        console.log(`Processing page ${page}/${maxPages}`);
        const parentListResponse = await siiirFetch(`${listUrl}`, `${listParamString}&limit=${pageSize}&start=${start}&page=${page}`);
        const parentListJSON = await parentListResponse.json();
        const parentList = parentListJSON.page.content;
        for (const { parent } of parentList) {
            currentResult++;
            const kidListResponse = await siiirFetch(`${kidListUrl}`, `${kidListParamString(parent.id)}`);
            const kidListJSON = await kidListResponse.json();
            const kidList = kidListJSON.page.content;
            console.log(`[${currentResult}/${maxResults}]: ${kidList.length} kids`);
            if (kidList.length < 2) {
                continue;
            }

            //generate combination of every kid with every other kid
            const possibleKids = new Map();

            kidList.forEach(({ student }) => {
                possibleKids.set(student.nin, student);
            });

            possibleKids.forEach(async (student, nin) => {
                //query for existing siblings
                const getSiblingResponse = await siiirFetch(`${getSiblingUrl}`, `${getSiblingParamString(student.id)}`);
                const getSiblingJSON = await getSiblingResponse.json();
                const siblingList = getSiblingJSON.page.content;
                const existingSiblingNinList = siblingList.map(sibling => sibling.nin);

                //add missing siblings if need be
                possibleKids.forEach(async (sibling, siblingNin) => {
                    if (nin === siblingNin) {
                        return;
                    }
                    if (existingSiblingNinList.includes(siblingNin)) {
                        return;
                    }

                    const payload = {
                        student: { id: student.id },
                        id: null,
                        properties: null,
                        firstName: sibling.firstName,
                        firstName1: sibling.firstName1,
                        firstName2: sibling.firstName2,
                        lastName: sibling.lastName,
                        nin: sibling.nin,
                    };
                    await siiirPost(`${addSiblingUrl}`, payload);
                    console.log(`Added ${sibling.firstName} ${sibling.lastName} as sibling to ${student.firstName} ${student.lastName}`);
                });
            });
        }
    }
})();