//import pdfjs
(async () => { await import("https://cdnjs.cloudflare.com/ajax/libs/jszip/3.10.0/jszip.min.js"); })();

const doZipCheck = async event => {
  //disable the default link behavior
  event.preventDefault();
  const el = event.target;
  const url = el.href;

  //disable the link
  loadingEnable(el);

  let fileBlob = null;
  //fetch the file
  const fileResponse = await fetch(url);
  fileBlob = await fileResponse.blob();
  const zip = new JSZip();
  try {
    await zip.loadAsync(fileBlob);
  } catch (e) {
    if (e.toString().includes(`Corrupted zip: can't find end of central directory`)) {
      console.error("Corrupt zip file", e);
      //remove loading indicator
      loadingDisable(el);
      //abort the download
      return;
    }
    if (e.toString().includes(`Encrypted zip are not supported`)) {
      //file is ok, but encrypted
    }
  }

  //download the file
  const a = document.createElement("a");
  a.href = URL.createObjectURL(fileBlob);
  a.download = url.split("/").pop();
  a.click();
  a.remove();

  //remove loading indicator
  loadingDisable(el);
}

//for each link in the page, check if it is a zip file
const mkLinks = () => document.querySelectorAll("a[href$='.zip']").forEach(el => {
  el.addEventListener("click", doZipCheck);
});

//after body loaded setup the link event handlers
document.querySelector("body").onpageshow = mkLinks();

const loadingEnable = el => {
  el.disabled = true;
}

const loadingDisable = el => {
  el.disabled = false;
}
