(async () => {
  const phaseId = "11"; //11 = nefinalizat 14 = finalizat

  const listUrl = "https://evnat.edu.ro/evnat/list/examCenterPhaseConfigurator.json";
  const actionUrlPrefix = "https://evnat.edu.ro/evnat/management/examCenterPhaseConfigurator";

  let codsiiir = [];
  let listParamString = codsiiir.length === 0 ? ["generatorKey=EXPCQG&filter=%5B%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%7D&page=1&start=0&limit=100"] : codsiiir.map(cod => `generatorKey=EXPCQG&filter=%5B%7B%22property%22%3A%22examCenter.school.siiirCode%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B%22${cod}%22%5D%7D%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%7D&page=1&start=0&limit=2`);

  for (paramString of listParamString) {
    const listResult = await fetch(listUrl + "?_dc=" + (new Date).getTime(), {
      method: 'post',
      credentials: 'same-origin',
      headers: {
        "Accept": "application/json",
        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
      },
      body: listParamString
    })
    const listJSON = await listResult.json();
    for (item of listJSON.page.content) {
      const payload = {
        id: item.id,
        phase: {
          id: phaseId
        },
        examCenter: {
          id: item.examCenter.id
        }
      }
      await fetch(actionUrlPrefix + "/" + item.id + "?_dc=" + (new Date).getTime(), {
        method: 'put',
        credentials: 'same-origin',
        headers: {
          "Accept": "application/json",
          "Content-type": "application/json"
        },
        body: JSON.stringify(payload)
      });
      console.log(item.examCenter.description);
    };
  }
})();
