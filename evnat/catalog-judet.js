(async () => {
    const president = "prof. Kiss Imre";
    const secretary = "prof. Pandele Maria";

    const reportUrl = `https://evnat.edu.ro/evnat/reports/10211.jasper`;
    const reportBody = `type=pdf&source=ISJ_GradeBookSource&president=${encodeURIComponent(president)}&secretar=${encodeURIComponent(secretary)}`;
    const reportResult = await fetch(reportUrl, {
        method: 'post',
        credentials: 'same-origin',
        headers: {
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
            "Content-Type": "application/x-www-form-urlencoded"
        },
        body: reportBody
    });
    const reportBlob = await reportResult.blob();

    const dateString = (new Date()).toISOString().substring(0, 10);
    const fileName = `catalog_judet_${dateString}.pdf`;

    const url = window.URL.createObjectURL(reportBlob);
    const a = document.createElement("a");
    a.href = url;
    a.download = fileName;
    a.click();
})();
