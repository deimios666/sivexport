//DOES NOT WORK
(async () => {


    await import("https://cdnjs.cloudflare.com/ajax/libs/jszip/3.10.1/jszip.min.js");

    const president = "prof. Kiss Imre";
    const secretary = "prof. Pandele Maria";

    const zip = new JSZip();

    const listUrl = "https://evnat.edu.ro/evnat/list/examCenterPhaseConfigurator.json";
    const listParams = `generatorKey=EXPCQG&filter=%5B%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%7D&page=1&start=0&limit=5000`;

    const listResult = await fetch(listUrl, {
        method: 'post',
        credentials: 'same-origin',
        headers: {
            "Accept": "application/json",
            "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
        },
        body: listParams
    });
    const listJSON = await listResult.json();

    for (const item of listJSON.page.content) {
        const centerId = item.examCenter.id;
        const centerName = item.examCenter.description.split('"').join("'");

        const reportUrl = `https://evnat.edu.ro/evnat/reports/10211.jasper`;
        const reportBody = `type=pdf&source=ISJ_GradeBookSource&examCenterId=${centerId}&president=${encodeURIComponent(president)}&secretar=${encodeURIComponent(secretary)}`;
        const reportResult = await fetch(reportUrl, {
            method: 'post',
            credentials: 'same-origin',
            headers: {
                "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
                "Content-Type": "application/x-www-form-urlencoded"
            },
            body: reportBody
        });
        const reportBlob = await reportResult.blob();
        zip.file(`${centerName}.pdf`, reportBlob, {
            type: 'application/zip'
        });

    }

    const zipBlob = await zip.generateAsync({
        type: "blob"
    });

    const dateString = (new Date()).toISOString().substring(0, 10);
    const fileName = `cataloage_evnat_centre_${dateString}.zip`;

    const url = window.URL.createObjectURL(zipBlob);
    const a = document.createElement("a");
    a.href = url;
    a.download = fileName;
    a.click();
})();
