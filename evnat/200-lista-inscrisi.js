var url="https://evnat.edu.ro/evnat/excel/10154.jasper";

fetch(url,{
	method: 'post',
	credentials: 'same-origin',
	headers: {
	"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
	"Content-type": "application/x-www-form-urlencoded"
	},
	body: "type=xls&source=ISJ_ListOfCandidates"

})
.then(
	response => response.blob()
	)
.then(blob => {
	var url = window.URL.createObjectURL(blob);
	var a = document.createElement('a');
	a.href = url;
	a.download = "inscrisi.xlsx";
	a.click(); 
});
