(async () => {
    const countyId = AMS.util.AppConfig.filtersConfig.county[0].parameters[0];

    const requestLimit = 3000;
    const listUrl = "https://evnat.edu.ro/evnat/list/examClassRoom.json";
    const listBody = `generatorKey=EVCQG&filter=%5B%7B%22property%22%3A%22examCenter.school.county.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B${countyId}%5D%7D%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%7D&page=1&start=0&limit=${requestLimit}`;
    const listResult = await fetch(listUrl, {
        method: 'post',
        credentials: 'same-origin',
        headers: {
            "Accept": "application/json",
            "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
        },
        body: listBody
    });
    const listJSON = await listResult.json();

    const csvData = [];
    const itemList = listJSON.page.content;

    const csvHeader = [
        "Id Sala",
        "Nume Sala",
        "Cod Centru",
        "Unitate",
        "Supraveghere Audio",
        "Supraveghere Video",
        "Capacitate",
        "Ocupat"
    ];

    const csvValues = [
        'id',
        'description',
        'examCenter.code',
        'examCenter.description',
        'audioSurveillance',
        'videoSurveillance',
        'capacity',
        'allocatedSeats'
    ];

    const getDeepValue = (obj, path) => {
        const pathArray = path.split('.');
        let value = obj;
        for (const key of pathArray) {
            value = value[key];
        }
        return value;
    };

    for (const item of itemList) {
        const csvRow = [];
        for (const key of csvValues) {
            let value = getDeepValue(item, key);
            if (value === null) {
                csvRow.push('');
            } else {
                //if it contains a quote or comma then string escape it
                if (typeof value === 'string' && (value.includes('"') || value.includes(","))) {
                    value = value.replace(/"/g, '""');
                    csvRow.push(`"${value}"`);
                } else {
                    csvRow.push(value);
                }
            }
        }
        csvData.push(csvRow.join(','));
    }

    const dateString = (new Date()).toISOString().substring(0, 10);
    const fileName = `export_săli_evnat_${dateString}.csv`;
    const buffer = "\uFEFF" + csvHeader.join(",") + "\n" + csvData.join("\n");
    const blob = new Blob([buffer], {
        type: "text/csv;charset=utf8;",
    });

    const url = window.URL.createObjectURL(blob);
    const a = document.createElement("a");
    a.href = url;
    a.download = fileName;
    a.click();

})();