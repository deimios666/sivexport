(async () => {
  await import('https://unpkg.com/dompurify@3.0.1/dist/purify.min.js');
  await import('https://unpkg.com/html2canvas@1.4.1/dist/html2canvas.min.js');
  await import('https://unpkg.com/jspdf@latest/dist/jspdf.umd.min.js');
  //console.log(jspdf);

  const doc = new jspdf.jsPDF(
    'p', 'mm', 'a4'
  );
  
  console.log(doc);
  await doc.html(document.getElementsByTagName("body")[0].outerHTML, {

    html2canvas: {
      scale: 0.25
    }
  });
  doc.save('sample-file.pdf');
})();
