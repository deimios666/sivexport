//this is run with nodejs, NOT in the browser

const password = "";

const siiirCodes = [1461100182, 1461100268, 1461100399, 1461100639, 1461100761, 1461100327, 1461100553, 1461100562, 1461100413, 1461100657, 1461100648, 1461100535, 1461100788, 1461100191, 1461100621, 1461100277, 1461100666, 1461100458, 1461100146, 1461100128, 1461100137, 1461100336, 1461100205, 1461100295, 1461100508, 1461100526, 1461100707, 1461100684, 1461100589, 1461100838, 1461100544, 1461100015, 1461100675, 1461100069, 1461100078, 1461100318, 1461100363, 1461100309, 1461100612, 1461100096, 1461100051, 1461100232, 1461100865, 1431100167, 1491103535, 1441100032, 1481100025, 1461100101, 1461100716, 1461103687, 1461100603, 1461100485, 1461100119, 1461100517, 1461100811, 1461100779, 1461100797, 1461100494, 1461100155, 1461100404, 1471100594, 1461100829, 1461100223, 1461100467, 1461100259, 1461100241, 1461100214, 1461100173, 1461100372, 1461100381, 1461100422, 1461100693, 1461100286, 1461100431, 1461100802, 1461100345, 1461100743, 1461200049, 1461100734, 1461100856, 1461100725, 1461100571, 1461100087, 1461100752];
//only licee
/*const siiirCodes = [
    1461100761, 1461100137, 1461100544, 1461100015, 1461100101, 1461100716, 1461100811, 1461100779, 1461100797, 1461100494, 1461100155, 1461100404, 1461100431, 1461100381, 1461100422, 1461100693, 1461100286,
];*/
const payload = async (cookie, siiirCode) => {

    //MUST BE HARDCODED, we have no AMS API
    const schoolYearId = 27;

    const listUrl = `https://siiir.edu.ro/siiir/list/kindergartenRegistration.json`;
    const listParams = `generatorKey=KRQG&filter=%5B%7B%22property%22%3A%22registrationEvaluationStatus%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B%22NOTEVALUATED%22%5D%7D%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A${schoolYearId}%7D`;

    const acceptActionUrl = () => `https://siiir.edu.ro/siiir/acceptKinderStudent`;
    const rejectActionUrl = () => `https://siiir.edu.ro/siiir/rejectKinderStudent`;

    //regId=2650051&loggedSchoolId=11485521

    const siiirFetch = async (url, params, cookie) => {
        return await fetch(url + "?_dc=" + (new Date).getTime(), {
            method: 'post',
            credentials: 'same-origin',
            headers: {
                "Accept": "application/json",
                "Cookie": cookie,
                "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
            },
            body: params
        });
    }

    //run script
    const listResult = await siiirFetch(listUrl + "&page=1&start=0&limit=10000;", listParams, cookie);

    const listJSON = await listResult.json();

    console.log(`[${siiirCode}] Found ${listJSON.page.total} students`);

    const concurrencyLimit = 10;
    let activeTasks = [];

    for (const reg of listJSON.page.content) {
        if (activeTasks.length >= concurrencyLimit) {
            await Promise.race(activeTasks);
        }
        const task = async () => {
            const regId = reg.id;
            const loggedSchoolId = (reg.kindergarten1.parentSchool || reg.kindergarten1).id

            //try to accept then reject
            const acceptResponse = await fetch(acceptActionUrl(), {
                method: 'post',
                credentials: 'same-origin',
                headers: {
                    "Accept": "application/json",
                    "Cookie": cookie,
                    "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                },
                body: `regId=${regId}&loggedSchoolId=${loggedSchoolId}`
            });
            const acceptResponseJSON = await acceptResponse.json();
            if (acceptResponseJSON.success) {
                console.log(`[${siiirCode}][${regId}] Accepted`);
            } else {
                await fetch(rejectActionUrl(), {
                    method: 'post',
                    credentials: 'same-origin',
                    headers: {
                        "Accept": "application/json",
                        "Cookie": cookie,
                        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                    },
                    body: `regId=${regId}&loggedSchoolId=${loggedSchoolId}`
                });
                console.log(`[${siiirCode}][${regId}] Rejected`);
            }
        }
        const activeTask = task().then(() => {
            activeTasks.splice(activeTasks.indexOf(activeTask), 1);
        }).catch(() => {
            activeTasks.splice(activeTasks.indexOf(activeTask), 1);
        });
        activeTasks.push(activeTask);
    }
}

(async () => {
    //for each siiirCode
    for (const siiirCode of siiirCodes) {
        console.log(`[${siiirCode}] Processing`);
        //get cookie
        const cookieResponse = await fetch(
            "https://siiir.edu.ro/siiir/login.jsp"
        );
        const setcookie = cookieResponse.headers.get("Set-Cookie");
        const cookie = setcookie.split(";")[2].split(", ")[1];
        console.log(`[${siiirCode}] Cookie: ${cookie}`);
        const username = `CVX_${siiirCode}`;


        //login
        const loginResponse = await fetch(
            "https://siiir.edu.ro/siiir/j_spring_security_check",
            {
                method: "POST", // *GET, POST, PUT, DELETE, etc.
                mode: "cors", // no-cors, cors, *same-origin
                credentials: "same-origin", // include, same-origin, *omit
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
                    "Cookie": cookie
                },
                body: `j_username=${username}&j_password=${password}`
            });
        const loginresponseText = await loginResponse.text();
        console.log(`[${siiirCode}] Login response: ${loginresponseText}`);
        if (loginresponseText.includes("{success:true}")) {
            await payload(cookie, siiirCode);
        }
    }
})();