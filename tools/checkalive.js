<p>
Status SIIIR: 
<svg height="32" width="32" style="vertical-align:middle">
  <defs>
    <radialGradient id="gradred" cx="50%" cy="50%" r="50%" fx="50%" fy="50%">
      <stop offset="0%" style="stop-color:rgb(255,0,0);" />
      <stop offset="100%" style="stop-color:rgb(255,200,200);stop-opacity:1" />
    </radialGradient>
    <radialGradient id="gradgreen" cx="50%" cy="50%" r="50%" fx="50%" fy="50%">
      <stop offset="0%" style="stop-color:rgb(0,200,0);" />
      <stop offset="100%" style="stop-color:rgb(128,200,128);stop-opacity:1" />
    </radialGradient>
  </defs>
  <circle id="siiir_status_color" cx="16" cy="16" r="8" fill="transparent" />
  Sorry, your browser does not support inline SVG.  
</svg>
<script type="text/javascript">
let siiircheck = () => {
fetch(
    "https://www.siiir.edu.ro/siiir/login.jsp", 
    {
      method: "POST", // *GET, POST, PUT, DELETE, etc.
      mode: "no-cors", // no-cors, cors, *same-origin
      cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
      credentials: "same-origin", // include, same-origin, *omit
      headers: {
          "Content-Type": "application/json; charset=utf-8",
      },
      redirect: "follow", // manual, *follow, error
      referrer: "no-referrer", // no-referrer, *client
  }
)
.then(res=>{
  if(res.ok){
    document.getElementById("siiir_status_color").style.fill="url(#gradgreen)";
  } else {
    document.getElementById("siiir_status_color").style.fill="url(#gradred)";
  }
})
.catch(err=>{})
}
siiircheck();
setInterval(siiircheck, 30000);
</script>
</p>