Add-Type -AssemblyName System.Windows.Forms

# Create FolderBrowserDialog object
$folderBrowser = New-Object System.Windows.Forms.FolderBrowserDialog

# Set the description for the dialog box
$folderBrowser.Description = "Select the directory that contains the Excel files to remove sheet protection from"

# Show the folder browser dialog and capture the result
$result = $folderBrowser.ShowDialog()

# Check if the user clicked OK
if ($result -eq [System.Windows.Forms.DialogResult]::OK) {
    # Display the selected directory path
    Write-Output "Selected Directory: $($folderBrowser.SelectedPath)"

    # Get a list of Excel files in the selected directory
    Get-ChildItem -Path $folderBrowser.SelectedPath -Filter *.xlsx | 
    ForEach-Object {
        # Rename excel to xlsx.zip
        Rename-Item -Path $_.FullName -NewName ($_.FullName + ".zip")
        # Unzip the Excel xml files:  \xl\worksheets\sheet*.xml
        $zipFile = ($_.FullName + ".zip")
        $zipFolder = $_.FullName.Replace(".xlsx", "")
        Expand-Archive -Path $zipFile -DestinationPath $zipFolder
        # Remove the sheet protection from each sheet*.xml file
        Get-ChildItem -Path $zipFolder\xl\worksheets -Filter sheet*.xml |
        ForEach-Object {
            $xmlFile = $_.FullName
            $xmlContent = Get-Content -Path $xmlFile
            $xmlContent = $xmlContent -replace 'sheet="0"', 'sheet="1"'
            $xmlContent = $xmlContent -replace '<sheetProtection ', '<sheetProtection sheet="1" '
            $xmlContent | Set-Content -Path $xmlFile
        }
        # Zip the Excel xml files back into the Excel file
        Compress-Archive -Path $zipFolder\* -DestinationPath $zipFile -Force
        # Rename the zip file back to xlsx
        Rename-Item -Path $zipFile -NewName ($zipFile.Replace(".zip", ""))

        # Remove the unzipped folder
        Remove-Item -Path $zipFolder -Recurse -Force
    }
    #Show alert that the process is complete
    [System.Windows.Forms.MessageBox]::Show("Sheet protection removed from Excel files in selected directory", "Process Complete", [System.Windows.Forms.MessageBoxButtons]::OK, [System.Windows.Forms.MessageBoxIcon]::Information)
}
else {
    Write-Output "No directory selected"
}
