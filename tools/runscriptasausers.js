//this is run with nodejs, NOT in the browser

const password = "";

//const siiirCodes = [1461100182, 1461100268, 1461100399, 1461100639, 1461100761, 1461100327, 1461100553, 1461100562, 1461100413, 1461100657, 1461100648, 1461100535, 1461100788, 1461100191, 1461100621, 1461100277, 1461100666, 1461100458, 1461100146, 1461100128, 1461100137, 1461100336, 1461100205, 1461100295, 1461100508, 1461100526, 1461100707, 1461100684, 1461100589, 1461100838, 1461100544, 1461100015, 1461100675, 1461100069, 1461100078, 1461100318, 1461100363, 1461100309, 1461100612, 1461100096, 1461100051, 1461100232, 1461100865, 1431100167, 1491103535, 1441100032, 1481100025, 1461100101, 1461100716, 1461103687, 1461100603, 1461100485, 1461100119, 1461100517, 1461100811, 1461100779, 1461100797, 1461100494, 1461100155, 1461100404, 1471100594, 1461100829, 1461100223, 1461100467, 1461100259, 1461100241, 1461100214, 1461100173, 1461100372, 1461100381, 1461100422, 1461100693, 1461100286, 1461100431, 1461100802, 1461100345, 1461100743, 1461200049, 1461100734, 1461100856, 1461100725, 1461100571, 1461100087, 1461100752];
//only licee
const siiirCodes = [
    1461100761, 1461100137, 1461100544, 1461100015, 1461100101, 1461100716, 1461100811, 1461100779, 1461100797, 1461100494, 1461100155, 1461100404, 1461100431, 1461100381, 1461100422, 1461100693, 1461100286,
];
const payload = async (cookie, siiirCode) => {


    //MUST BE HARDCODED, we have no AMS API
    const schoolYearId = 27;

    const listUrl = `https://siiir.edu.ro/siiir/list/asocStudentAtSchoolView.json`;
    const listParams = `generatorKey=ASSQG&filter=%5B%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A${schoolYearId}%7D`;

    const getActionUrl = (studentId) => `https://siiir.edu.ro/siiir/management/student/${studentId}/?schoolYearId=${schoolYearId}&id=${studentId}&_dc=${new Date().getTime()}`;
    const putActionUrl = (studentId) => `https://siiir.edu.ro/siiir/management/student/${studentId}/?schoolYearId=${schoolYearId}&_dc=${new Date().getTime()}`;

    const siiirGet = async (url, cookie) => {
        return await fetch(url, {
            method: 'get',
            credentials: 'same-origin',
            headers: {
                "Accept": "application/json",
                "Cookie": cookie
            }
        });
    }

    const siiirFetch = async (url, params, cookie) => {
        return await fetch(url + "?_dc=" + (new Date).getTime(), {
            method: 'post',
            credentials: 'same-origin',
            headers: {
                "Accept": "application/json",
                "Cookie": cookie,
                "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
            },
            body: params
        });
    }

    const siiirPut = async (url, bodyJSON, cookie) => {
        return await fetch(url, {
            method: 'put',
            credentials: 'same-origin',
            headers: {
                "Accept": "application/json",
                "Cookie": cookie,
                "Content-type": "application/json; charset=UTF-8"
            },
            body: JSON.stringify(bodyJSON)
        });
    }

    const doubleLetters = ["CS", "DZ", "GY", "LY", "NY", "SZ", "TY", "ZS",];
    const tripleLetters = ["DZS",];

    const splitInitials = (initials) => {
        const trimmed = initials.toUpperCase().trim();
        if (trimmed === "") return "";

        if (trimmed === "-") return "-";

        //if last character is a dot return
        if (trimmed[trimmed.length - 1] === ".") return trimmed;

        //if length is 1 return with dot
        if (trimmed.length === 1) return trimmed + ".";

        const doSplit = (input, output) => {
            if (input.length === 0) return output;
            if (input.length === 1) return output + input + ".";
            if (input.length === 2) {
                if (doubleLetters.includes(input)) return output + input + ".";
                return output + input[0] + "." + input[1] + ".";
            }
            if (input.length === 3) {
                if (tripleLetters.includes(input)) return output + input + ".";
                //check if first two are a double letter
                if (doubleLetters.includes(input.slice(0, 2))) return output + input.slice(0, 2) + "." + input[2] + ".";
                //else check last two
                if (doubleLetters.includes(input.slice(1, 3))) return output + input[0] + "." + input.slice(1, 3) + ".";
                //else split into 3
                return output + input[0] + "." + input[1] + "." + input[2] + ".";
            }

            //check if first 3 are a triple letter
            if (tripleLetters.includes(input.slice(0, 3))) return doSplit(input.slice(3), output + input.slice(0, 3) + ".");
            //else check if first two are a double letter
            if (doubleLetters.includes(input.slice(0, 2))) return doSplit(input.slice(2), output + input.slice(0, 2) + ".");
            //else split first
            return doSplit(input.slice(1), output + input[0] + ".");
        };

        return doSplit(trimmed, "");
    }

    //run script
    const listResult = await siiirFetch(listUrl + "&page=1&start=0&limit=10000;", listParams, cookie);

    const listJSON = await listResult.json();

    console.log(`[${siiirCode}] Found ${listJSON.page.total} students`);

    const concurrencyLimit = 10;
    let activeTasks = [];

    for (const assoc of listJSON.page.content) {
        //check if assoc.formationCode contains "XII" (only run for students in the 12th grade)
        if (!assoc.formationCode.includes("XII")) continue;

        if (activeTasks.length >= concurrencyLimit) {
            await Promise.race(activeTasks);
        }
        const task = async () => {

            const studentId = assoc.student.id;
            //check and fix initials
            let initials = assoc.student.fatherInitial;

            //if initials contains a dot
            if (initials.includes(".")) {
                // strip spaces
                initials = initials.replace(/\s/g, "");
                // .- -> -
                initials = initials.replace(/\.\-/g, "-");
                // -. -> -
                initials = initials.replace(/\-\./g, "-");
                // .. -> .
                initials = initials.replace(/\.\./g, ".");

                //if we changed the initials, update the student
                if (initials != assoc.student.fatherInitial) {
                    const payloadResp = await siiirGet(getActionUrl(studentId), cookie);
                    const payloadJSON = await payloadResp.json();
                    const student = payloadJSON.baseEntity;
                    student.asocUnitDateFrom = student.from;
                    const newInitials = initials;
                    console.log(`[${siiirCode}][${student.nin}] ${student.fatherInitial} -> ${newInitials}`);
                    student.fatherInitial = newInitials;
                    await siiirPut(putActionUrl(studentId), student, cookie);
                }
            } else {
                const payloadResp = await siiirGet(getActionUrl(studentId), cookie);
                const payloadJSON = await payloadResp.json();
                const student = payloadJSON.baseEntity;
                student.asocUnitDateFrom = student.from;
                const newInitials = splitInitials(student.fatherInitial);
                console.log(`[${siiirCode}][${student.nin}] ${student.fatherInitial} -> ${newInitials}`);
                student.fatherInitial = newInitials;
                const putResponse = await siiirPut(putActionUrl(studentId), student, cookie);
                const putResponseText = await putResponse.text();
                //console.log(`[${siiirCode}][${student.nin}] Put response: ${putResponseText}`);
            }
        }
        const activeTask = task().then(() => {
            activeTasks.splice(activeTasks.indexOf(activeTask), 1);
        }).catch(() => {
            activeTasks.splice(activeTasks.indexOf(activeTask), 1);
        });
        activeTasks.push(activeTask);
    }
}

(async () => {
    //for each siiirCode
    for (const siiirCode of siiirCodes) {
        console.log(`[${siiirCode}] Processing`);
        //get cookie
        const cookieResponse = await fetch(
            "https://www.siiir.edu.ro/siiir/login.jsp"
        );
        const setcookie = cookieResponse.headers.get("Set-Cookie");
        const cookie = setcookie.split(";")[2].split(", ")[1];
        console.log(`[${siiirCode}] Cookie: ${cookie}`);
        const username = `CVX_${siiirCode}`;
        

        //login
        const loginResponse = await fetch(
            "https://www.siiir.edu.ro/siiir/j_spring_security_check",
            {
                method: "POST", // *GET, POST, PUT, DELETE, etc.
                mode: "cors", // no-cors, cors, *same-origin
                credentials: "same-origin", // include, same-origin, *omit
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
                    "Cookie": cookie
                },
                body: `j_username=${username}&j_password=${password}`
            });
        const loginresponseText = await loginResponse.text();
        console.log(`[${siiirCode}] Login response: ${loginresponseText}`);
        if (loginresponseText.includes("{success:true}")) {
            await payload(cookie, siiirCode);
        }
    }
})();